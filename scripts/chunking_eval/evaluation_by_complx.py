from scripts.CodeBLEU import calc_code_bleu
from pathlib import Path
import json


def get_results(result_file):
    try:
        with open(result_file, "r", encoding="utf-8") as f:
            results = json.load(f)
    except FileNotFoundError:
        results = {}
        with open(result_file, "w", encoding="utf-8") as f:
            json.dump(results, f, indent=4)
    
    return results


def eval_gen_code(target_code_file, generated_code_wocorrt_file, generated_code_chunking_file, generated_code_inputext_file,
                  params,
                  result_file,
                  weights=(0.25, 0.25, 0.25, 0.25)):
    wocorrt_bleu, wocortt_ngram_match_score, wocortt_weighted_ngram_match_score, wocortt_syntax_match_score, wocortt_dataflow_match_score = \
        calc_code_bleu.calculate_code_bleu([target_code_file], generated_code_wocorrt_file, "python", params, weights=weights)
    
    chunking_bleu, chunking_ngram_match_score, chunking_weighted_ngram_match_score, chunking_syntax_match_score, chunking_dataflow_match_score = \
        calc_code_bleu.calculate_code_bleu([target_code_file], generated_code_chunking_file, "python", params, weights=weights)
    
    inputext_bleu, inputext_ngram_match_score, inputext_weighted_ngram_match_score, inputext_syntax_match_score, inputext_dataflow_match_score = \
        calc_code_bleu.calculate_code_bleu([target_code_file], generated_code_inputext_file, "python", params, weights=weights)

    results = get_results(result_file)

    info = target_code_file.stem.split("_")
    diff = info[3]
    task = info[2]
    case = info[4]
    params_text = [str(p) for p in params]

    if diff not in results:
        results[diff] = {}
    if task not in results[diff]:
        results[diff][task] = {}
    if case not in results[diff][task]:
        results[diff][task][case] = {}
    if "-".join(params_text) not in results[diff][task][case]:
        results[diff][task][case]["-".join(params_text)] = {}

    results[diff][task][case]["-".join(params_text)]["codebleu-score"]= {
        "wocorrt": {
            "codebleu": wocorrt_bleu,
            "ngram-match-score": wocortt_ngram_match_score,
            "weighted-ngram-match-score": wocortt_weighted_ngram_match_score,
            "syntax-match-score": wocortt_syntax_match_score,
            "dataflow-match-score": wocortt_dataflow_match_score
        },
        "chunking": {
            "codebleu": chunking_bleu,
            "ngram-match-score": chunking_ngram_match_score,
            "weighted-ngram-match-score": chunking_weighted_ngram_match_score,
            "syntax-match-score": chunking_syntax_match_score,
            "dataflow-match-score": chunking_dataflow_match_score
        },
        "inputext": {
            "codebleu": inputext_bleu,
            "ngram-match-score": inputext_ngram_match_score,
            "weighted-ngram-match-score": inputext_weighted_ngram_match_score,
            "syntax-match-score": inputext_syntax_match_score,
            "dataflow-match-score": inputext_dataflow_match_score
        }
    }

    with open(result_file, "w", encoding="utf-8") as f:
        json.dump(results, f, indent=4)


if __name__ == '__main__':
    data_path  = Path("path to your/data/eval_results")
    result_file = data_path / "eval_results.json"

    # modify the following parameters every time running the evaluation
    complx_name = "complex4" # 0->4
    case_name = "_wrong.txt" # _all.txt, _notwrong.txt, _wrong.txt
    # end of modify

    target_code_file = data_path / "target_code_alltasks_{}{}".format(complx_name, case_name)
    generated_code_wocorrt_file = data_path / "generated_code_wocorrt_alltasks_{}{}".format(complx_name, case_name)
    generated_code_chunking_file = data_path / "generated_code_chunking_alltasks_{}{}".format(complx_name, case_name)
    generated_code_inputext_file = data_path / "generated_code_inputext_alltasks_{}{}".format(complx_name, case_name)

    

    params = [0.1,0.1,0.4,0.4] # ngram, weighted-ngram, syntax, dataflow
    eval_gen_code(target_code_file, generated_code_wocorrt_file, generated_code_chunking_file, generated_code_inputext_file,
                  params,
                  result_file,
                  weights=(0.25, 0.25, 0.25, 0.25)) # BLEU-4
    
    print("=====================================")
    
    params = [0.1,0.1,0.5,0.3] # ngram, weighted-ngram, syntax, dataflow
    eval_gen_code(target_code_file, generated_code_wocorrt_file, generated_code_chunking_file, generated_code_inputext_file,
                  params,
                  result_file,
                  weights=(0.25, 0.25, 0.25, 0.25)) # BLEU-4