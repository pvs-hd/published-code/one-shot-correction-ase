import json
import pickle
from scripts.one_shot_correction import nl_embedding
from scripts.ui_simulator import nl_to_code_with_options as nl2code_w_opts
from scripts.ui_simulator import nl_to_code_with_corrections as nl2code_w_corrts
from scripts.one_shot_correction.correction_ds import CorrectionDS
import copy


DESC_STR = "description"


def get_correction_info(nl_code, query_idx, corrt_ds, config):
    """Get correction information of a specific query from the correction datastore.

    Args:
        nl_code (dict): dict of nl queries and their corresponding codes
        query_idx (int or str): index of the query
        corrt_ds (dict): correction datastore
        config (dict): configuration

    Returns:
        tuple, dict: embedding of the query and its correction information.
    """
    if str(query_idx) not in nl_code:
        print("No query with index {} found in the nl-code file.".format(query_idx))
        return [], {}
    
    nl = nl_code[str(query_idx)]["nl"]
    nl_embd = nl_embedding.get_nl_embedding_multi_caches(nl, 
                                                         config["embedding"]["cache_file_paths"], 
                                                         config["embedding"]["embd_engine"], 
                                                         config["embedding"]["rate_limit_per_minute"])
    if tuple(nl_embd) in corrt_ds.keys():
        correction_info = corrt_ds[tuple(nl_embd)]
    else:
        correction_info = {}
    
    return nl_embd, correction_info


def retrieve_corrt_ds(query_idxs, config, test_case_idx):
    """Retrieve correction information of specific queries in the correction datastore.

    Args:
        query_idxs (list): list of query indexes with pre-defined patterns.
            "task_x_y": query describing x tasks with index y
            "all_1": all queries describing 1 task
            "all_1_excl": all queries describing 1 task, excluding the current test case index
            "all_2": all queries describing 2 tasks
            "all_3": all queries describing 3 tasks
            "all_2_3": all queries describing 2 or 3 tasks
        config (dict): config
        test_case_idx (int): index of the current test case

    Returns:
        dict: correction datastore of specific queries
    """
    if not query_idxs:
        print("No query indices provided.")
        return {}
    
    # load the whole correction ds
    corrt_ds_path = config["correction_ds"]["correction_ds_path"]
    try:
        with open(corrt_ds_path, "rb") as cds_f:
            corrt_ds = pickle.load(cds_f)
    except FileNotFoundError:
        print("No correction datastore found at: ", corrt_ds_path)
        return {}
    
    # load the queries and their target codes
    queries_1_task_file = config["scenarios"]["1_task"]
    queries_2_tasks_file = config["scenarios"]["2_tasks"]
    queries_3_tasks_file = config["scenarios"]["3_tasks"]
    try:
        with open(queries_1_task_file, "r") as q1_f:
            queries_1_task = json.load(q1_f)
        with open(queries_2_tasks_file, "r") as q2_f:
            queries_2_tasks = json.load(q2_f)
        with open(queries_3_tasks_file, "r") as q3_f:
            queries_3_tasks = json.load(q3_f)
    except FileNotFoundError:
        print("No queries file found at: ", queries_1_task_file)
        return {}
    
    # retrieve the correction info of specific queries
    filtered_corrt_ds = {}
    for query_idx in query_idxs:
        if query_idx.startswith("task_"):
            task_num = int(query_idx.split("_")[1])
            query_idx = int(query_idx.split("_")[2])
            if task_num == 1:
                nl_embd, corrt_info = get_correction_info(queries_1_task, query_idx, corrt_ds, config)
            elif task_num == 2:
                nl_embd, corrt_info = get_correction_info(queries_2_tasks, query_idx, corrt_ds, config)
            elif task_num == 3:
                nl_embd, corrt_info = get_correction_info(queries_3_tasks, query_idx, corrt_ds, config)

            if corrt_info:
                filtered_corrt_ds[tuple(nl_embd)] = corrt_info

        elif query_idx.startswith("all_"):
            if query_idx == "all_1":
                for idx in queries_1_task.keys():
                    nl_embd, corrt_info = get_correction_info(queries_1_task, idx, corrt_ds, config)
                    if corrt_info:
                        filtered_corrt_ds[tuple(nl_embd)] = corrt_info

            elif query_idx == "all_1_excl":
                for idx in queries_1_task.keys():
                    if idx != test_case_idx:
                        nl_embd, corrt_info = get_correction_info(queries_1_task, idx, corrt_ds, config)
                        if corrt_info:
                            filtered_corrt_ds[tuple(nl_embd)] = corrt_info

            if query_idx == "all_2" or query_idx == "all_2_3":
                for idx in queries_2_tasks.keys():
                    nl_embd, corrt_info = get_correction_info(queries_2_tasks, idx, corrt_ds, config)
                    if corrt_info:
                        filtered_corrt_ds[tuple(nl_embd)] = corrt_info
            
            if query_idx == "all_3" or query_idx == "all_2_3":
                for idx in queries_3_tasks.keys():
                    nl_embd, corrt_info = get_correction_info(queries_3_tasks, idx, corrt_ds, config)
                    if corrt_info:
                        filtered_corrt_ds[tuple(nl_embd)] = corrt_info

        else:
            print("Invalid query index: ", query_idx)
            return {}
        
    return filtered_corrt_ds


def gen_code_wo_corrt(nl_query, config):
    """Generate code for a NL query using NL2Code model.

    Args:
        nl_query (str): NL query
        config (dict): configuration

    Returns:
        list(str): generated codes.
    """
    # get config values for NL embedding
    embd_config = config["embedding"]
    embedding_cache_files = embd_config["cache_file_paths"]
    embd_engine = embd_config["embd_engine"]
    rate_limit_per_minute = embd_config["rate_limit_per_minute"]

    nl_embd = nl_embedding.get_nl_embedding_multi_caches(nl_query, embedding_cache_files, embd_engine, rate_limit_per_minute)

    # get config values for NL2Code
    nl2code_config = config["nl_to_code"]["openai"]
    nl2code_cache_files = nl2code_config["cache_file_paths"]
    order_logprobs = nl2code_config["order_logprobs"]
    discard_extra_code_snippets = nl2code_config["discard_extra_code_snippets"]
    function_only = nl2code_config["function_only"]
    remove_python_mark = nl2code_config["remove_python_mark"]
    rate_limit_per_minute = nl2code_config["rate_limit_per_minute"]

    model = nl2code_config["model"]
    messages = copy.deepcopy(nl2code_config["messages_frame"]) # deep copy to avoid changing the original config
    old_system_content = messages[0]["content"]
    old_user_content = messages[-1]["content"]
    messages[0]["content"] = "{}{}".format(old_system_content, nl2code_config["system_content"])
    messages[-1]["content"] = "{}{}{}{}".format(old_user_content, nl2code_config["user_content"], nl2code_config["query_prefix"], nl_query)
    temperature = nl2code_config["temperature"]
    max_tokens = nl2code_config["max_tokens"]
    top_p = nl2code_config["top_p"]
    n = nl2code_config["n"]
    frequency_penalty = nl2code_config["frequency_penalty"]
    presence_penalty = nl2code_config["presence_penalty"]
    stop_sequences = nl2code_config["stop_sequences"]

    print("Messages for getting code wo correction: ", messages)
    generated_codes = nl2code_w_opts.get_codes_single_input(nl_query, nl_embd, nl2code_cache_files,
                                             order_logprobs, discard_extra_code_snippets, function_only,
                                             remove_python_mark,
                                             rate_limit_per_minute,
                                             model=model,
                                             messages=messages,
                                             temperature=temperature,
                                             max_tokens=max_tokens,
                                             top_p=top_p,
                                             n=n,
                                             frequency_penalty=frequency_penalty,
                                             presence_penalty=presence_penalty,
                                             stop=stop_sequences)
    
    return generated_codes


def gen_code_chunking(nl_query, config, filtered_correction_ds, single_chunk=True):
    """Generate code for a NL query using chunking method.

    Args:
        nl_query (str): NL query
        config (dict): configuration
        corrt_ds (dict): filtered correction datastore

    Returns:
        list(str): generated codes.
    """
    # get config values
    embd_config = config["embedding"]
    embedding_cache_files = embd_config["cache_file_paths"]
    embd_engine = embd_config["embd_engine"]

    knn_config = config["correction_ds"]
    knn = knn_config["knn"]

    if single_chunk:
        knn_threshold = knn_config["knn_threshold_single"]
    else:
        knn_threshold = knn_config["knn_threshold_multi"]

    combine_values = config["nl2code_with_correction"]["combine_correctinfo_gencode"]
    chunking_config = config["nl2code_with_correction"]["chunking_method"]
    nl_threshold = chunking_config["nl_simi_threshold"]
    lemma = chunking_config["use_lemma"]
    stop_words = chunking_config["eliminate_stop_words"]
    nl_code_threshold = chunking_config["nl_code_threshold"]
    check_root_verb = chunking_config["check_root_verb"]
    has_string = chunking_config["compare_include_string"]
    has_comment = chunking_config["compare_include_comment"]
    language_model = chunking_config["spacy_language_model"]
    reduce_simi_snippets_threshold = chunking_config["reduce_simi_snippets_threshold"]
    reduce_strings = chunking_config["reduce_strings_in_built_code"]
    alternative_string = chunking_config["alternative_string"]
    string_len_threshold = chunking_config["string_len_threshold"]

    top_k = config["nl_to_code"]["top_k"]
    nl2code_config = config["nl_to_code"]["openai"]
    nl2code_cache_files= nl2code_config["cache_file_paths"]
    messages = copy.deepcopy(nl2code_config["messages_frame"]) # deep copy to avoid changing the original config
    old_system_content = messages[0]["content"]
    old_user_content = messages[-1]["content"]
    messages[0]["content"] = "{}{}".format(old_system_content, nl2code_config["system_content"])
    messages[-1]["content"] = "{}{}".format(old_user_content, nl2code_config["user_content"])
    query_prefix = nl2code_config["query_prefix"]
    rate_limit_per_minute = nl2code_config["rate_limit_per_minute"]
    remove_python_mark = nl2code_config["remove_python_mark"]
    order_logprobs = nl2code_config["order_logprobs"]
    discard_extra_code_snippets = nl2code_config["discard_extra_code_snippets"]
    function_only = nl2code_config["function_only"]

    model = nl2code_config["model"]
    temperature = nl2code_config["temperature"]
    max_tokens = nl2code_config["max_tokens"]
    top_p = nl2code_config["top_p"]
    n = nl2code_config["n"]
    frequency_penalty = nl2code_config["frequency_penalty"]
    presence_penalty = nl2code_config["presence_penalty"]
    stop_sequences = nl2code_config["stop_sequences"]

    correction_ds_path = None # we use filtered correction datastore

    # get code with correction
    correction_info, \
        recomm_codes = nl2code_w_corrts.build_code_by_chunking(
                                                        nl_query,
                                                        embedding_cache_files,
                                                        nl2code_cache_files,
                                                        messages,
                                                        query_prefix,
                                                        embd_engine,
                                                        rate_limit_per_minute,
                                                        remove_python_mark,
                                                        order_logprobs,
                                                        discard_extra_code_snippets,
                                                        top_k,
                                                        function_only,
                                                        correction_ds_path,
                                                        filtered_correction_ds,
                                                        knn,
                                                        combine_values,
                                                        nl_threshold,
                                                        lemma,
                                                        stop_words,
                                                        nl_code_threshold,
                                                        check_root_verb, 
                                                        has_string,
                                                        has_comment,
                                                        knn_threshold,
                                                        language_model,
                                                        reduce_simi_snippets_threshold,
                                                        reduce_strings,
                                                        alternative_string,
                                                        string_len_threshold,
                                                        model=model,
                                                        temperature=temperature,
                                                        max_tokens=max_tokens,
                                                        top_p=top_p,
                                                        n=n,
                                                        frequency_penalty=frequency_penalty,
                                                        presence_penalty=presence_penalty,
                                                        stop=stop_sequences
                                                        )
    
    if correction_info:
        return [correction_info["code"]]
    else:
        return recomm_codes
    

def gen_code_input_extending(nl_query, config, filtered_correction_ds, single_chunk=True):
    """Generate code for a NL query using input extending method.

    Args:
        nl_query (str): NL query
        config (dict): configuration
        corrt_ds (dict): filtered correction datastore

    Returns:
        list(str): generated codes.
        list(int): indices of the nearest neighbors in the correction datastore.
    """
    corrt_ds = CorrectionDS(ds=filtered_correction_ds)

    # get config values
    embd_config = config["embedding"]
    embedding_cache_files = embd_config["cache_file_paths"]
    embd_engine = embd_config["embd_engine"]
    rate_limit_per_minute = embd_config["rate_limit_per_minute"]

    nl_embd = nl_embedding.get_nl_embedding_multi_caches(nl_query, embedding_cache_files, embd_engine, rate_limit_per_minute)

    knn_config = config["correction_ds"]
    knn = knn_config["knn"]

    if single_chunk:
        knn_threshold = knn_config["knn_threshold_single"]
    else:
        knn_threshold = knn_config["knn_threshold_multi"]
    knn_indices, distances = corrt_ds.get_knn_indices(nl_embd, knn, knn_threshold)

    top_k = config["nl_to_code"]["top_k"]
    nl2code_config = config["nl_to_code"]["openai"]
    nl2code_cache_files= nl2code_config["cache_file_paths"]
    messages = copy.deepcopy(nl2code_config["messages_frame"]) # deep copy to avoid changing the original config
    old_system_content = messages[0]["content"]
    old_user_content = messages[-1]["content"]
    messages[0]["content"] = "{}{}".format(old_system_content, nl2code_config["system_content"])
    messages[-1]["content"] = "{}{}".format(old_user_content, nl2code_config["user_content"])
    examples_prefix = nl2code_config["examples_prefix"]
    query_prefix = nl2code_config["query_prefix"]
    simi_query_prefix = nl2code_config["simi_query_prefix"]
    rate_limit_per_minute = nl2code_config["rate_limit_per_minute"]
    remove_python_mark = nl2code_config["remove_python_mark"]
    order_logprobs = nl2code_config["order_logprobs"]
    discard_extra_code_snippets = nl2code_config["discard_extra_code_snippets"]
    function_only = nl2code_config["function_only"]

    model = nl2code_config["model"]
    temperature = nl2code_config["temperature"]
    max_tokens = nl2code_config["max_tokens"]
    top_p = nl2code_config["top_p"]
    n = nl2code_config["n"]
    frequency_penalty = nl2code_config["frequency_penalty"]
    presence_penalty = nl2code_config["presence_penalty"]
    stop_sequences = nl2code_config["stop_sequences"]

    combine_values = config["nl2code_with_correction"]["combine_correctinfo_gencode"]

    # if NL query is in correction ds
    if tuple(nl_embd) in corrt_ds.correction_ds:
        correction_info = corrt_ds.get_correction(nl_embd)

        # we can also get the code directly from correction info
        # however, we run this to to make it consistent with the other methods
        correction_info, \
        recomm_codes = nl2code_w_corrts.get_codes_with_correction(correction_info, nl_query, nl_embd, nl2code_cache_files,
                                         remove_python_mark,
                                         messages,
                                         query_prefix,
                                         order_logprobs, discard_extra_code_snippets, top_k, combine_values, 
                                         function_only,
                                         rate_limit_per_minute,
                                         model=model,
                                        temperature=temperature,
                                        max_tokens=max_tokens,
                                        top_p=top_p,
                                        n=n,
                                        frequency_penalty=frequency_penalty,
                                        presence_penalty=presence_penalty,
                                        stop=stop_sequences)
        if correction_info:
            return [correction_info["code"]], []
        else:
            return recomm_codes, []

    # correction_info in this case is None
    correction_info, \
        recomm_codes = nl2code_w_corrts.get_codes_with_corrections_codex(
                                                        corrt_ds,
                                                        knn_indices,
                                                        nl_query,
                                                        nl_embd,
                                                        embedding_cache_files,
                                                        embd_engine,
                                                        nl2code_cache_files,
                                                        remove_python_mark,
                                                        messages,
                                                        examples_prefix,
                                                        query_prefix,
                                                        simi_query_prefix,
                                                        order_logprobs,
                                                        discard_extra_code_snippets,
                                                        top_k,
                                                        function_only,
                                                        rate_limit_per_minute,
                                                        model=model,
                                                        temperature=temperature,
                                                        max_tokens=max_tokens,
                                                        top_p=top_p,
                                                        n=n,
                                                        frequency_penalty=frequency_penalty,
                                                        presence_penalty=presence_penalty,
                                                        stop=stop_sequences)
    
    # get simi queries
    if corrt_ds.correction_ds:
        correction_keys = list(corrt_ds.correction_ds.keys())
    simi_queries = []
    for idx in knn_indices:
        simi_query_embd = correction_keys[idx]
        correction_info = corrt_ds.get_correction(simi_query_embd)
        simi_nl = correction_info["nl"]
        simi_queries.append(simi_nl)
    
    return recomm_codes, simi_queries


def gen_code(test_case_info, config, case, query_type, test_case_idx):
    """Generate code for a specific case in the scenarios,
    and save the generated code to the scenarios file.

    Args:
        test_case_info (dict): test case info
        config (dict): config
        case (str): case, e.g. "empty_corrt_ds"
        query_type (str): query type, e.g. "1_task_queries"
        test_case_idx (int): test case index, e.g. 0

    Returns:
        dict: the updated test case info
    """

    # ignore case that already had code generated
    if (not test_case_info) or ("tag" in test_case_info and test_case_info["tag"] == "skip"):
        # or test_case_info["gen_code_chunking"]
        return test_case_info
    
    if "tag" in test_case_info and test_case_info["tag"] == "new":
    
        corrt_ds = retrieve_corrt_ds(test_case_info["corrt_ds"], config, test_case_idx)

        single_chunk = all(["task_1" in ds for ds in test_case_info["corrt_ds"]]) or all(["all_1" in ds for ds in test_case_info["corrt_ds"]])

        # generate code
        test_case_info["gen_code_wo_corrt"] = gen_code_wo_corrt(test_case_info["nl_query"], config)
        test_case_info["gen_code_chunking"] = gen_code_chunking(test_case_info["nl_query"], config, corrt_ds, single_chunk)
        codes, simi_queries = gen_code_input_extending(test_case_info["nl_query"], config, corrt_ds, single_chunk)
        test_case_info["gen_code_input_extending"] = codes
        test_case_info["simi_queries"] = simi_queries
        
    return test_case_info


def gen_codes(scenarios_file, config_file, case=None, query_type=None, test_case_idx=None):
    """Generate codes for all cases or a specific case in the scenarios,
    and save the generated codes to the scenarios file.

    Args:
        scenarios_file (str): path to scenarios file
        config_file (str): path to config file
        case (str, optional): case, e.g. "empty_corrt_ds"
        query_type (str, optional): query type, e.g. "1_task_queries"
        test_case_idx (int, optional): test case index, e.g. 0
    """
    try:
        with open(scenarios_file, "r") as sf:
            scenarios = json.load(sf)

        with open(config_file, "r") as cf:
            config = json.load(cf)
    except FileNotFoundError:
        print("File not found!")
        return
    
    if case is None:
        for case in scenarios:
            for query_type in scenarios[case]:
                if query_type != DESC_STR:
                    for test_case_idx in scenarios[case][query_type]:
                        if test_case_idx != DESC_STR:
                            test_case_info = scenarios[case][query_type][test_case_idx]
                            scenarios[case][query_type][test_case_idx] = gen_code(test_case_info, config, case, query_type, test_case_idx)
    elif query_type is None:
        for query_type in scenarios[case]:
            if query_type != DESC_STR:
                for test_case_idx in scenarios[case][query_type]:
                    if test_case_idx != DESC_STR:
                        test_case_info = scenarios[case][query_type][test_case_idx]
                        scenarios[case][query_type][test_case_idx] = gen_code(test_case_info, config, case, query_type, test_case_idx)
    elif test_case_idx is None:
        for test_case_idx in scenarios[case][query_type]:
            if test_case_idx != DESC_STR:
                test_case_info = scenarios[case][query_type][test_case_idx]
                scenarios[case][query_type][test_case_idx] = gen_code(test_case_info, config, case, query_type, test_case_idx)
    else:
        test_case_info = scenarios[case][query_type][test_case_idx]
        scenarios[case][query_type][test_case_idx] = gen_code(test_case_info, config, case, query_type, test_case_idx)

    with open(scenarios_file, "w") as sf:
        json.dump(scenarios, sf, indent=4)

    
if __name__ == "__main__":
    config_file = "path to your/scripts/chunking_eval/config.json"
    scenarios_file = "path to your/data/scenarios.json"

    case = "empty_corrt_ds"
    query_type = "1_task_queries"
    start_test_case_idxs = 0
    max_idx = 46

    for i in range(5):
        test_case_idx = start_test_case_idxs + i
        if test_case_idx <= max_idx:
            print("******* Running test case idx: ", test_case_idx)
            gen_codes(scenarios_file, config_file, case, query_type, str(test_case_idx))