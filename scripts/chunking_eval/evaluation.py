from scripts.CodeBLEU import calc_code_bleu
from pathlib import Path
import json


def get_results(result_file):
    try:
        with open(result_file, "r", encoding="utf-8") as f:
            results = json.load(f)
    except FileNotFoundError:
        results = {}
        with open(result_file, "w", encoding="utf-8") as f:
            json.dump(results, f, indent=4)
    
    return results


def eval_gen_code(target_code_file, generated_code_wocorrt_file, generated_code_chunking_file, generated_code_inputext_file,
                  params,
                  result_file,
                  weights=(0.25, 0.25, 0.25, 0.25)):
    wocorrt_bleu, wocortt_ngram_match_score, wocortt_weighted_ngram_match_score, wocortt_syntax_match_score, wocortt_dataflow_match_score = \
        calc_code_bleu.calculate_code_bleu([target_code_file], generated_code_wocorrt_file, "python", params, weights=weights)
    
    chunking_bleu, chunking_ngram_match_score, chunking_weighted_ngram_match_score, chunking_syntax_match_score, chunking_dataflow_match_score = \
        calc_code_bleu.calculate_code_bleu([target_code_file], generated_code_chunking_file, "python", params, weights=weights)
    
    inputext_bleu, inputext_ngram_match_score, inputext_weighted_ngram_match_score, inputext_syntax_match_score, inputext_dataflow_match_score = \
        calc_code_bleu.calculate_code_bleu([target_code_file], generated_code_inputext_file, "python", params, weights=weights)

    results = get_results(result_file)

    info = target_code_file.stem.split("_")
    task = info[3]
    case = info[2]
    complx = info[4]
    diff = info[5]
    special_cases = info[6]
    params_text = [str(p) for p in params]

    if task not in results:
        results[task] = {}
    if case not in results[task]:
        results[task][case] = {}
    if "{}-{}".format(complx, diff) not in results[task][case]:
        results[task][case]["{}-{}".format(complx, diff)] = {}
    if special_cases not in results[task][case]["{}-{}".format(complx, diff)]:
        results[task][case]["{}-{}".format(complx, diff)][special_cases] = {}
    if "-".join(params_text) not in results[task][case]["{}-{}".format(complx, diff)][special_cases]:
        results[task][case]["{}-{}".format(complx, diff)][special_cases]["-".join(params_text)] = {}

    results[task][case]["{}-{}".format(complx, diff)][special_cases]["-".join(params_text)]["codebleu-score"]= {
        "wocorrt": {
            "codebleu": wocorrt_bleu,
            "ngram-match-score": wocortt_ngram_match_score,
            "weighted-ngram-match-score": wocortt_weighted_ngram_match_score,
            "syntax-match-score": wocortt_syntax_match_score,
            "dataflow-match-score": wocortt_dataflow_match_score
        },
        "chunking": {
            "codebleu": chunking_bleu,
            "ngram-match-score": chunking_ngram_match_score,
            "weighted-ngram-match-score": chunking_weighted_ngram_match_score,
            "syntax-match-score": chunking_syntax_match_score,
            "dataflow-match-score": chunking_dataflow_match_score
        },
        "inputext": {
            "codebleu": inputext_bleu,
            "ngram-match-score": inputext_ngram_match_score,
            "weighted-ngram-match-score": inputext_weighted_ngram_match_score,
            "syntax-match-score": inputext_syntax_match_score,
            "dataflow-match-score": inputext_dataflow_match_score
        }
    }

    with open(result_file, "w", encoding="utf-8") as f:
        json.dump(results, f, indent=4)


if __name__ == '__main__':
    data_path  = Path("path to your/data/eval_results")
    result_file = data_path / "eval_results.json"

    # only for 3tasks
    explanations = [
        ["{}-exct-exct"],
        ["{}-exct-nosimi", "{}-nosimi-exct"],
        ["{}-exct-simi1", "{}-simi1-exct"],
        ["{}-exct-simin", "{}-simin-exct"],
        ["{}-nosimi-nosimi"],
        ["{}-nosimi-simi1", "{}-simi1-nosimi"],
        ["{}-nosimi-simin", "{}-simin-nosimi"],
        ["{}-simi1-simi1"],
        ["{}-simi1-simin", "{}-simin-simi1"],
        ["{}-simin-simin"]
    ]

    expl_cases = [
        "exct",
        "nosimi",
        "simi1",
        "simin"
    ]

    # case_name = "exct-exct-exct"
    case_names = [
        explanations[0][0],
        explanations[1][0],
        explanations[2][0],
        explanations[3][0],
        explanations[4][0],
        explanations[5][0],
        explanations[6][0],
        explanations[7][0],
        explanations[8][0],
        explanations[9][0]
    ]

    complx_diffs = [
        [
            ["complex0", "diff0"],
            ["complex2", "diff2"],
            ["complex3", "diff2"],
            ["complex4", "diff2"],
            ["complex2", "diff3"],
            ["complex3", "diff2"],
            ["complex4", "diff3"],
            ["complex3", "diff2"],
            ["complex4", "diff2"],
            ["complex4", "diff3"]
        ],
        [
            ["complex2", "diff2"],
            ["complex2", "diff3"],
            ["complex3", "diff2"],
            ["complex4", "diff3"],
            ["complex2", "diff4"],
            ["complex3", "diff3"],
            ["complex4", "diff4"],
            ["complex3", "diff2"],
            ["complex4", "diff3"],
            ["complex4", "diff3"]
        ],
        [
            ["complex3", "diff2"],
            ["complex3", "diff2"],
            ["complex3", "diff2"],
            ["complex4", "diff2"],
            ["complex3", "diff3"],
            ["complex3", "diff2"],
            ["complex4", "diff3"],
            ["complex3", "diff2"],
            ["complex4", "diff2"],
            ["complex4", "diff3"]
        ],
        [
            ["complex4", "diff2"],
            ["complex4", "diff3"],
            ["complex4", "diff2"],
            ["complex4", "diff3"],
            ["complex4", "diff4"],
            ["complex4", "diff3"],
            ["complex4", "diff3"],
            ["complex4", "diff2"],
            ["complex4", "diff3"],
            ["complex4", "diff3"]
        ]
    ]

    # modify the following parameters every time running the evaluation!!
    task_type = "3tasks"
    expl_case_num = 3
    expl_case_detail_num = 6

    special_cases = "all"

    # end of modification

    expl_case = expl_cases[expl_case_num]
    case_name = case_names[expl_case_detail_num].format(expl_case)
    complex_name = complx_diffs[expl_case_num][expl_case_detail_num][0]
    diff_name = complx_diffs[expl_case_num][expl_case_detail_num][1]

    target_code_file = data_path / task_type / "target_code_{}_{}_{}_{}_{}.txt".format(case_name, task_type, complex_name, diff_name, special_cases)
    generated_code_wocorrt_file = data_path / task_type / "generated_code_wocorrt_{}_{}_{}_{}_{}.txt".format(case_name, task_type, complex_name, diff_name, special_cases)
    generated_code_chunking_file = data_path / task_type / "generated_code_chunking_{}_{}_{}_{}_{}.txt".format(case_name, task_type, complex_name, diff_name, special_cases)
    generated_code_inputext_file = data_path / task_type / "generated_code_inputext_{}_{}_{}_{}_{}.txt".format(case_name, task_type, complex_name, diff_name, special_cases)

    

    params = [0.1,0.1,0.4,0.4] # ngram, weighted-ngram, syntax, dataflow
    eval_gen_code(target_code_file, generated_code_wocorrt_file, generated_code_chunking_file, generated_code_inputext_file,
                  params,
                  result_file,
                  weights=(0.25, 0.25, 0.25, 0.25)) # BLEU-4
    
    print("=====================================")
    
    params = [0.1,0.1,0.5,0.3] # ngram, weighted-ngram, syntax, dataflow
    eval_gen_code(target_code_file, generated_code_wocorrt_file, generated_code_chunking_file, generated_code_inputext_file,
                  params,
                  result_file,
                  weights=(0.25, 0.25, 0.25, 0.25)) # BLEU-4