# Refer to https://gitlab.com/pvs-hd/published-code/code-token-type-taxonomy


from scripts.ast_parser.ctt_tax import *
from scripts.ast_parser.vars_collector import *
from scripts.ast_parser import utils


def extract_token_types(node, ctt_tax, collector, vec, token, node_ids):
    """Extract token types of a given node in a recurrsive manner.

    Args:
        node (tree-sitter node): A tree-sitter node.
        ctt_tax (CttTax): A CttTax object to get token types.
        collector (VarsCollector): A collector of definitions of variables.
        vec (list): A list of taxonomy vectors.
        token (list): A list of tokens.
        node_ids (list): A list of node ids.
    """
    if node.is_named: # only consider named nodes (i.e. discarded : , . def if etc.)
        # add taxonomy vector of the current node
        tax_info = ctt_tax.get_taxonomy(node, collector.vars_def)
        refined_tax_info = list()
        for info in tax_info:
            if all(v ==0 for v in info):
                refined_info = [-1] * len(info)
                refined_tax_info.append(refined_info)
            else:
                refined_tax_info.append(info)

        vec.append(refined_tax_info)

        # add text of the current node
        token.append(utils.get_text(node))

        # add node id of the current node
        node_ids.append(node.id) #TODO

    # recurrsively add the above infos for all children nodes
    if node.named_children:
        for n in node.named_children:
            extract_token_types(n, ctt_tax, collector, vec, token, node_ids)
            

def tax_it(tree, categories):
    """Get token types info from a code snippet.

    Args:
        tree (tree-sitter): The code snippet in the tree-sitter format.
        categories(list): List with enabled categories
    """
    ctt_tax = CttTax(categories)
    vec = list()
    token = list()
    node_ids = list()

    collector = VarsCollector(tree)
    collector.collect_vars()

    root_node = tree.root_node
    extract_token_types(root_node, ctt_tax, collector, vec, token, node_ids)

    return vec, token, node_ids, ctt_tax