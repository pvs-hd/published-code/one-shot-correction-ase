# Refer to https://gitlab.com/pvs-hd/published-code/code-token-type-taxonomy


from ordered_set import OrderedSet
from scripts.ast_parser.tax import *

def get_literal_token_types(ctt_tax, vec):
    """Get literal token types from a taxonomy vector.

    Args:
        ctt_tax (CttTax): A CttTax object to get token types.
        vec (list): A list of taxonomy vectors.

    Returns:
        list: A list of literal token types.
    """
    literal_type = None
    if vec[0][0] != -1: # has syntax type
        literal_type = ctt_tax.translate_to_literal(vec)
    return literal_type

def get_types_per_token(tree, categories):
    """Get token types for each token in a tree-sitter tree.

    Args:
        tree (tree-sitter): The code snippet in the tree-sitter tree format
        categories(list): List with enabled categories

    Returns:
        dict: A dict with node id as key and token and list of token types as value.
    """
    ct3_per_token = {}
    vec_result, token_result, node_ids, ctt_tax = tax_it(tree, categories)

    for n_id, token, vec in zip(node_ids, token_result, vec_result):
        ct3_per_token[n_id] = {"token": token, "ct3": get_literal_token_types(ctt_tax, vec)}
    
    return ct3_per_token

def get_tokens_per_type(tree, category="SyntaxType"):
    """Get tokens for each token type in a tree-sitter tree.

    Args:
        tree (tree-sitter): The code snippet in the tree-sitter format
        category(str): Name of the enabled category, default is SyntaxType

    Returns:
        dict: A dict with token type as key and set of tokens as value.
    """
    ct3_per_token = get_types_per_token(tree, [category])
    ct3_per_type = {}
    for n_id, info in ct3_per_token.items():
        token_types = info["ct3"] #[["type1", "type2", ...]] or None
        if token_types is not None:
            for t_type in token_types[0]: # only one token type per token
                if t_type not in ct3_per_type:
                    ct3_per_type[t_type] = OrderedSet()
                ct3_per_type[t_type].add(info["token"])
        
    return ct3_per_type

def get_tokens_per_type_from_list(trees, category="SyntaxType"):
    """Get tokens for each token type in a list of tree-sitter trees.

    Args:
        trees (list of tree-sitter trees): A list of code snippets in tree-sitter trees format
        category(str): Name of the enabled category, default is SyntaxType

    Returns:
        dict: A dict with token type as key and set of tokens as value.
    """
    ct3_per_type_trees = {}
    for tree in trees:
        ct3_per_type_tree = get_tokens_per_type(tree, category)
        for t_type, tokens in ct3_per_type_tree.items():
            if t_type not in ct3_per_type_trees:
                ct3_per_type_trees[t_type] = OrderedSet()
            ct3_per_type_trees[t_type] |= tokens # union two OrderedSets
        
    return ct3_per_type_trees