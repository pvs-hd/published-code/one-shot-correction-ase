# utils for tree-sitter

from tree_sitter import Language, Parser
import os

def abbreviations():
    """List of common abbreviations in coding.
    """
    return {
        "+": "add",
        "-": "subtract",
        "*": "mulitply",
        "/": "divide",
        " ": "space",
        "_": "underscore",
        "addr": "address", # a
        "addrs": "addresses",
        "alt": "alternative",
        "alts": "alternatives",
        "app": "application",
        "apps": "applications",
        "arg": "argument",
        "args": "arguments",
        "arr": "array",
        "arrs": "arrays",
        "async": "asynchronous",
        "attr": "attribute",
        "attrs": "attributes",
        "auth": "authentication",
        "auths": "authentications",
        "avg": "average",
        "avgs": "averages",
        "bin": "binary", # b
        "bins": "binaries",
        "bg": "background",
        "bgs": "backgrounds",
        "bool": "boolean",
        "bools": "booleans",
        "btn": "button",
        "btns": "buttons",
        "buf": "buffer",
        "bufs": "buffers",
        "byte": "byte",
        "bytes": "bytes",
        "cache": "cache", # c
        "caches": "caches",
        "calc": "calculate",
        "calcs": "calculates",
        "cap": "capability",
        "caps": "capabilities",
        "cat": "category",
        "cats": "categories",
        "cb": "callback",
        "cbs": "callbacks",
        "cert": "certificate",
        "certs": "certificates",
        "cfg": "configuration",
        "cfgs": "configurations",
        "char": "character",
        "chars": "characters",
        "class": "class",
        "classes": "classes",
        "cli": "command-line interface",
        "clis": "command-line interfaces",
        "cmd": "command",
        "cmds": "commands",
        "cnt": "count",
        "cnts": "counts",
        "code": "code",
        "codes": "codes",
        "col": "column",
        "cols": "columns",
        "comp": "component",
        "comps": "components",
        "conf": "configuration",
        "confs": "configurations",
        "concat": "concatenate",
        "concats": "concatenates",
        "conn": "connection",
        "conns": "connections",
        "config": "configuration",
        "configs": "configurations",
        "const": "constant",
        "consts": "constants",
        "conv": "convert",
        "convs": "converts",
        "coord": "coordinate",
        "coords": "coordinates",
        "cos": "cosine",
        "cosines": "cosines",
        "cot": "cotangent",
        "cots": "cotangents",
        "count": "count",
        "counts": "counts",
        "cpu": "central processing unit",
        "cpus": "central processing units",
        "ctrl": "control",
        "ctrls": "controls",
        "ctx": "context",
        "ctxs": "contexts",
        "curr": "current",
        "currs": "currents",
        "data": "data", # d
        "datas": "datas",
        "db": "database",
        "dbs": "databases",
        "dbg": "debug",
        "dbgs": "debugs",
        "dec": "decimal",
        "decs": "decimals",
        "def": "definition",
        "defs": "definitions",
        "del": "delete",
        "dels": "deletes",
        "desc": "description",
        "descs": "descriptions",
        "dest": "destination",
        "dests": "destinations",
        "dev": "developer or development or device",
        "devs": "developers or developments or devices",
        "diff": "difference",
        "dict": "dictionary",
        "dicts": "dictionaries",
        "dir": "directory",
        "dirs": "directories",
        "dist": "distance",
        "dists": "distances",
        "disp": "display",
        "disps": "displays",
        "div": "divide",
        "doc": "document",
        "docs": "documents",
        "dom": "domain",
        "doms": "domains",
        "dot": "dot",
        "dots": "dots",
        "dyn": "dynamic",
        "dyns": "dynamics",
        "e": "event or exponent", # e
        "en": "enable",
        "eq": "equal",
        "eqs": "equals",
        "elem": "element",
        "elems": "elements",
        "env": "environment",
        "envs": "environments",
        "err": "error",
        "errs": "errors",
        "evt": "event",
        "evts": "events",
        "exe": "execute",
        "exes": "executes",
        "exec": "execute",
        "execs": "executes",
        "expr": "expression",
        "exprs": "expressions",
        "exp": "exponent",
        "exps": "exponents",
        "ext": "extension",
        "exts": "extensions",
        "e.g.": "for example",
        "etc.": "and so on",
        "f": "function", # f
        "fact": "factorial",
        "facts": "factorials",
        "fig": "figure",
        "figs": "figures",
        "file": "file",
        "files": "files",
        "func": "function",
        "funcs": "functions",
        "fwd": "forward",
        "fwds": "forwards",
        "g": "global", # g
        "gen": "generate",
        "gens": "generates",
        "geom": "geometry",
        "geoms": "geometries",
        "ge": "greater than or equal to",
        "gt": "greater than",
        "gui": "graphical user interface",
        "guis": "graphical user interfaces",
        "hash": "hash", # h
        "hashes": "hashes",
        "hdr": "header",
        "hdrs": "headers",
        "hex": "hexadecimal",
        "hexs": "hexadecimals",
        "hier": "hierarchy",
        "hiers": "hierarchies",
        "hist": "histogram",
        "hists": "histograms",
        "i": "index i", # i
        "id": "identifier",
        "ids": "identifiers",
        "idx": "index",
        "idxs": "indices",
        "img": "image",
        "imgs": "images",
        "impl": "implementation",
        "impls": "implementations",
        "inc": "include",
        "incs": "includes",
        "info": "information",
        "infos": "informations",
        "init": "initialize",
        "inits": "initializes",
        "ins": "insert",
        "inst": "instance",
        "int": "integer",
        "ints": "integers",
        "iter": "iterator",
        "iters": "iterators",
        "j": "index j", # j
        "js": "JavaScript",
        "json": "JavaScript Object Notation",
        "key": "key", # k
        "keys": "keys",
        "kwd": "keyword",
        "kwd": "keywords",
        "lang": "language", # l
        "langs": "languages",
        "len": "length",
        "lens": "lengths",
        "le": "less than or equal to",
        "lt": "less than",
        "lib": "library",
        "libs": "libraries",
        "list": "list",
        "lists": "lists",
        "map": "map", # m
        "maps": "maps",
        "mat": "matrix",
        "mats": "matrices",
        "max": "maximum",
        "maxs": "maximums",
        "mem": "memory",
        "mems": "memories",
        "mid": "middle",
        "mids": "middles",
        "min": "minimum",
        "mins": "minimums",
        "ml": "machine learning",
        "mls": "machine learnings",
        "mod": "module",
        "mods": "modules",
        "msg": "message",
        "msgs": "messages",
        "mul": "multiply",
        "mut": "mutable",
        "mut": "mutables",
        "nav": "navigate", # n
        "navs": "navigates",
        "ne": "not equal",
        "neq": "not equal",
        "neqs": "not equals",
        "net": "network",
        "nets": "networks",
        "n": "number",
        "ns": "numbers",
        "nl": "newline",
        "nls": "newlines",
        "num": "number",
        "nums": "numbers",
        "obj": "object", # o
        "objs": "objects",
        "oct": "octal",
        "octs": "octals",
        "op": "operator",
        "ops": "operators",
        "opt": "option",
        "opts": "options",
        "ord": "order",
        "ords": "orders",
        "out": "output",
        "outs": "outputs",
        "param": "parameter", # p
        "params": "parameters",
        "path": "path",
        "paths": "paths",
        "perm": "permutation",
        "perms": "permutations",
        "perf": "performance",
        "perfs": "performances",
        "pic": "picture",
        "pics": "pictures",
        "pix": "pixel",
        "pixs": "pixels",
        "pkg": "package",
        "pkgs": "packages",
        "pos": "position",
        "prop": "property",
        "props": "properties",
        "pred ": "predict",
        "preds": "predicts",
        "proc": "process",
        "procs": "processes",
        "prog": "program",
        "progs": "programs",
        "pref": "preference",
        "prefs": "preferences",
        "prev": "previous",
        "prevs": "previous",
        "priv": "private",
        "privs": "privates",
        "pub": "public",
        "ptr": "pointer",
        "ptrs": "pointers",
        "py": "Python",
        "py3": "Python 3",
        "py2": "Python 2",
        "q": "queue or query", # q
        "rand": "random", # r
        "rec": "record",
        "ref": "reference",
        "refs": "references",
        "regex ": "regular expression",
        "repo": "repository",
        "repos": "repositories",
        "req": "request",
        "reqs": "requests",
        "resp": "response",
        "resps": "responses",
        "rm": "remove",
        "rmv": "remove",
        "set": "set", # s
        "sets": "sets",
        "seq": "sequence",
        "sin": "sine",
        "sqrt": "square root",
        "src": "source",
        "stat": "statistic",
        "std": "standard",
        "stdio": "standard input output",
        "str": "string",
        "strs": "strings",
        "sync": "synchronization",
        "sys": "system",
        "sys": "systems",
        "temp": "temporary", # t
        "test": "test",
        "tests": "tests",
        "tf": "TensorFlow",
        "tmp": "temporary",
        "txt": "text",
        "util": "utility", # u
        "val": "value", # v
        "vals": "values",
        "var": "variable",
        "vars": "variables",
        "vec": "vector",
        "vecs": "vectors",
        "win": "window", # w
        "wins": "windows",
        "1": "one", # number according to stop words in spaCy
        "2": "two",
        "3": "three",
        "4": "four",
        "5": "five",
        "6": "six",
        "8": "eight",
        "9": "nine",
        "10": "ten",
        # "11": "eleven",
        # "12": "twelve",
        # "13": "thirteen",
        # "15": "fifteen",
        # "20": "twenty",
        # "50": "fifty",
        # "60": "sixty",
        # "100": "one hundred",
        "df": "dataframe",
        "dfs": "dataframes",
        "dropna": "drop all rows NaN",
        "thresh": "threshold",
    }

def operator_to_nl():
    """Get mapping from operator to NL.
    """
    return {
        "(": "of", 
        ")": "", # ignore ")"
        "[": "index of or list of", 
        "]": "", # ignore "]"
        "{": "dictionary", 
        "}": "", # ignore "}"
        "\'": "", # ignore "\'"
        "\"": "", # ignore "\""
        "__future__": "", # ignore "__future__"
        "+x": "positive", # unary operator
        "-x": "negative", # unary operator
        "~x": "bitwise not", # unary operator
        "+": "add",
        "-": "subtract",
        "*": "mulitply",
        "/": "divide",
        "%": "modulo",
        "@": "matrix multiply",
        "**": "exponentiate",
        "//": "floor divide",
        "<<": "bitwise left shift",
        ">>": "bitwise right shift",
        "&": "bitwise and",
        "|": "bitwise or",
        "^": "bitwise exclusive or (xor)",
        "~": "bitwise not",
        "==": "is equal to",
        "!=": "is not equal",
        "<>": "is not equal",
        "<": "is less than",
        "<=": "is less than or equal",
        ">": "is greater than",
        ">=": "is greater than or equal",
        "=": "is",
        "+=": "is increased by",
        "-=": "is decreased by",
        "*=": "is multiplied by",
        "/=": "is divided by",
        "%=": "is modulo by",
        "&=": "is bitwise and by",
        "|=": "is bitwise or by",
        "^=": "is bitwise exclusive or (xor) by",
        "<<=": "is bitwise left shift by",
        ">>=": "is bitwise right shift by",
        "**=": "is exponentiated by",
        "//=": "is floor divided by",
        "@=": "is matrix multiplied by",
        "->": "return anotation",
        ":=": "is",
        "async": "asynchronous",
        "break": "stop",
        "def": "define function",
        "del": "delete",
        "elif": "otherwise check if",
        "else": "otherwise",
        "endswith": "ends with",
        "except*": "except",
        "exec": "execute",
        "float": "float number",
        "for": "for each",
        "if": "check if",
        "input": "input from users",
        "int": "integer number",
        "lamda": "anonymous function",
        "lower": "lowercase",
        "nonlocal": "non local",
        "startswith": "starts with",
        "upper": "uppercase",
        # additional
        "to_csv": "write to csv",
        "read_csv": "read from csv",
    }

def get_ast_parser():
    """Get AST parser using tree-sitter.

    Returns:
        parser: An AST parser.
    """
    parser_path = '/export/home/tuyenle/repos/one-shot-correction-for-nl-to-code/scripts/ast_parser/build/my-languages.so'
    tree_sitter_python_repo = '/export/home/tuyenle/repos/tree-sitter-python'
    
    if os.name == 'nt':
        parser_path = "scripts/ast_parser/build/my-languages.so"
        tree_sitter_python_repo = "D:/PythonProjects/tree-sitter-python" # clone it by running 'git clone https://github.com/tree-sitter/tree-sitter-python'
        
    Language.build_library(
        # Store the library in the `build` directory
        parser_path,

        [
        tree_sitter_python_repo
        ]
    )

    PY_LANGUAGE = Language(parser_path, 'python')
    parser = Parser()
    parser.set_language(PY_LANGUAGE)
    return parser

def get_tree_sitter_tree(code_snippet):
    """Get tree-sitter tree from a code snippet.
    """
    # convert str code snippet to bytes
    snippet = bytes(code_snippet, "utf8")
    parser = get_ast_parser()
    tree = parser.parse(snippet)

    return tree

def get_text(node):
    """Get text of a tree-sitter node.

    Args:
        node (tree-sitter node): The node to get text from.

    Returns:
        (str) The text of the node.
    """
    return node.text.decode('utf8')

def is_root(node):
    """Check if node is root node.
    Args:
        node (tree-sitter node): The node to investigate.

    Returns:
        (bool) True if node is root node, False otherwise.
    """
    return node.parent is None

def is_leaf(node):
    """Check if node is leaf node.
    Args:
        node (tree-sitter node): The node to investigate.

    Returns:
        (bool) True if node is leaf node, False otherwise.
    """
    return node.named_child_count == 0 # for string case, " is a leaf node.

def is_leaf_strict(node):
    """Check if node is leaf node stricly because of quotes in string.
    Args:
        node (tree-sitter node): The node to investigate.

    Returns:
        (bool) True if node is leaf node, False otherwise.
    """
    return node.child_count == 0 # for string case, " is a leaf node.

def discard_first_empty_items(items):
    """Discard the first empty items in a list of items.
    Each item is a string.
    """
    cut_idx = len(items)
    for i in range(len(items)):
        if "".join(items[i].split()) != "":
            cut_idx = i
            break
    return items[cut_idx:]

def get_dfs_sequence(node, dfs_sequence):
    """Get depth-first search sequence starting from a tree-sitter node.
    Only consider named children.
    """
    dfs_sequence.append(node)
    for child in node.named_children:
        get_dfs_sequence(child, dfs_sequence)
    return dfs_sequence

def get_dfs_next_node_of_leaf(node):
    """Get the next node in DFS of a leaf node.
    """
    if node is None:
        return None
    
    if is_leaf(node):
        if node.next_sibling is not None:
            return node.next_sibling
        else:
            next_sibling = None
            while node.parent is not None:
                node = node.parent
                if node.next_sibling is not None:
                    next_sibling = node.next_sibling
                    break
            return next_sibling
    else:
        return None

def explain_simply(node, sumarization, has_string=True, has_comment=True):
    """Simply explain one code statement by getting depth-first search sequence of tokens starting from the node.
    Only consider leaf nodes.
    If the leaf node is also the named child, get its text
    if the leaf node is not the named child, and its type is from a predefined dictionary operator_to_nl,
    get the value of the dictionary, otherwise, get the type of the leaf node.
    """
    operator_nl = operator_to_nl()
    if is_leaf(node):
        node_line = node.start_point[0]
        next_leaf_node = get_dfs_next_node_of_leaf(node)
        if next_leaf_node is not None:
            next_node_line = next_leaf_node.start_point[0]
        else:  
            next_node_line = node_line

        if node.type == ":":
            if node_line + 1 == next_node_line:
                if node.parent and "definition" in node.parent.type:
                    sumarization.append("as follows:")
                else:
                    sumarization.append(",")
            else:
                sumarization.append("to") 
        elif node.type == "comment": # comment starts with #
            if has_comment:
                comment_content = get_text(node)[1:].strip()
                if comment_content:
                    sumarization.append(". {}.".format(comment_content))
        elif node.type == "string":
            if has_string:
                str_content = get_text(node)[1:-1]
                if str_content:
                    sumarization.append(str_content)
        elif node.type == "identifier":
            if node.parent.type == "call" and get_text(node).strip() in operator_nl:
                node_text = operator_nl[get_text(node).strip()]
            elif node.parent.type == "attribute" and\
                  node.parent.parent and\
                      node.parent.parent.type == "call" and\
                          node == node.parent.named_children[1] and\
                              get_text(node).strip() in operator_nl:
                # method call
                node_text = operator_nl[get_text(node).strip()]
            else:
                node_text = get_text(node).strip()

            id_tokens = node_text.split("_")
            if id_tokens:
                sumarization.extend(id_tokens)
        elif node.type == ".":
            sumarization.append("of")
        else:
            if node.is_named:
                node_text = get_text(node).strip()
                if node_text:
                    sumarization.append(node_text)
            else:
                node_type = node.type
                # unary cases
                if node.parent.type == "unary_operator":
                    node_type = "{}x".format(node.type)

                if node_type in operator_nl:
                    node_type_nl = operator_nl[node_type]
                    if node_type_nl:
                        sumarization.append(node_type_nl)
                else:
                    node_type_tokens = node_type.split("_")
                    if node_type:
                        sumarization.extend(node_type_tokens)
        
        if node_line + 1 == next_node_line and node.type != ":":
            sumarization.append(";")
    else:
        for child in node.children:
            explain_simply(child, sumarization, has_string=has_string, has_comment=has_comment)

    # convert some common abbreviations
    abbrevs = abbreviations()
    for idx, item in enumerate(sumarization):
        # trick for "result", which is recognized as VERB with spaCy
        if item.strip() == "result":
            sumarization[idx] = "the result"
        elif item in abbrevs:
            sumarization[idx] = abbrevs[item]

    return sumarization
    