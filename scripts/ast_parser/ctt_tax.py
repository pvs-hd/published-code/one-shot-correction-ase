# Refer to https://gitlab.com/pvs-hd/published-code/code-token-type-taxonomy


import importlib
from collections import defaultdict
from scripts.ast_parser.categories import *
import ast


class CttTax:
    def __init__(self, category_names):
        """Interface class for code token type generation and manipulation
           for given code tokens.

        Args:
            categories (list(str)): Names of enabled the category classes for
                               code token type generation
        """
        self.categories = defaultdict(lambda: None)
        self.features = None
        self.max_features = None
        self.initialize_categories(category_names)

    def get_taxonomy(self, node, vars_def):
        """Generates the code token type for a given code token.

        Args:
            (vertex (igraph.Vertex)): AST node containing code token
            (dict) vars_def: a dictionary of vars definition with <node_idx:(node_name, scope)>

        Returns.
            (list(list(int))) Numerical code token type vector.
        """
        return [cat.get_feature_vec(node, node.id, vars_def)
                for cat in self.categories.values()]

    def initialize_categories(self, category_names):
        """Initializes the enabled categories when instantiating the CttTax
           class.

        Args:
            categories (list(str)): Names of enabled the category classes for
                               code token type generation
        """
        init_categories = [getattr(importlib.import_module("scripts.ast_parser.categories"), cat)
                           for cat in category_names]
        init_categories = [Cat() for Cat in init_categories]
        self.features = [c.get_features() for c in init_categories]
        self.max_features = self.get_max_features_len()
        [c.set_feature_vec_len(self.max_features) for c in init_categories]
        for cat_name, cat in zip(category_names, init_categories):
            self.categories[cat_name] = cat

    def translate_to_literal(self, taxonomy):
        """Translates code token type numerical vectors to
           literal code token types. Literal code token types contain the
           actual feature names as entries instead of numerical encodings.

        Args:
            (list(list(int))): Numerical code token type vector

        Returns.
            (list(list(str))) Literal code token type vector.
        """
        return [sum([[c] * t for c, t in zip(cat.get_features(), tax)], [])
                for cat, tax in zip(self.categories.values(), taxonomy)]

    def translate_to_vec(self, literal_tax):
        """Translates literal code token types to
           numerical code token type vectors. Numerical code token type vectors
           contain numerical encodings instead of actual feature names
           as entries.

        Args:
            (list(list(str))): Literal code token type

        Returns.
            (list(list(int)))  Numerical code token type vector.
        """
        tax = list()
        for feats_lit, all_feats in zip(ast.literal_eval(str(literal_tax)), self.features):
            feature_vec = [0] * len(all_feats)
            for f_l in feats_lit:
                try:
                    feature_vec[all_feats.index(f_l)] += 1
                except ValueError:
                    continue

            feature_vec += [0] * (self.max_features - len(feature_vec))
            tax.append(feature_vec)

        return tax

    def update_categories(self, ast):
        """We won't use this function for the tree-sitter case.
        Updates the categories' attributes for each AST. Needs to be called
           in the outer loop when iterating over the ASTs for
           code token type generation.
           --> for ast in ASTs:
                    update_categories(ast)
                    for v in vertices:
                        get_taxonomy(v)


        Args:
            (AST (igraph)): AST containing vertices
        """
        if "Origin" in self.categories:
            self.categories["Origin"].ast = ast
            self.categories["Origin"].stdlibs = self.categories["Origin"]\
                                                    .get_stdlibs()
            self.categories["Origin"].extlibs = self.categories["Origin"]\
                                                    .get_extlibs()
        if "Frequency" in self.categories:
            self.categories["Frequency"].ast = ast
            self.categories["Frequency"].get_frequencies()
            self.categories["Frequency"].set_boundaries()

    def get_max_features_len(self):
        """Gets the feature number of the category with the most features.

        Args:
            None

        Returns.
            (int) maximum number of features in any category
        """
        return max([len(features) for features in self.features])

    def flatten(self, padded_two_d_tax):
        """Flattens a 2D numerical code token type vector.

        Args:
            (list(list(int))): regular numerical code token type vector
                               generated by "get_taxonomy"

        Returns.
            (list(int)) Flattened regular numerical code token type vector.
        """
        flat_tax = list()
        for vector in padded_two_d_tax:
            flat_tax += vector
        return flat_tax

    def to_two_d(self, padded_flat_tax):
        """Adds second dim to a 1D numerical code token type vector.

        Args:
            (list(int)): regular 1D numerical code token type vector

        Returns.
            (list(list(int))) 2D regular numerical code token type vector.
        """
        two_d_tax = list()
        for i, offset in enumerate([len(feat) for feat in self.features]):
            two_d_tax\
                .append(padded_flat_tax
                        [i * self.max_features:(i + 1) * self.max_features])
        return two_d_tax

    def remove_padding(self, padded_flat_tax):
        """Removes redundant positions in a 1D numerical
           code token type vector. As the categories have different numbers of
           features, feature vectors are padded so they all have the same size.
           This creates redundant positions in a feature vector which shorter
           than the maximum feature length.

        Args:
            (list(int)): regular 1D numerical code token type vector

        Returns.
            (list(int)) shortened (removed padding)1D numerical
                        code token type vector.
        """
        no_padding = list()
        for i, offset in enumerate([len(feat) for feat in self.features]):
            no_padding +=\
                padded_flat_tax[i * self.max_features:i * self.max_features + offset]
        return no_padding

    def add_padding(self, flat_tax):
        """Adds redundant positions in a 1D numerical
           code token type vector again after being removed before.

        Args:
            (list(int)): shortened 1D numerical code token type vector

        Returns.
            (list(int)) regular 1D numerical code token type vector.
        """
        padding = list()
        len_feats = [len(feat) for feat in self.features]
        for i in range(len(self.features)):
            padding +=\
                flat_tax[sum(len_feats[:i]):
                         sum(len_feats[:i]) + len_feats[i]]\
                + [0] * (self.max_features - len_feats[i])
        return padding
