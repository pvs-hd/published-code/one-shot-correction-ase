# Get token types of an AST

from scripts.ast_parser import utils

def extract_token_types(node, target_types, snippet, token_types):
    """Extract token types of a give node in a recurrsive manner.

    Args:
        node (Node): A node in an AST.
        target_types (list): A list of target token types, except boolean type.
        snippet (str): A code snippet.
        token_types (dict): A dictionary of token types.
    
    Returns:
        token_types (dict): A dictionary of token types.
    """
    if node.children:
        for n in node.children:
            if n.type == "true" or n.type == "false":
                if "boolean" not in token_types:
                    token_types["boolean"] = set()
                token_types["boolean"].add(snippet[n.start_byte:n.end_byte].decode('utf8'))
            elif n.type in target_types:
                if n.type not in token_types:
                    token_types[n.type] = set()
                token_types[n.type].add(snippet[n.start_byte:n.end_byte].decode('utf8'))
            extract_token_types(n, target_types, snippet, token_types)
    return token_types


def get_token_types(snippet, target_types):
    """Get token types of tokens in a given code snippet.

    Args:
        snippet (str): A code snippet.
        target_types (list): A list of target token types.

    Returns:
        token_types (dict): A dictionary of token types.
    """
    # convert str code snippet to bytes
    snippet = bytes(snippet, "utf8")
    parser = utils.get_ast_parser()
    tree = parser.parse(snippet)
    root_node = tree.root_node
    token_types = extract_token_types(root_node, target_types, snippet, token_types={})
    return token_types


def merge_dicts(dict1, dict2):
    """Merge two dictionaries and collect values of the same key into a set.

    Args:
        dict1 (dict): A dictionary.
        dict2 (dict): A dictionary.

    Returns:
        dict (dict): A dictionary.
    """
    if not dict1:
        return dict2
    elif not dict2:
        return dict1

    final_keys = set(dict1.keys()).union(set(dict2.keys()))
    dict = {}

    for key in final_keys:
        if key in dict1 and key in dict2:
            dict[key] = dict1[key].union(dict2[key])
        elif key in dict1:
            dict[key] = dict1[key]
        else:
            dict[key] = dict2[key]
    return dict


def get_token_types_from_list(snippets, target_types):
    """Get token types of tokens from a given list of code snippets.

    Args:
        snippets (list): A list of code snippets.
        target_types (list): A list of target token types.

    Returns:
        token_types (dict): A dictionary of token types.
    """
    token_types_list = {}
    for snippet in snippets:
        token_types = get_token_types(snippet, target_types)
        token_types_list = merge_dicts(token_types_list, token_types)
    return token_types_list