# Refer to https://gitlab.com/pvs-hd/published-code/code-token-type-taxonomy
# Collect variables defined in a code snippet


from scripts.ast_parser import utils

class VarsCollector():
    def __init__(self, tree):
        # input is a tree-sitter tree
        self.tree = tree
        self.context = [('global', set())]
        self.avail_vars = set()
        self.vars_def = {}

    def collect_vars_by_node(self, node):
        # check if the variable is defined in __init__ of a class
        # This implementation is not optimal, but it works for now
        is_init_vars_single = node.parent is not None\
            and node.parent.parent is not None\
            and node.parent.parent.parent is not None\
            and node.parent.parent.parent.parent is not None\
            and node.parent.parent.parent.parent.type == "function_definition"\
            and utils.get_text(node.parent.parent.parent.parent.named_children[0]) == "__init__"\
            and node.parent.parent.parent.parent.parent is not None\
            and node.parent.parent.parent.parent.parent.parent is not None\
            and node.parent.parent.parent.parent.parent.parent.type == "class_definition"

        is_init_vars_plural = node.parent is not None\
            and node.parent.parent is not None\
            and node.parent.parent.parent is not None\
            and node.parent.parent.parent.parent is not None\
            and node.parent.parent.parent.parent.parent is not None\
            and node.parent.parent.parent.parent.parent.type == "function_definition"\
            and utils.get_text(node.parent.parent.parent.parent.parent.named_children[0]) == "__init__"\
            and node.parent.parent.parent.parent.parent.parent is not None\
            and node.parent.parent.parent.parent.parent.parent.parent is not None\
            and node.parent.parent.parent.parent.parent.parent.parent.type == "class_definition"

        if node.type == "class_definition":
            self.context.append(('class', set()))
        elif node.type == "function_definition":
            self.context.append(('function', set()))
        elif (node.parent is not None) and (node.parent.type == "global_statement"):
            if (node.is_named) and (node.type == "identifier"):
                if self.context:
                    self.context[0][1].add(utils.get_text(node))
                    self.avail_vars.add(utils.get_text(node))
                    self.vars_def[node.id] = (utils.get_text(node), self.context[0][0])
        elif (node.parent is not None) and (node.parent.type == "assignment"):
            if node == node.parent.named_children[0]:
                if node.type not in ["tuple_pattern", "pattern_list"]:
                    if utils.get_text(node) not in self.avail_vars:
                        if is_init_vars_single:
                            pre_lvl = -2 # assumming the higher level is a class definition
                        else:
                            pre_lvl = -1 # function

                        if self.context:
                            self.context[pre_lvl][1].add(utils.get_text(node))
                            self.avail_vars.add(utils.get_text(node))
                            self.vars_def[node.id] = (utils.get_text(node), self.context[pre_lvl][0])
        elif (node.parent is not None) and (node.parent.type in ["tuple_pattern", "pattern_list"])\
            and (node.parent.parent is not None) and (node.parent.parent.type == "assignment"):
            if node.parent == node.parent.parent.named_children[0]:
                if utils.get_text(node) not in self.avail_vars:
                    if is_init_vars_plural:
                        pre_lvl = -2 # assumming the higher level is a class definition
                    else:
                        pre_lvl = -1
                    if self.context:
                        self.context[pre_lvl][1].add(utils.get_text(node))
                        self.avail_vars.add(utils.get_text(node))
                        self.vars_def[node.id] = (utils.get_text(node), self.context[pre_lvl][0])
        elif (node.parent is not None) and (node.parent.type in ["for_statement", "for_in_clause"]):
            if node == node.parent.named_children[0]:
                if node.type == "identifier":
                    if self.context:
                        self.context[-1][1].add(utils.get_text(node))
                        self.avail_vars.add(utils.get_text(node))
                        self.vars_def[node.id] = (utils.get_text(node), self.context[-1][0])
        elif (node.parent is not None) and (node.parent.type == "as_pattern_target"): # with open(...) as ...
            if (node.parent.parent is not None) and (node.parent.parent.type == "as_pattern"):
                if (node.parent.parent.parent is not None) and (node.parent.parent.parent.type == "with_item"):
                    if (node.parent.parent.parent.parent is not None) and (node.parent.parent.parent.parent.type == "with_clause"):
                        if (node.parent.parent.parent.parent.parent is not None) and (node.parent.parent.parent.parent.parent.type == "with_statement"):        
                                if node.parent == node.parent.parent.named_children[1]:
                                    if node.type == "identifier":
                                        if self.context:
                                            self.context[-1][1].add(utils.get_text(node))
                                            self.avail_vars.add(utils.get_text(node))
                                            self.vars_def[node.id] = (utils.get_text(node), self.context[-1][0])
        
        # recursively collect variables in children
        for child in node.named_children:
            self.collect_vars_by_node(child)
        
        # update the scope of variables
        # in tree-sitter, "block" is the final named child of a class/function definition, if, for, while, etc.
        # after handling all the children of "block", the current scope is popped if it is a class/function definition
        end_of_scope_init = node.type == "block"\
            and\
            (
                node.parent is not None\
                    and node.parent.type == "function_definition"\
                    and node.parent.parent is not None\
                    and node.parent.parent.parent is not None\
                    and node.parent.parent.parent.type == "class_definition"\
                    and utils.get_text(node.parent.named_children[0]) == "__init__"
            )
        end_of_scope = node.type == "block"\
            and\
            (
                node.parent is not None\
                    and node.parent.type in ["class_definition", "function_definition"]
            )
        if end_of_scope_init:
            removed_vars = set()
            if self.context:
                removed_vars.update(self.context[-1][1])
                self.context.pop()

            # only remove variables that don't start with "self."
            removed_vars = set(filter(lambda x: not x.startswith("self."), removed_vars))
            self.avail_vars.difference_update(removed_vars)
        elif end_of_scope:
            removed_vars = set()
            if self.context:
                removed_vars.update(self.context[-1][1])
                self.context.pop()
            self.avail_vars.difference_update(removed_vars)

    def collect_vars(self):
        root_node = self.tree.root_node
        self.collect_vars_by_node(root_node)

