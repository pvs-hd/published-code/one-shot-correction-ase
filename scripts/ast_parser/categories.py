# Refer to https://gitlab.com/pvs-hd/published-code/code-token-type-taxonomy


import keyword
import builtins
from collections import OrderedDict
from scripts.ast_parser import utils


class _BaseCategory(object):
    def __init__(self):
        """Base class for categories implementing shared attributes and
           methods.

        Args:
            None
        """
        self.feature_vec_len = None
        self.conditions = OrderedDict()
        self.taxed = 0
        self.add_conditions()

    def set_feature_vec_len(self, len):
        """Set feature vector length of the category. Equals the number of
           featres in the particular category.
        Args:
            int: Feature length.

        Returns:
            (void)
        """
        self.feature_vec_len = len

    def get_features(self):
        """Get the features of the particular category.
        Args:
            None

        Returns:
            (list) List with the category's features
        """
        return ["_".join(x.split('_')[1:])
                for x in dir(self) if x.split('_')[0] == "is"]

    def add_conditions(self):
        """Add the conditions to check (feature methods) of the particular
           category to the conditions attribute.
        Args:
            None

        Returns:
            (void)
        """
        condition_methods = [x for x in dir(self) if x.split("_")[0] == "is"]
        for i, method in enumerate(condition_methods):
            self.conditions[i] = getattr(self, method)

    def get_feature_vec(self, *args):
        """Get the feature vector. Combining the feature vectors of different
           categories represent a code token's code token type.
        Args:
            as needed ; usually --> (tree-sitter node, int): node,
                                                            node's index

        Returns:
            (list(int)) 1D feature vector
        """
        self.taxed = 0
        feature_vector = [0] * self.feature_vec_len
        for vec_pos, con in self.conditions.items():
            if con(*args):
                feature_vector[vec_pos] = 1 * con(*args)
                self.taxed = 1
        return feature_vector

    def get_ancestors(self, node):
        """Root path wihtout node itself.
        Args:
            node (tree-sitter node): The node to investigate.

        Returns:
            (list(tree-sitter node)) Ancestors of node
        """
        return [node.parent] + self.get_ancestors(node.parent) if not utils.is_root(node.parent) else []

    def get_ancestor_types(self, node):
        """Root path wihtout node itself.
        Args:
            node (tree-sitter node): The node to investigate.

        Returns:
            (list(str) Ancestor node's node types
        """
        return [n.type for n in self.get_ancestors(node)]

    def get_ith_child_of_parent(self, node, i):
        """Get the ith child of the node's parent.
        Args:
            node (tree-sitter node): The node to investigate.
            i (int): The index of the child to get. 0 is the first child.

        Returns:
            (tree-sitter node) the ith child of the node's parent. None otherwise.
        """
        if node.parent is None:
            return None

        if (i < 0) or (i >= node.parent.child_count):
            return None

        return node.parent.children[i]

    def get_ith_named_child_of_parent(self, node, i):
        """Get the ith named child of the node's parent.
        Args:
            node (tree-sitter node): The node to investigate.
            i (int): The index of the child to get. 0 is the first child.

        Returns:
            (tree-sitter node) the ith named child of the node's parent. None otherwise.
        """
        if node.parent is None:
            return None

        if (i < 0) or (i >= node.parent.named_child_count):
            return None

        return node.parent.named_children[i]

    def get_ith_ancestor(self, node, i):
        """Get the ith ancestor of the node.
        Args:
            node (tree-sitter node): The node to investigate.
            i (int): The index of the ancestor to get. 0 is the first ancestor, i.e. direct parent.

        Returns:
            (tree-sitter node) the ith ancestor of the node. None otherwise.
        """
        if node.parent is None:
            return None

        ancestors = self.get_ancestors(node)

        if (i < 0) or (i >= len(ancestors)):
            return None

        return ancestors[i]
    
    def get_nearest_ancestor_call_name(self, node):
        """Get the name of the nearest ancestor call of the node.

        Args:
            node (tree-sitter node): The node to investigate.

        Returns:
            (str) the name of the nearest ancestor call of the node. None otherwise.
        """
        ancestors = self.get_ancestors(node)

        for ancestor in ancestors:
            if ancestor.type == "call":
                return utils.get_text(ancestor.named_children[0])

        return None


class SyntaxType(_BaseCategory):
    """Category checks syntax type of AST node.

    Args:
        None
    """

    def __init__(self):
        super().__init__()
        self.python_keywords = set(keyword.kwlist + ["self"])
        self.exceptions = [name for name, value in builtins.__dict__.items()
                           if isinstance(value, type) and issubclass(value,
                                                                     BaseException)]
        
        # function name as keyword arguments
        # extract from built-in functions in Python
        # e.g. key=len
        self.keyword_args = {
            "key": ["max", "min", "sorted"],
            "fget": ["property"],
            "fset": ["property"],
            "fdel": ["property"]
        }

    def is_arg_def(self, *args):
        """Check if a node is an argument definition

            A:  no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,
                is leaf node,
                has nodetype: "identifier",
                not exception,
                not python_keyword,
            AND
            
            (B: normal args
                node's parent's type is "parameters"
                (parameter in func def or method def)
            OR

            C:  default args
                node's parent's type is "default_parameter"
                "parameters" in node's ancestor types
                the node is the first child of its parent
                (arg=value)

            D:  *args
                node's parent's type is "list_splat_pattern"
                "parameters" in node's ancestor types
                the node is the second child of its parent
                the first child of its parent has nodetype "*"
                (vararg)

            OR

            E:  **kwargs
                node's parent's type is "dictionary_splat_pattern"
                "parameters" in node's ancestor types
                the node is the second child of its parent
                the first child of its parent has nodetype "**"
                (kwarg)

            OR
            F:  lambda args
                node's parent's type is "lambda_parameters"

            OR
            G:  node's parent's type is "keyword_argument",
                "argument_list" in node's ancestor types,
                "call" in node's ancestor types,
                node is the first named child of its parent
                (a_func_call(1, 2, arg_def=value)))
            )
            A AND (B OR C OR D OR E OR F OR G)

        Args:
            as needed ; usually --> (tree-sitter node), int): node,
                                                            node's index

        Returns:
            (bool) True if the node is an argument definition.
        """
        node, node_index, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and utils.get_text(node)\
            and utils.get_text(node) not in self.exceptions\
            and utils.get_text(node) not in self.python_keywords\
            and\
            (
                (node.parent.type == "parameters")\
                or\
                (
                    node.parent.type == "default_parameter"\
                        and "parameters" in self.get_ancestor_types(node)\
                        and node == self.get_ith_child_of_parent(node, 0)
                )
                or\
                (
                    node.parent.type == "list_splat_pattern"\
                        and "parameters" in self.get_ancestor_types(node)\
                        and node == self.get_ith_child_of_parent(node, 1)\
                        and self.get_ith_child_of_parent(node, 0).type == "*"
                )
                or\
                (
                    node.parent.type == "dictionary_splat_pattern"\
                        and "parameters" in self.get_ancestor_types(node)\
                        and node == self.get_ith_child_of_parent(node, 1)\
                        and self.get_ith_child_of_parent(node, 0).type == "**"
                )
                or\
                (
                    node.parent.type == "lambda_parameters"
                )
                or\
                (
                    node.parent.type == "keyword_argument"\
                        and "argument_list" in self.get_ancestor_types(node)\
                        and "call" in self.get_ancestor_types(node)\
                        and node == self.get_ith_named_child_of_parent(node, 0)
                )
            )
            
    def is_attribute(self, *args):
        """Check if a node is an attribute
           (e.g. qualifier.an_attribute)

           A:   no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,
                is leaf node,
                has nodetype: "identifier",
                node's parent's type is "attribute",
                the node is the third child of its parent,
                the second child is not None and is a dot.

           AND

           (B:  attribute without function call
                no "call" in node's ancestor types

            OR

            C:  attribute with function call
                "call" in node's ancestor types,
                node's grandparent is not None,
                node's grandparent's type is not "call"
                (AClass.attr.method(var))
            )

        Returns:
            (bool) True if node is a attribute false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and node.parent.type == "attribute"\
            and node == self.get_ith_child_of_parent(node, 2)\
            and self.get_ith_child_of_parent(node, 1) is not None\
            and self.get_ith_child_of_parent(node, 1).type == "."\
            and\
            (
                ("call" not in self.get_ancestor_types(node))\
                or\
                (
                    "call" in self.get_ancestor_types(node)\
                        and node.parent.parent is not None\
                        and node.parent.parent.type != "call"
                )
            )

    def is_class_def(self, *args):
        """Check if a node is a class definition.
            no other feature has already been True
            -->(security measure; prevents double categorizations),
            not root node,
            is leaf node,
            has nodetype: "identifier",
            node's parent's type is "class_definition",
            the node is the second child of its parent,

        Args:
            as needed ; usually --> (tree-sitter node, int): node,
                                                                   node's index

        Returns:
            (bool) True if the node is a class definition, false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and node.parent.type == "class_definition"\
            and node == self.get_ith_child_of_parent(node, 1)

    def is_class_usg(self, *args):
        """Check if a node is a class usage.

            A:   no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,
                is leaf node,
                has nodetype: "identifier",
            AND
            (
                B:  node's parent's type is "argument_list",
                    node's grandparent is not None,
                    node's grandparent's type is "class_definition",
                    (base class, e.g. def AnotherClass(BaseClassUsage))
                OR
                (
                    C:  not exception,
                        not python keyword,
                        "call" in ancestor node types,
                        first char is an upper case
                    AND
                    (
                        D:  node's parent's type is "call",
                            the node is the first child of its parent
                            (e.g. a_var = ClassUage())
                        OR
                        E:  node's grandparent is not None,
                            node's grandparent's type is "call",
                            node's parent is the first child of its grandparent
                            (e.g. Lib.SubLib.ClassUsage())
                    )   
                )
                OR
                (
                    F:  not exception,
                        not python_keyword,
                        first char is an upper case,
                        node's parent's type is "attribute",
                        the node is the first child of its parent,
                        the second child is not None and is a dot                        
                        (e.g. a_var = ClassUsage.an_attribute
                        or resutls = ClassUsage.a_method())
                )
            )
            A and (B or (C and (D or E)) or (F))
        Args:
            as needed ; usually --> (vertex (igraph.Vertex), int): node,
                                                                   node's index

        Returns:
            (bool) True if AST node is a class usage,false otherwise.
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and\
            (
                (
                    node.parent.type == "argument_list"\
                        and node.parent.parent is not None\
                        and node.parent.parent.type == "class_definition"
                )
                or\
                (
                    utils.get_text(node)\
                    and utils.get_text(node) not in self.exceptions\
                        and utils.get_text(node) not in self.python_keywords\
                        and "call" in self.get_ancestor_types(node)\
                        and utils.get_text(node)[0].isupper()\
                        and\
                        (
                            (node.parent.type == "call"\
                                and node == self.get_ith_child_of_parent(node, 0))
                            or\
                            (node.parent.parent is not None\
                                and node.parent.parent.type == "call"\
                                and node.parent == self.get_ith_child_of_parent(node.parent, 0))
                        )
                )
                or\
                (
                    utils.get_text(node) not in self.exceptions\
                        and utils.get_text(node) not in self.python_keywords\
                        and utils.get_text(node)\
                        and utils.get_text(node)[0].isupper()\
                        and node.parent.type == "attribute"\
                        and node == self.get_ith_child_of_parent(node, 0)\
                        and self.get_ith_child_of_parent(node, 1) is not None\
                        and self.get_ith_child_of_parent(node, 1).type == "."\
                )
            )

    def is_const_float(self, *args):
        """Check if a node is a constant float (e.g. 1.5).
            no other feature has already been True
            -->(security measure; prevents double categorizations),
            not root node,
            is leaf node,
            has nodetype: "float"

        Args:
            as needed ; usually --> (tree-sitter node, int): node,
                                                            node's index

        Returns:
            (bool) True if the node is a constant float, false otherwise
            """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "float"

    def is_const_int(self, *args):
        """Check if a node is a constant integer (e.g. 1).
            no other feature has already been True
            -->(security measure; prevents double categorizations),
            not root node,
            is leaf node,
            has nodetype: "integer"

        Args:
            as needed ; usually --> (tree-sitter node, int): node,
                                                            node's index

        Returns:
            (bool) True if the node is a constant integer, false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "integer"

    def is_const_str(self, *args):
        """Check if a node is a literal (string constant)
            no other feature has already been True
            -->(security measure; prevents double categorizations),
            not root node,
            is leaf node,
            has nodetype: "string"

        Args:
            as needed ; usually --> (tree-sitter node), int): node,
                                                            node's index

        Returns:
            (bool) True if the node is a literal, false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "string"

    def is_exception(self, *args):
        """Check if a node is an exception (e.g. except someException).
            no other feature has already been True
            -->(security measure; prevents double categorizations),
            not root node,
            is leaf node,
            has nodetype: "identifier",

            AND (
                (node's parent's type is "except_clause",
                the node is the second child of its parent,
                e.g. except someException)

                OR

                (node's parent's type is "tuple",
                node's grandparent is not None and has type "except_clause"
                e.g. except (someException, anotherException):...)

                OR

                ("except_clause" is in node's ancestor types,
                "as_pattern" is in node's ancestor types,
                e.g. except someException as someException)

                OR

                ("raise_statement" is in node's ancestor types,
                "argument_list" is not in node's ancestor types,
                e.g. raise someException(optional_args))
            )

        Args:
            as needed ; usually --> (tree-sitter node), int): node,
                                                            node's index

        Returns:
            (bool) True if the node is an exception, false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and\
            (
                (node.parent.type == "except_clause"\
                    and node == self.get_ith_child_of_parent(node, 1))\
                or\
                (node.parent.type == "tuple"\
                    and node.parent.parent is not  None\
                    and node.parent.parent.type == "except_clause")\
                or\
                ("except_clause" in self.get_ancestor_types(node)\
                    and "as_pattern" in self.get_ancestor_types(node))\
                or\
                ("raise_statement" in self.get_ancestor_types(node)\
                    and "argument_list" not in self.get_ancestor_types(node))
            )

    def is_func_def(self, *args):
        """Check if a node is function def.
           no other feature has already been True
           -->(security measure; prevents double categorizations),
           not root node,
           is leaf node,
           has nodetype: "identifier",
           node's parent's type is "function_definition",
           node is the second child of its parent,
           "class_definition" not in ancestor node types (i.e. method def)

        Args:
            as needed ; usually --> (tree-sitter, int): node,
                                                        node's index

        Returns:
            (bool) True if the node is function def false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and node.parent.type == "function_definition"\
            and node == self.get_ith_child_of_parent(node, 1)\
            and "class_definition" not in self.get_ancestor_types(node)

    def is_func_usg(self, *args):
        """Check if a node is a function call
           no other feature has already been True
           -->(security measure; prevents double categorizations),
           not root node,
           is leaf node,
           has nodetype: "identifier",
           first char of node's symbol is NOT upper case,
           and(
                A: 
                node's parent's type is "call",
                node is the first child of its parent

                OR

                B: 
                node's parent's type is "keyword_argument",
                node is the second named child of its parent,
                the first named child of its parent has type "identifier" and has text in the keyword_args dict,
                the name of the nearest ancestor call of its parent is in the list of functions of the first child of its parent,
                defined by the keyword_args dict
                (function as keyword argument, e.g. key=key_func, or fget=getter_func, etc.)
            )       

        Returns:
            (bool) True if the node is a function call, false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and utils.get_text(node)\
            and not utils.get_text(node)[0].isupper()\
            and\
            (
                (node.parent.type == "call"\
                and node == self.get_ith_child_of_parent(node, 0))\
                or\
                (node.parent.type == "keyword_argument"\
                 and node == self.get_ith_named_child_of_parent(node, 1)\
                 and self.get_ith_named_child_of_parent(node, 0).type == "identifier"\
                 and utils.get_text(self.get_ith_named_child_of_parent(node, 0)) in self.keyword_args\
                 and self.get_nearest_ancestor_call_name(node.parent) in self.keyword_args[utils.get_text(self.get_ith_named_child_of_parent(node, 0))])
            )

    def is_imp_alias(self, *args):
        """Check if a node is an import ID (i.e. alias, e.g. import some_lib as imp_alias)
            no other feature has already been True
            -->(security measure; prevents double categorizations),
            not root node,
            is leaf node,
            has nodetype: "identifier",
            node's parent's type is "aliased_import",
            node is the third child of its parent,
            the second child of its parent is not None and has type "as"

        Args:
            as needed ; usually --> (tree-sitter node, int): node,
                                                            node's index

        Returns:
            (bool) True if the node is an import alias false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and node.parent.type == "aliased_import"\
            and node == self.get_ith_child_of_parent(node, 2)\
            and self.get_ith_child_of_parent(node, 1) is not None\
            and self.get_ith_child_of_parent(node, 1).type == "as"

    def is_imp_lib(self, *args):
        """Check if a node is a module (i.e. imported lib)
            no other feature has already been True
            -->(security measure; prevents double categorizations),
            not root node,
            is leaf node,
            has node type: "identifier",
            node's parent's type is "dotted_name"
            AND
            (
                "import_statement" is in ancestor node types
                OR
                "import_from_statement" is in ancestor node types
            )
            AND NOT
            (
                node is a sub import
                OR
                node is an import ID (i.e. import alias)
            )

        Args:
            as needed ; usually --> (tree-sitter node), int): node,
                                                            node's index

        Returns:
            (bool) True if the node is a module false otherwise
        """
        node, _, vars_def = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and node.parent.type == "dotted_name"\
            and\
            (
                "import_statement" in self.get_ancestor_types(node)\
                or\
                "import_from_statement" in self.get_ancestor_types(node)
            )\
            and not\
            (
                self.is_imp_sublib(node, node.id, vars_def)\
                or\
                self.is_imp_alias(node, node.id, vars_def)
            )

    def is_imp_sublib(self, *args):
        """Check if a node is a sub import.
            We don't consider wildcards as sub imports (e.g. from some_lib import *).            
            no other feature has already been True
            -->(security measure; prevents double categorizations),
            not root node,
            is leaf node,
            has node type: "identifier",
            node's parent's type is "dotted_name",
            "import_from_statement" is in ancestor node types,
            grandparent is not None
            AND
            (
                A: node's grandparent's type is "import_from_statement",
                    node's parent is not the first NAMED CHILD of node's grandparent.
                    (e.g. from lib import sublib)
                OR
                B: node's grandparent's type is "aliased_import",
                    node's grandparent is not the first NAMED CHILD of its grand-grandparent.
                    (from lib import sublib as alib)
            )

        Args:
            as needed ; usually --> (tree-sitter node), int): node,
                                                            node's index

        Returns:
            (bool) True if the node is sub import, false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and node.parent.type == "dotted_name"\
            and "import_from_statement" in self.get_ancestor_types(node)\
            and node.parent.parent is not None\
            and\
            (
                (node.parent.parent.type == "import_from_statement"\
                    and node.parent != self.get_ith_named_child_of_parent(node.parent, 0))
                or\
                (
                    node.parent.parent.type == "aliased_import"\
                        and node.parent.parent != self.get_ith_named_child_of_parent(node.parent.parent, 0)
                )
            )

    def is_keyword(self, *args):
        """Check if a node is python keyword (e.g. for)

        Args:
            as needed ; usually --> (tree-sitter node), int): node,
                                                            node's index

        Returns:
            (bool) True if the node is python keyword false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and utils.get_text(node) \
            and utils.get_text(node) in self.python_keywords

    def is_method_def(self, *args):
        """Check if a node is method def
           no other feature has already been True
           -->(security measure; prevents double categorizations),
           not root node,
           leaf node,
           has node type: "identifier",
           node's parent's type is "function_definition",
           the node is the first NAMED child of its parent,
           "class_definition" is in ancestor node types

        Args:
            as needed ; usually --> (tree-sitter node), int): node,
                                                            node's index

        Returns:
            (bool) True if the node is method def, false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and node.parent.type == "function_definition"\
            and node == self.get_ith_named_child_of_parent(node, 0)\
            and "class_definition" in self.get_ancestor_types(node)

    def is_method_usg(self, *args):
        """Check if a node is a method call
           no other feature has already been True
           -->(security measure; prevents double categorizations),
           not root node,
           leaf node,
           has node type "identifier",
           node's parent's type is "attribute",
           node's grandparent is not None and has type "call",
           the node is the second NAMED child of its parent,
           first char of node's symbol is NOT upper case

        Returns:
            (bool) True if the node is a method call, false otherwise
        """
        node, _, _ = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and node.parent.type == "attribute"\
            and node.parent.parent is not None\
            and node.parent.parent.type == "call"\
            and node == self.get_ith_named_child_of_parent(node, 1)\
            and utils.get_text(node)\
            and not utils.get_text(node)[0].isupper()

    def is_var_def(self, *args):
        """Check if a node is a var definition.

            A:  no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,
                not exception,
                not python_keyword,

            AND
            (
                B:  has node type "identifier" or "attribute",,
                    node id in vars_def (i.e. a dict of vars def in the file)
                    (a = ... or self.a = ...)

                OR

                C:  is leaf node,
                    has node type "identifier",
                    node's parent's type is "global_statement",
                    (global variable)
            )
        Args:
            as needed ; usually --> (tree-sitter node), int): node,
                                                            node's index

        Returns:
            (bool) True if the node is a var definition, false otherwise.
        """
        node, _, vars_def = args

        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.get_text(node)\
            and utils.get_text(node) not in self.exceptions\
            and utils.get_text(node) not in self.python_keywords\
            and\
            (
                (node.type in ["identifier", "attribute"]\
                    and node.id in vars_def)
                or\
                (utils.is_leaf(node)\
                    and node.type == "identifier"\
                    and node.parent.type == "global_statement")
            )

    def is_var_usg(self, *args):
        """Check if a node is a var usage.

            (A:  no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,
                is leaf node,
                has nodetype "identifier",
                not exception,
                not python_keyword,

           AND

           (
           B:   "argument_list" in node's ancestor types,
                "call" in node's ancestor types,
                (parameter in func call)

           OR

           C:   not "call" is node's ancestor types,
                not "subscript" in node's ancestor types,
                not (node's parent's type is "argument_list" and 
                    node's grandparent is not None and has type "class_definition")
                not "attribute" in node's ancestor types
                not a var definition
                (normal variable)

           OR

           D:   "subscript" in node's ancestor types,
                not "attribute" in node's ancestor types
                (Subscripts, e.g. a[var_usg])

            OR

            E:   "slice" in node's ancestor types,
                not "attribute" in node's ancestor types
                (Slices, e.g. a[var_usg:var_usg])

           F:   first char is not an upper case (i.e. not a class)
                node's parent's type is "attribute",
                the node is the first NAMED child of its parent
                (var.method() or var.attribute)

           OR

           G:   parent's type is "keyword_argument"
                the node is the second named child of its parent
                (keyword argument in func call, e.g. func(a, b=var_usg))

            OR

            I:  parent's type is "for_in_clause"
                the node is the second named child of its parent
                (one line for loop, e.g. sum(1 for c in string if c.isupper()))
            ))

            OR
            (H:
                no other feature has already been True
                -->(security measure; prevents double categorizations),
                not root node,
                has nodetype "attribute",
                not exception,
                not python_keyword,
                node's parent's type is not "call" or "attribute" (i.e. not self.method() or self.attr1 of self.attr1.attr2),
                node's text start with "self"
                (self.[a.b...]c)
            )

           (A and (B or C or D or E or F or G or I)) or H

        Args:
            as needed ; usually --> (tree-sitter node, int): node,
                                                            node's index

        Returns:
            (bool) True if the node is a var definition, false otherwise.
        """
        node, _, vars_def = args

        return\
            (self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"\
            and utils.get_text(node)\
            and utils.get_text(node) not in self.exceptions\
            and utils.get_text(node) not in self.python_keywords\
            and\
            (
                ("argument_list" in self.get_ancestor_types(node)\
                    and "call" in self.get_ancestor_types(node))
                or\
                ("call" not in self.get_ancestor_types(node)\
                    and "subscript" not in self.get_ancestor_types(node)\
                    and not (node.parent.type == "argument_list"\
                        and node.parent.parent is not None\
                        and node.parent.parent.type == "class_definition")\
                    and "attribute" not in self.get_ancestor_types(node)\
                    and node.id not in vars_def)
                or\
                ("subscript" in self.get_ancestor_types(node)\
                    and "attribute" not in self.get_ancestor_types(node))
                or\
                ("slice" in self.get_ancestor_types(node)\
                    and "attribute" not in self.get_ancestor_types(node))
                or\
                (not utils.get_text(node)[0].isupper()\
                    and node.parent.type == "attribute"\
                    and node == self.get_ith_named_child_of_parent(node, 0))
                or\
                (node.parent.type == "keyword_argument"\
                    and node == self.get_ith_named_child_of_parent(node, 1))
                or\
                (node.parent.type == "for_in_clause"\
                    and node == self.get_ith_named_child_of_parent(node, 1))
            ))\
            or\
            (
                self.taxed == 0\
                and not utils.is_root(node)\
                and node.type == "attribute"\
                and utils.get_text(node)\
                and utils.get_text(node) not in self.exceptions\
                and utils.get_text(node) not in self.python_keywords\
                and node.parent.type not in ["call", "attribute"]\
                and utils.get_text(node).startswith("self")
            )

    def is_znknown(self, *args):
        """Check if a node is a leaf node with a code token
           that has not been cateorized
           (znknown so it is evaluated last)
           no other feature has already been True
            -->(security measure; prevents double categorizations),
            not root node,
            is leaf node,
            has nodetype "identifier",

        Args:
            as needed ; usually --> (tree-sitter node, int): node,
                                                            node's index

        Returns:
            (bool) True if the node an uncategorized code tokfalse otherwise
        """
        node, _, _ = args
        return self.taxed == 0\
            and not utils.is_root(node)\
            and utils.is_leaf(node)\
            and node.type == "identifier"