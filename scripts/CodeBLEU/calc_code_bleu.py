# Copyright (c) Microsoft Corporation. 
# Licensed under the MIT license.

# -*- coding:utf-8 -*-
from scripts.CodeBLEU import bleu
from scripts.CodeBLEU import weighted_ngram_match
from scripts.CodeBLEU import syntax_match
from scripts.CodeBLEU import dataflow_match

def calculate_code_bleu(target_code_files, gen_code_file, lang, params=[0.25,0.25,0.25,0.25], weights=(0.25, 0.25, 0.25, 0.25)):
    alpha = params[0]
    beta = params[1]
    gamma = params[2]
    theta = params[3]

    # preprocess inputs
    pre_references = [[x.strip() for x in open(file, 'r', encoding='utf-8').readlines()] \
                    for file in target_code_files]
    hypothesis = [x.strip() for x in open(gen_code_file, 'r', encoding='utf-8').readlines()]

    for i in range(len(pre_references)):
        assert len(hypothesis) == len(pre_references[i])

    references = []
    for i in range(len(hypothesis)):
        ref_for_instance = []
        for j in range(len(pre_references)):
            ref_for_instance.append(pre_references[j][i])
        references.append(ref_for_instance)
    assert len(references) == len(pre_references)*len(hypothesis)


    # calculate ngram match (BLEU)
    tokenized_hyps = [x.split() for x in hypothesis]
    tokenized_refs = [[x.split() for x in reference] for reference in references]

    ngram_match_score = bleu.corpus_bleu(tokenized_refs,tokenized_hyps, weights=weights)

    # calculate weighted ngram match
    keyword_folder = "D:/PythonProjects/one-shot-correction-for-nl-to-code/scripts/CodeBLEU/keywords/"
    keywords = [x.strip() for x in open(keyword_folder+lang+'.txt', 'r', encoding='utf-8').readlines()]
    def make_weights(reference_tokens, key_word_list):
        return {token:1 if token in key_word_list else 0.2 \
                for token in reference_tokens}
    tokenized_refs_with_weights = [[[reference_tokens, make_weights(reference_tokens, keywords)]\
                for reference_tokens in reference] for reference in tokenized_refs]

    weighted_ngram_match_score = weighted_ngram_match.corpus_bleu(tokenized_refs_with_weights,tokenized_hyps, weights=weights)

    # calculate syntax match
    syntax_match_score = syntax_match.corpus_syntax_match(references, hypothesis, lang)

    # calculate dataflow match
    dataflow_match_score = dataflow_match.corpus_dataflow_match(references, hypothesis, lang)

    print('ngram match: {0}, weighted ngram match: {1}, syntax_match: {2}, dataflow_match: {3}'.\
                        format(ngram_match_score, weighted_ngram_match_score, syntax_match_score, dataflow_match_score))

    code_bleu_score = alpha*ngram_match_score\
                    + beta*weighted_ngram_match_score\
                    + gamma*syntax_match_score\
                    + theta*dataflow_match_score

    print('code_bleu: {0}'.format(code_bleu_score))
    return code_bleu_score, ngram_match_score, weighted_ngram_match_score, syntax_match_score, dataflow_match_score