# given a NL string, return a code snippet


import json
from scripts.nl_to_code import codex_code_gen
from scripts.ast_parser import utils
from ordered_set import OrderedSet
import pickle


def get_nl2code_cache(nl2code_cache_file):
    """Get NL to code cache.
    """
    try:
        with open(nl2code_cache_file, "rb") as f:
            nl2code_cache = pickle.load(f)
            print("Len of nl2code cache: ", len(nl2code_cache))
    except FileNotFoundError:
        print("Creating new nl2code cache")
        nl2code_cache = {}
        with open(nl2code_cache_file, "wb") as f:
            pickle.dump(nl2code_cache, f)

    return nl2code_cache


def get_codes_single_input(nl_query,
                           query_embd, nl2code_cache_files,
                           order_logprobs=False, discard_extra_code_snippets=True, function_only=False,
                           remove_python_mark=True,
                            rate_limit_per_minute=20,
                            **kwargs):
    """ Get codes for a NL query.

    Args:
        nl_query (str): NL query.
        query_embd (list): embedding of the NL query.
        nl2code_cache_files (list): list of paths to cache files for NL to code.
        order_logprobs (bool): whether to order the generated codes by logprobs.
        discard_extra_code_snippets (bool): whether to discard extra code snippets, 
            i.e. unnecessary functions in the generated code,
            only use this function when the target code is a single function.
        function_only (bool): whether to only keep function code snippets.
        remove_python_mark (bool): whether to remove the python mark in the generated code.
        rate_limit_per_minute (int): rate limit per minute.
        kwargs: other arguments needed for code generation.
    
    Returns:
        list: list of generated codes for the query.
    """
    cached_codes = []
    for nl2code_cache_file in nl2code_cache_files:
        nl2code_cache = get_nl2code_cache(nl2code_cache_file)
        if tuple(query_embd) in nl2code_cache.keys():
            cached_codes = nl2code_cache[tuple(query_embd)]
            break
    
    if cached_codes:
        generated_codes = cached_codes
        print("Code for the query is in cache, using cached code...")
    else:
        print("Code for the query is not in cache, generating with nl2code model...")
        generated_codes = codex_code_gen.get_codes_single_input(nl_query, rate_limit_per_minute, **kwargs)

        # store the new generated codes to cache if not empty
        if generated_codes:
            nl2code_cache[tuple(query_embd)] = generated_codes
            with open(nl2code_cache_file, "wb") as cache_file:
                pickle.dump(nl2code_cache, cache_file)

            if "test_suite" in nl2code_cache_file:
                print("Len of nl2code cache test suite changed to: ", len(nl2code_cache))

    if remove_python_mark: # this condition should be considered first
        for i, code in enumerate(generated_codes):
            generated_codes[i] = codex_code_gen.remove_python_mark(code)

    if function_only: # inapplicable for gpt-3.5-turbo case
        generated_codes = codex_code_gen.functions_only(generated_codes)

    if order_logprobs: # inapplicable for gpt-3.5-turbo case
        generated_codes = codex_code_gen.order_logprobs(generated_codes)

    if discard_extra_code_snippets:
        for idx, code in enumerate(generated_codes):
            generated_codes[idx] = codex_code_gen.discard_extra_codes(code)

    return generated_codes


def filter_results(code_snippets, top_k=1):
    """Filter the returned codes by top k.

    Args:
        code_snippets (list): list of code snippets.
        top_k (int): number of code snippets to be considered as top results.

    Returns:
        list(str): top-k recommended code snippets.
    """
    # discard the first code snippet if it's empty
    code_snippets = utils.discard_first_empty_items(code_snippets)

    if top_k > len(code_snippets):
        top_k = len(code_snippets)

    recomm_codes = code_snippets[0:top_k]

    return recomm_codes


def get_codes_with_options_single_input(nl_query, query_embd, nl2code_cache_files,
                                        remove_python_mark=True,
                                        order_logprobs=False, discard_extra_code_snippets=True, top_k=1, function_only=False,
                                        rate_limit_per_minute=20, **kwargs):
    """Get codes for a given NL query.

    Args:
        nl_query (str): NL query.
        order_logprobs (bool): whether to order the generated codes by logprobs, default is True.
        discard_extra_code_snippets (bool): whether to discard extra code snippets, default is True.
        top_k (int): number of code snippets to be considered as top results.
        add_quote (bool): whether to add triple quotes to the nl query.
        function_only (bool): whether to only keep function code snippets.

    Returns:
        list(str): top-k recommended code snippets.
    """    
    # get codes
    generated_codes = get_codes_single_input(nl_query, query_embd, nl2code_cache_files,
                                             order_logprobs, discard_extra_code_snippets, function_only,
                                             remove_python_mark,
                                             rate_limit_per_minute, **kwargs)

    # filter results
    return filter_results(generated_codes, top_k)