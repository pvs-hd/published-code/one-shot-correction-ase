# Translating nl to code with corrections from users

from scripts.ui_simulator import nl_to_code_with_options as nl2code
from scripts.one_shot_correction.correction_ds import CorrectionDS
from scripts.one_shot_correction import nl_embedding
from ordered_set import OrderedSet
from scripts.one_shot_correction.chunking import queries_comparison
from scripts.one_shot_correction.chunking import nlp_setting
from scripts.one_shot_correction.chunking import nl_code_mapping
from scripts.one_shot_correction.chunking import code_building
from scripts.ast_parser import utils as ast_utils
import copy


def get_codes_with_correction(correction_info, nl_query, query_embd, nl2code_cache_files,
                              remove_python_mark,
                              messages,
                              query_prefix,
                              order_logprobs, discard_extra_code_snippets, top_k, combine=True, function_only=False,
                              rate_limit_per_minute=20, **kwargs):
    """Get generated codes for a given nl query when the nl query:
    (1) is in the correction datastore, or
    (2) is not in the correction datastore and has no knns. correction_info is None.

    Args:
        correction_info (dict): correction info, refer to CorrectionDS for the format.
        nl_query (str): the nl query.
        query_embd (np.array): the embedding of the nl query.
        nl2code_cache_files (dict): cache files for nl2code.
        remove_python_mark (bool): whether to remove the python mark in the generated code, for gpt-3.5-turbo.
        messages (list(dict)): conversation between user and system, for gpt-3.5-turbo.
        query_prefix (str): prefix for the nl query, for gpt-3.5-turbo.
        order_logprobs (bool): whether to order the code snippets by log probability.
        discard_extra_code_snippets (bool): whether to discard extra code snippets.
        top_k (int): number of code snippets to return.
        combine (bool): whether to combine the correction info and generated info in terms of possible values.
        function_only (bool): whether to only keep function code snippets.
        rate_limit_per_minute (int): rate limit for openai api.
        **kwargs: other arguments.
    Returns:
        dict: correction info.
        list(str): top-k recommended code snippets.
    """
    # add nl query to the user content
    w_corrt_messages = copy.deepcopy(messages)
    user_content = w_corrt_messages[-1]["content"]
    extended_user_content = "{}{}{}".format(user_content, query_prefix, nl_query)
    w_corrt_messages[-1]["content"] = extended_user_content

    # get the top-k code snippets
    print("Message for getting code with correction: {}".format(w_corrt_messages))
    recomm_codes = \
        nl2code.get_codes_with_options_single_input(nl_query, query_embd, nl2code_cache_files,
                                                    remove_python_mark,
                                                    order_logprobs, discard_extra_code_snippets, top_k, function_only=function_only,
                                                    rate_limit_per_minute=rate_limit_per_minute,
                                                    messages=w_corrt_messages,
                                                    **kwargs)
    
        
    return correction_info, recomm_codes


def get_codes_with_corrections_codex(
        corrt_ds,
        knn_indices,
        nl_query,
        query_embd,
        embedding_cache_files,
        embd_engine,
        nl2code_cache_files,
        remove_python_mark,
        messages,
        examples_prefix,
        query_prefix,
        simi_query_prefix,
        order_logprobs=False, discard_extra_code_snippets=True,
        top_k=3,
        function_only=False,
        rate_limit_per_minute=20,
        **kwargs):
    """Integrate the generated codes with corrections from the datastore by using Codex.
    The nl query is extended by its knns in the datastore.

    Args:
        corrt_ds (CorrectionDS): correction datastore.
        knn_indices (list): list of indices of nearest neighbors in correction_ds.
        nl_query (str): the nl query.
        query_embd (np.array): the embedding of the nl query.
        nl2code_cache_files (dict): cache files for nl2code.
        remove_python_mark (bool): whether to remove the python mark in the generated code, for gpt-3.5-turbo.
        messages (list(dict)): the conversation, for gpt-3.5-turbo.
        examples_prefix (str): prefix for examples in the extended user content.
        query_prefix (str): prefix for the nl query in the extended user content.
        order_logprobs (bool): whether to order the code snippets by log probability.
        discard_extra_code_snippets (bool): whether to discard extra code snippets.
        top_k (int): number of code snippets to return.
        function_only (bool): whether to only keep function code snippets.
        rate_limit_per_minute (int): rate limit for openai api.
        example_prefix (str): prefix for examples in the extended nl query.
        query_prefix (str): prefix for the extended nl query.

    Returns:
        dict: correction info, None.
        list(str): top-k recommended code snippets.
        list(tree-sitter): top-k tree objects of the top-k recommended code snippet.
        list(dict): top-k of dictionaries of alternative values for each identifier in top-k snippets, keys are node ids.
        list(dict): top-k of dictionaries of token types info for each token in top-k recommended code snippets, keys are node ids.
        dict: dictionary of possible values for each token type in code snippets, keys are token types.
    """
    codex_messages = copy.deepcopy(messages)
    if corrt_ds.correction_ds:
        correction_keys = list(corrt_ds.correction_ds.keys())

        examples = [examples_prefix]
        new_queries = []
        for index in knn_indices:
            simi_query_embd = correction_keys[index]
            correction_info = corrt_ds.get_correction(simi_query_embd)
            simi_nl = "{}{}".format(simi_query_prefix, correction_info["nl"])
            simi_code = correction_info["code"]
            example = "{}\n{}".format(simi_nl, simi_code)
            examples.append(example)

            # to embedding new queries, we eliminate "Function" prefix in the nl query
            new_queries.append("{}\n{}".format(correction_info["nl"], simi_code))

        example_contents = "\n".join(examples)
        new_queries.append(nl_query)
        new_query_text = "\n".join(new_queries)

        new_embd = nl_embedding.get_nl_embedding_multi_caches(new_query_text, embedding_cache_files, embd_engine, rate_limit_per_minute)

        old_user_content = codex_messages[-1]["content"]
        extended_user_content = "{}{}\n{}{}".format(old_user_content, example_contents, query_prefix, nl_query)
        codex_messages[-1]["content"] = extended_user_content
    else:
        # this case is similar to generate codes without corrections
        new_embd = query_embd
        new_query_text = nl_query
        
        old_user_content = codex_messages[-1]["content"]
        extended_user_content = "{}{}{}".format(old_user_content, query_prefix, nl_query)
        codex_messages[-1]["content"] = extended_user_content

    # get the top-k code snippets
    print("Message for getting code with Codex: {}".format(codex_messages))
    recomm_codes = \
        nl2code.get_codes_with_options_single_input(new_query_text, new_embd, nl2code_cache_files,
                                                    remove_python_mark,
                                                    order_logprobs, discard_extra_code_snippets, top_k, function_only=function_only,
                                                    rate_limit_per_minute=rate_limit_per_minute,
                                                    messages=codex_messages,
                                                    **kwargs)

    return None, recomm_codes
            

def generate_code_for_nl_chunk(nl_chunk, chunk_embd, nl2code_cache_files,
                               remove_python_mark,
                               messages,
                               query_prefix,
                               order_logprobs=False, discard_extra_code_snippets=True, top_k=1, function_only=False,
                               rate_limit_per_minute=20,
                               **kwargs):
    """Generate code for a NL chunk using the code generation model.
    The NL chunk is not in the correction datastore.

    Args:
        nl_chunk (str): the NL chunk or a verb-noun phrase.
        chunk_embd (np.ndarray): the embedding of the NL chunk.
        nl2code_cache_files (list(str)): list of cache files.
        remove_python_mark (bool): whether to remove the python mark.
        messages (list(dict)): conversation between user and system, for gpt-3.5-turbo.
        query_prefix (str): prefix for the query.
        order_logprobs (bool): whether to order the code snippets by log probability.
        discard_extra_code_snippets (bool): whether to discard extra code snippets.
        top_k (int): number of code snippets to return.
        function_only (bool): whether to only keep function code snippet.
        rate_limit_per_minute (int): rate limit for the openai api.
        **kwargs: other arguments.

    Returns:
        list(list(str), str): list of (extracted snippet, generated code)
    """
    chunk_messages = copy.deepcopy(messages)
    # add nl chunk to the user content
    user_content = chunk_messages[-1]["content"]
    extended_user_content = "{}{}{}".format(user_content, query_prefix, nl_chunk)
    chunk_messages[-1]["content"] = extended_user_content

    # generate code
    print("Message for getting code for chunk: {}".format(chunk_messages))
    recomm_codes = \
        nl2code.get_codes_with_options_single_input(nl_chunk, chunk_embd, nl2code_cache_files,
                                                    remove_python_mark,
                                                    order_logprobs, discard_extra_code_snippets, top_k, function_only=function_only,
                                                    rate_limit_per_minute=rate_limit_per_minute,
                                                    messages=chunk_messages,
                                                    **kwargs)
    
    recomm_trees = [ast_utils.get_tree_sitter_tree(code) for code in recomm_codes]

    snippets_codes = []
    for i, tree in enumerate(recomm_trees):
        root_node = tree.root_node
        dfs_sequence_ast = ast_utils.get_dfs_sequence(root_node, [])
        sub_nodes = nl_code_mapping.extract_sub_snippets(dfs_sequence_ast)
        function_body = []
        for node_info in sub_nodes:
            node, idx = node_info
            node_text = ast_utils.get_text(node)
            function_body.append(node_text)
        snippets_codes.append((function_body, recomm_codes[i]))

    return snippets_codes


def extract_sub_snippet_from_query_code(nl_chunk, nlp, simi_nl_query, simi_target_code,
                                        nl_threshold=0.55, lemma=True, stop_words=True,
                                        nl_code_threshold=0.5, check_root_verb=False, 
                                        has_string=True, has_comment=True):
    """Extract sub code snippet from a given query and its corresponding target code.
    Note that the query is a KNN of the NL chunk.

    Args:
        nl_chunk (str): the NL chunk or a verb-noun phrase.
        nlp (spacy.lang.en.English): Spacy model
        simi_nl_query (str): similar NL query
        simi_target_code (str): similar target code
        nl_threshold (float, optional): the threshold of similarity. Defaults to 0.7
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.
        nl_code_threshold (float, optional): the threshold of similarity between NL and code. Defaults to 0.5.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        has_string (bool, optional): whether to include string in the explaination. Defaults to True.
        has_comment (bool, optional): whether to include comment in the explaination. Defaults to True.

    Returns:
        list(str, float, str), str: list of (sub code snippet, similarity score with the nl phrase, snippet explaination).
            None if the sub snippet cannot be extracted,
            and the most similar chunk in the similar NL query.
    """
    comparison = queries_comparison.compare_chunk_and_query(nl_chunk, simi_nl_query, nlp, nl_threshold, lemma, stop_words)

    if comparison is None:
        return None, None
    
    _, most_similar_v_n_phrase = comparison
    
    # extract sub snippet from the target code
    query_verb_noun_phrases_to_code = nl_code_mapping.map_nl_ast(simi_nl_query, simi_target_code, nlp, 
                                                                 nl_code_threshold, lemma=lemma, check_root_verb=check_root_verb, 
                                                                 has_string=has_string, has_comment=has_comment, stop_words=stop_words)
    
    if most_similar_v_n_phrase not in query_verb_noun_phrases_to_code:
        return None, None
    
    snippets_info = query_verb_noun_phrases_to_code[most_similar_v_n_phrase]

    return snippets_info, most_similar_v_n_phrase
            

def get_sub_snippets_for_nl_chunk(nl_chunk, nlp, embedding_cache_files,
                                  nl2code_cache_files,
                                  remove_python_mark,
                                  messages,
                                  query_prefix,
                                  embd_engine="text-embedding-ada-002", 
                                  rate_limit_per_minute=20,
                                  order_logprobs=False, discard_extra_code_snippets=True,
                                  top_k=1, function_only=False,
                                  correction_ds_path=None,
                                  filtered_correction_ds=None,
                                  knn=5,
                                  nl_threshold=0.55, lemma=True, stop_words=True,
                                  nl_code_threshold=0.5, check_root_verb=False, 
                                  has_string=True, has_comment=True,
                                  knn_threshold=0.25,
                                  handle_loop=True,
                                  handle_specific_value=True,
                                  **kwargs):
    """Get sub snippets for a NL chunk.
    The codes in correction datastore will be searched first,
    if there is no match, then the code generation model will be used.

    Args:
        nl_chunk (str): the NL chunk or a verb-noun phrase.
        nlp (spacy.lang.en.English): Spacy model
        cache_file_paths (list): list of paths to cache files for embedding.
        embd_engine (str): engine to use for embedding. Defaults to "text-embedding-ada-002".
        rate_limit_per_minute (int): rate limit per minute, e.g. 20, from OpenAI's website.
        order_logprobs (bool): whether to order the code snippets by log probability.
        discard_extra_code_snippets (bool): whether to discard extra code snippets.
        top_k (int): number of code snippets to return.
        function_only (bool): whether to only keep function code snippet.
        correction_ds_path (str): path to the correction datastore.
        filtered_correction_ds (dict): filtered correction datastore.
        knn (int): number of nearest neighbors to return.
        nl_threshold (float, optional): the threshold of similarity. Defaults to 0.55
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.
        nl_code_threshold (float, optional): the threshold of similarity between NL and code. Defaults to 0.5.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        has_string (bool, optional): whether to include string in the explaination. Defaults to True.
        has_comment (bool, optional): whether to include comment in the explaination. Defaults to True.
        knn_threshold (float): threshold of cosine distance to filter out the nearest neighbors. Default is 0.25.
        handle_loop (bool): whether to handle chunks with loop.
        handle_specific_value (bool): whether to handle chunks with specific values.

    Returns:
        list(list(str, float, str), str, str, str): a list of tuples of (sub code snippets, most similar chunk, similar nl query, similar target code).
            + similar nl query is the query that is similar to the nl chunk
            + similar target code is the code corresponding to the similar nl query
            + sub code snippets is a list of tuples of (sub code snippet, simlarity score with the phrase in the simi. query , snippet explaination)
            These information will be used to combine the sub code snippets to build the target code.
    """
    result_sub_snippets = []
    # load the correction datastore
    corrt_ds = CorrectionDS(correction_ds_path, filtered_correction_ds)

    # get the embedding of the nl query
    chunk_embd = nl_embedding.get_nl_embedding_multi_caches(nl_chunk, embedding_cache_files, embd_engine, rate_limit_per_minute)

    if chunk_embd is None:
        raise Exception("Failed to get the embedding for the chunk with the engine: {}".format(embd_engine))
    
    if tuple(chunk_embd) in corrt_ds.correction_ds:
        correction_info = corrt_ds.get_correction(chunk_embd)
        similar_nl_query = correction_info["nl"]
        similar_target_code = correction_info["code"]

        # separate similar target code to sub snippets
        # get AST sub snippets
        tree = ast_utils.get_tree_sitter_tree(similar_target_code)
        root_ast = tree.root_node
        dfs_sequence_ast = ast_utils.get_dfs_sequence(root_ast, [])
        sub_snippets = nl_code_mapping.extract_sub_snippets(dfs_sequence_ast)

        # list of sub snippets in target code
        function_body = []
        for node_info in sub_snippets:
            node, idx = node_info
            node_text = ast_utils.get_text(node)
            function_body.append(node_text)
        
        result_sub_snippets.append(([(fun, 1.0, "") for fun in function_body], similar_nl_query, similar_nl_query, similar_target_code))
    else:
        # get knns of the nl query from the correction datastore
        knn_indices, distances = corrt_ds.get_knn_indices(chunk_embd, knn, knn_threshold)
        
        counter = 0
        correction_keys = list(corrt_ds.correction_ds.keys())
        for index in knn_indices:
            query_embd = correction_keys[index]
            correction_info = corrt_ds.get_correction(query_embd)
            similar_nl_query = correction_info["nl"]
            similar_target_code = correction_info["code"]
            extracted_sub_snippets, most_similar_v_n_phrase = extract_sub_snippet_from_query_code(nl_chunk, nlp, similar_nl_query, similar_target_code,
                                                                nl_threshold=nl_threshold, lemma=lemma, stop_words=stop_words,
                                                                nl_code_threshold=nl_code_threshold, check_root_verb=check_root_verb, 
                                                                has_string=has_string, has_comment=has_comment)
            if extracted_sub_snippets: # extracted sub snippets can be empty or None too
                result_sub_snippets.append((extracted_sub_snippets, most_similar_v_n_phrase, similar_nl_query, similar_target_code))
                counter += 1
                if counter >= top_k:
                    break

        if len(knn_indices) == 0 or not result_sub_snippets: # no KNNs for the chunk or can't extract sub snippets from the KNNs
            snippets_and_codes = generate_code_for_nl_chunk(nl_chunk, chunk_embd, nl2code_cache_files,
                                                           remove_python_mark,
                                                           messages,
                                                           query_prefix,
                                                           order_logprobs, discard_extra_code_snippets, top_k, function_only=function_only,
                                                           rate_limit_per_minute=rate_limit_per_minute,
                                                           **kwargs)
            for snippets_code in snippets_and_codes:
                generated_snippets, generated_code = snippets_code
                result_sub_snippets.append(([(generated_snippet, 1.0, "") for generated_snippet in generated_snippets], nl_chunk, nl_chunk, generated_code))
                
    return result_sub_snippets


def create_correction_info_from_built_code(built_code, nl_query):
    """Create correction info from the built code and the nl query.
    The built code is the code built by the chunking method.
    The nl query is the nl query corresponding to the built code.

    Args:
        built_code (str): the built code.
        nl_query (str): the nl query.

    Returns:
        dict: a dictionary of correction info.
        {
            "nl": "the nl query",
            "code": "the corrected code in string"
        }
    """

    correction_info = {
        "nl": nl_query,
        "code": built_code,
        # additional fields will be added later
    }

    return correction_info


def refine_chunk(nl_chunk):
    """Refine the nl chunk to convert it to a "normal" nl query.
    """
    chunk_tokens = nl_chunk.split(" ")
    if nl_chunk.startswith("which ") and len(chunk_tokens) > 2: # which is/are...
        modified_tokens = ["get"] + chunk_tokens[2:]
    elif nl_chunk.startswith("then ") and len(chunk_tokens) > 1: # then print...
        modified_tokens = chunk_tokens[1:]
    else:
        modified_tokens = chunk_tokens

    modified_chunk = " ".join(modified_tokens)

    return modified_chunk
            

def build_code_by_chunking(
        nl_query,
        embedding_cache_files,
        nl2code_cache_files,
        messages,
        query_prefix,
        embd_engine="text-embedding-ada-002",
        rate_limit_per_minute=20,
        remove_python_mark=True,
        order_logprobs=False, discard_extra_code_snippets=True,
        top_k=1,
        function_only=False,
        correction_ds_path=None,
        filtered_correction_ds=None,
        knn=5,
        combine_values=True,
        nl_threshold=0.55, lemma=True, stop_words=True,
        nl_code_threshold=0.5, check_root_verb=False, 
        has_string=False, has_comment=False,
        knn_threshold=0.15,
        language_model= "en_core_web_md",
        reduce_simi_snippets_threshold=0.85,
        reduce_strings=True,
        alternative_string="<update string>",
        string_len_threshold=15,
        handle_loop=True,
        handle_specific_value=True,
        refine_no_start_return=True,
        refine_reduce_plural=True,
        refine_between_return=True,
        **kwargs
        ):
    """Get the top-k corrected code snippets for a given nl query.
    Integrate the generated codes with corrections from the datastore.
    First, decompose the nl query into chunks and get code snippets for each chunk.
    Then combine the obtained code snippets to build the target code.

    Args:
        

    Returns:
        dict: correction info. None if no correction is found.
        list(str): top-k recommended code snippets.
        list(tree-sitter): top-k tree objects of the top-k recommended code snippet.
        list(dict): top-k of dictionaries of alternative values for each identifier in top-k snippets, keys are node ids.
        list(dict): top-k of dictionaries of token types info for each token in top-k recommended code snippets, keys are node ids.
        dict: dictionary of possible values for each token type in code snippets, keys are token types.
    """
    # load the correction datastore
    corrt_ds = CorrectionDS(correction_ds_path, filtered_correction_ds)

    # get the embedding of the nl query
    query_embd = nl_embedding.get_nl_embedding_multi_caches(nl_query, embedding_cache_files, embd_engine, rate_limit_per_minute)

    if query_embd is None:
        raise Exception("Failed to get the embedding for the query with the engine: {}".format(embd_engine))
    
    if tuple(query_embd) in corrt_ds.correction_ds:
        correction_info = corrt_ds.get_correction(query_embd)
        return get_codes_with_correction(correction_info, nl_query, query_embd, nl2code_cache_files,
                                         remove_python_mark,
                                         messages,
                                         query_prefix,
                                         order_logprobs, discard_extra_code_snippets, top_k, combine=combine_values, 
                                         function_only=function_only,
                                         rate_limit_per_minute=rate_limit_per_minute,
                                         **kwargs)
    else:
        # get knns of the nl query from the correction datastore
        knn_indices, distances = corrt_ds.get_knn_indices(query_embd, knn, knn_threshold)
        
        # get the nlp
        nlp = nlp_setting.create_nlp(language_model=language_model)

        # decompose the nl query into chunks
        root_verb, verb_noun_phrases, sequence_graph = queries_comparison.extract_root_and_verb_noun_phrases(nl_query, nlp)

        if root_verb is None:
            # no verb in the query, generate code using code generation model
            correction_info = None
            return get_codes_with_correction(correction_info, nl_query, query_embd, nl2code_cache_files,
                                            remove_python_mark,
                                            messages,
                                            query_prefix,
                                            order_logprobs, discard_extra_code_snippets, top_k, combine=combine_values, 
                                            function_only=function_only,
                                            rate_limit_per_minute=rate_limit_per_minute,
                                            **kwargs)
        else:

            # get the code snippets for each chunk
            sub_snippets = {}
            for chunk in verb_noun_phrases:
                # refine the chunk to make it a "normal" nl query
                refined_chunk = refine_chunk(chunk)
                sub_snippets[chunk] = get_sub_snippets_for_nl_chunk(refined_chunk, nlp,
                                                    embedding_cache_files,
                                                    nl2code_cache_files,
                                                    remove_python_mark,
                                                    messages,
                                                    query_prefix,
                                                    embd_engine, 
                                                    rate_limit_per_minute,
                                                    order_logprobs, discard_extra_code_snippets, 
                                                    top_k=top_k, function_only=function_only,
                                                    correction_ds_path=correction_ds_path, 
                                                    filtered_correction_ds=filtered_correction_ds,
                                                    knn=knn,
                                                    nl_threshold=nl_threshold, lemma=lemma, stop_words=stop_words,
                                                    nl_code_threshold=nl_code_threshold, check_root_verb=check_root_verb, 
                                                    has_string=has_string, has_comment=has_comment, knn_threshold=knn_threshold,
                                                    handle_loop=handle_loop, handle_specific_value=handle_specific_value,
                                                    **kwargs)
                
            # combine the obtained code snippets to build the target code
            built_code, mapping = code_building.build_code_from_sub_snippets(nl_query, sub_snippets, nlp, knn_indices, distances, corrt_ds, sequence_graph, 
                                                                             reduce_threshold=reduce_simi_snippets_threshold,
                                                                             lemma=lemma, check_root_verb=check_root_verb, stop_words=stop_words,
                                                                             reduce_strings=reduce_strings, default_string=alternative_string,
                                                                             string_len_threshold=string_len_threshold,
                                                                             refine_no_start_return=refine_no_start_return,
                                                                             refine_reduce_plural=refine_reduce_plural,
                                                                             refine_between_return=refine_between_return)

            # create correction info
            correction_info = create_correction_info_from_built_code(built_code, nl_query)

            return get_codes_with_correction(correction_info, nl_query, query_embd, nl2code_cache_files,
                                            remove_python_mark,
                                            messages,
                                            query_prefix,
                                            order_logprobs, discard_extra_code_snippets, top_k, combine=combine_values,
                                            function_only=function_only,
                                            rate_limit_per_minute=rate_limit_per_minute,
                                            **kwargs)