# Refer to https://github.com/openai/openai-cookbook/blob/main/examples/Recommendation_using_embeddings.ipynb


from scripts.one_shot_correction import nl_embedding
from openai.embeddings_utils import (
    distances_from_embeddings,
    indices_of_nearest_neighbors_from_distances
)


def filter_cache_by_engine(cache, engine):
    """Filter a cache of embeddings as a dictionary by engine

    Args:
        cache (dict): a dictionary of cache of embeddings, keys are tuples of (nl_quert, engine)
        engine (str): name of the engine used, e.g. "text-embedding-ada-002"

    Returns:
        (dict) The filtered cache with only items embedded by engine.
    """
    if not engine:
        return cache
    
    return {k: v for k, v in cache.items() if k[1] == engine}


def get_avail_embeddings(cache_files, engine=""):
    """Get all available embeddings from the available caches.

    Args:
        cache_files (list): list of cache file paths. Cache file names must be unique.
        engine (str): name of the engine used, default value is "" (i.e. gets all values from the cache).

    Returns:
        (dict) Dictionary of lists of embeddings, keys are cache file names.
    """
    embeddings = {}
    for cache_file in cache_files:
        cache_file_name = str(cache_file).split("/")[-1]
        cache = nl_embedding.get_embeddings_cache(cache_file)
        if engine:
            cache = filter_cache_by_engine(cache, engine)
        embeddings[cache_file_name] = list(cache.values())

    return embeddings


def get_indices_nearest_neighbors(query_emb, queries_embs, knn=5, knn_threshold=0.25):
    """Get the indices of KNNs of a given query embedding.
    The indices are based on the indices of queries_embs.

    Args:
        query_emb (list): embedding of the query.
        queries_embs (list): list of embeddings of other queries to compare with.
        knn (int): number of nearest neighbors to return.
        knn_threshold (float): threshold of cosine distance to filter out the nearest neighbors. Default is 0.25.

    Returns:
        (list, list) A tuple of list of indices of KNNs, and list of distances.
    """
    # get distances between the source embedding and other embeddings (function from embeddings_utils.py)
    distances = distances_from_embeddings(query_emb, queries_embs, distance_metric="cosine")
    # get indices of nearest neighbors (function from embeddings_utils.py)
    indices_of_nearest_neighbors = indices_of_nearest_neighbors_from_distances(distances)
    print("Originally, there are {} nearest neighbors.".format(len(indices_of_nearest_neighbors)))

    if knn > len(indices_of_nearest_neighbors):
        knn = len(indices_of_nearest_neighbors)

    # filter out the nearest neighbors with distance larger than knn_threshold
    nearest_indices = []
    for idx in indices_of_nearest_neighbors[:knn]:
        if distances[idx] <= knn_threshold:
            nearest_indices.append(idx)

    return nearest_indices, [distances[i] for i in nearest_indices]


def get_indices_nearest_neighbors_nl(nl_query, other_queries, cache_files, knn=5, engine="text-embedding-ada-002", rate_limit_per_minute=20, knn_threshold=0.25):
    """Get the indices of KNNs of a given NL query.
    The indices are based on the indices of other_queries.

    Args:
        nl_query (str): NL query.
        other_queries (list): list of other queries to compare with.
        cache_files (list): list of cache file paths.
        knn (int): number of nearest neighbors to return.
        engine (str): name of the engine used, default is "text-embedding-ada-002"
        rate_limit_per_minute (int): rate limit per minute, e.g. 20, from OpenAI's website.
        knn_threshold (float): threshold of cosine distance to filter out the nearest neighbors. Default is 0.25.

    Returns:
        (list, list) A tuple of list of indices of KNNs, and list of distances.
    """
    embeddings = [
        nl_embedding.get_nl_embedding_multi_caches(
        query, cache_files, engine, rate_limit_per_minute) for query in other_queries
    ]
    query_embedding = nl_embedding.get_nl_embedding_multi_caches(
        nl_query, cache_files, engine, rate_limit_per_minute)

    indices, distances = get_indices_nearest_neighbors(query_embedding, embeddings, knn, knn_threshold)
    return indices, distances