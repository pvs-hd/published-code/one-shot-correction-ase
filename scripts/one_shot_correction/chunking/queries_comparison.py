# Comparing two similar NL queries to find the common and uncommon parts
# All the NL queries have to follow the same structure
# Query: Function <verb> ... [, and then <verb> ...]
# E.g. Function adds two numbers, and then print the result
# Identifiers with multiple tokens should use underscore as the separator

import textacy
from ordered_set import OrderedSet


def get_string_quotes_indices(nlp_doc):
    """Get the indices of the string quotes in a NL query

    Args:
        nlp_doc (spacy.tokens.doc.Doc): the NL query

    Returns:
        list(int, int): the indices of the open and end string quotes
    """
    string_quotes_indices = []

    is_opening = False
    open_quote_idx = None
    for token in nlp_doc:
        # only " is allowed as the string quote
        open_quote = (is_opening == False) and \
            (token.text == "\"")
        close_quote = (is_opening == True) and \
            (token.text == "\"") and \
            (open_quote_idx is not None)
        
        if open_quote:
            is_opening = True
            open_quote_idx = token.i
        elif close_quote:
            string_quotes_indices.append((open_quote_idx, token.i))

            # reset
            is_opening = False
            open_quote_idx = None

    return string_quotes_indices


def extract_nouns(nlp_doc):
    """Extract nouns from a NL query

    Args:
        nlp_doc (spacy.tokens.doc.Doc): the NL query

    Returns:
        list: the nouns
    """
    noun_patterns = [[{"POS": "NOUN"}]]
    nouns = textacy.extract.token_matches(nlp_doc, patterns=noun_patterns)

    return nouns


def extract_verb_phrases(nlp_doc):
    """Extract the verb phrases from a NL query

    Args:
        nlp_doc (spacy.tokens.doc.Doc): the NL query

    Returns:
        list: the verb phrases
    """
    verb_patterns = [[{"POS": "AUX", "OP": "!"}, {"POS": "VERB"}], 
                     [{"POS": "AUX"}, {"POS": "VERB", "OP": "!"}], 
                     [{"POS": "AUX"}, {"POS": "VERB"}], 
                     [{"POS": "VERB", "IS_SENT_START": True}]] # verb at the beginning of the query
    verb_phrases = textacy.extract.token_matches(nlp_doc, patterns=verb_patterns)

    return verb_phrases


def check_token_between_quotes(verb, string_quotes_indices):
    """Check if a token is between string quotes

    Args:
        verb (spacy.tokens.token.Token): the token
        string_quotes_indices (list(int, int)): the indices of the open and end string quotes

    Returns:
        bool: True if the token is between string quotes, False otherwise
    """
    is_between_quotes = False
    for quote_range in string_quotes_indices:
        open_quote_idx, close_quote_idx = quote_range
        if (verb.i > open_quote_idx) and (verb.i < close_quote_idx):
            is_between_quotes = True
            break

    return is_between_quotes


def get_root_non_root_verbs(verb_phrases, string_quotes_indices):
    """Get the root verb and non-root verbs

    Args:
        verb_phrases (list): the verb phrases
        string_quotes_indices (list(int, int)): the indices of the open and end string quotes

    Returns:
        (spacy.tokens.token.Token, OrderedSet, OrderedSet): the root verb and non-root verbs, 
        and ignored support verbs (v-ing, v-ed) that don't describe the action
    """
    root_verb = None
    non_root_verbs = OrderedSet()
    ignored_verbs = OrderedSet()
    for verb_chunk in verb_phrases:
        # sometimes "then" is recognized as a root of "then <verb>"
        supp_verbs = ((verb_chunk.root.pos_ not in ["VERB", "AUX"]) and \
                (verb_chunk.root.text not in ["then", "and"])) or \
                (check_token_between_quotes(verb_chunk.root, string_quotes_indices)) # v-ing, v-ed and verb in between string quotes
        if not supp_verbs: # ignore the support verbs
            if verb_chunk.root.dep_ != "ROOT":
                if verb_chunk.root.pos_ in ["VERB", "AUX"]:
                    non_root_verbs.add(verb_chunk.root)
                else:
                    for token in verb_chunk:
                        if token.pos_ in ["VERB", "AUX"]:
                            non_root_verbs.add(token)
            else:
                root_verb = verb_chunk.root # there is only one root verb in a NL query
        else:
            for token in verb_chunk:
                if token.pos_ in ["VERB", "AUX"]:
                    ignored_verbs.add(token.i)
    return root_verb, non_root_verbs, ignored_verbs


def get_bwd_nearest_mainverb_in_ancerstors(token, ignored_verbs):
    """Get the nearest (backward) main verb of a token by tracing ancestors of the token until the first verb is reached.

    Args:
        token (spacy.tokens.token.Token): the token
        ignored_verbs (OrderedSet(int)): the ignored support verbs

    Returns:
        spacy.tokens.token.Token: the nearest verb in ancestors.
    """
    nearest_mainverb = None
    for anc in token.ancestors:
        if (anc.pos_ == "VERB") and (anc.head.pos_ == "VERB") and (anc.i not in ignored_verbs):
            nearest_mainverb = anc
            break

    return nearest_mainverb


def get_fwd_nearest_subverb_in_subtree(token, ignored_verbs):
    """Get the nearest (forward) sub verb of a token by tracing the subtree of the token until the first verb is reached.

    Args:
        token (spacy.tokens.token.Token): the token
        ignored_verbs (OrderedSet(int)): the ignored support verbs

    Returns:
        spacy.tokens.token.Token: the nearest verb in subtree.
    """
    nearest_subverb = None
    for child in token.subtree:
        is_aux_or_verb = child.pos_ in ["AUX", "VERB"]
        is_not_mainverb = child.head.pos_ != "VERB"
        belong_current_mainverb_scope = get_bwd_nearest_mainverb_in_ancerstors(child, ignored_verbs) == token
        not_support_verb = child.i not in ignored_verbs
        if is_aux_or_verb and is_not_mainverb and belong_current_mainverb_scope and not_support_verb:
            nearest_subverb = child
            break

    return nearest_subverb


def get_immediate_after_verb(verb, ignored_verbs):
    """Get the immediate after verb of a verb.

    Args:
        verb (spacy.tokens.token.Token): the verb
        ignored_verbs (OrderedSet(int)): the ignored support verbs

    Returns:
        spacy.tokens.token.Token: the immediate after verb.
    """
    imme_after_verb = None

    if verb is  None or verb.pos_ not in ["AUX", "VERB"]:
        return imme_after_verb
    
    fwd_nearest_subverb = get_fwd_nearest_subverb_in_subtree(verb, ignored_verbs)
    if fwd_nearest_subverb is not None:
        imme_after_verb = fwd_nearest_subverb
    else:
        bwd_nearest_mainverb = get_bwd_nearest_mainverb_in_ancerstors(verb, ignored_verbs)
        if bwd_nearest_mainverb is not None:
            is_not_mainverb = verb.head.pos_ != "VERB"
            if is_not_mainverb:
                # previous main verb of the current main verb
                imme_after_verb = get_bwd_nearest_mainverb_in_ancerstors(bwd_nearest_mainverb, ignored_verbs)
            else:
                imme_after_verb = bwd_nearest_mainverb

    return imme_after_verb


def find_refined_imme_after_verb(imme_after_sequence, idx):
    """Find the suitable immediate after verb of a verb based on available immediate after verbs.
    If the idx is not in the imme_after_sequence, return the idx itself.
    If not, it means there is a verb after idx already,
    so the refined idx will be the "first empty" slot in the imme_after_sequence.
    """
    if idx not in imme_after_sequence:
        return idx
    else:
        return find_refined_imme_after_verb(imme_after_sequence, imme_after_sequence[idx][0])


def build_sequence_graph(root_verb, non_root_verbs, ignored_verbs):
    """Build the sequence graph of all verbs in a NL query.
    The verbs in non_root_verbs are already in the order of their appearance in the NL query.

    Args:
        root_verb (spacy.tokens.token.Token): the root verb
        non_root_verbs (OrderedSet): the non-root verbs
        ignored_verbs (OrderedSet(int)): the ignored support verbs (v-ing, v-ed) that don't describe the action

    Returns:
        dict: the sequence graph sorted by the order of actions in the NL query
        {
            verb_idx: {
                "verb": spacy.tokens.token.Token,
                "imme_after": verb idx
            }
        }
    """
    sequence_graph = {}

    if root_verb is None: # no verb in the NL query
        return sequence_graph

    verbs = [root_verb] + list(non_root_verbs)

    imme_after_sequence = {}
    start_verb_i = -100
    start_verb = None
    check_none = 0
    for verb in verbs:
        imme_after_verb = get_immediate_after_verb(verb, ignored_verbs)
        if imme_after_verb is not None:
            # make sure that one verb is after only one verb and has at most one following verb
            refined_imme_verb = find_refined_imme_after_verb(imme_after_sequence, imme_after_verb.i)
            imme_after_sequence[refined_imme_verb] = (verb.i, verb)
        else:
            start_verb_i = verb.i # there is only 1 start verb in a query
            start_verb = verb
            check_none += 1

    assert check_none == 1 # There should be only one verb that has no immediate after verb
    assert len(verbs) == len(imme_after_sequence) + 1

    if start_verb_i != -100 and start_verb is not None:
        sequence_graph[start_verb_i] = {"verb": start_verb, "imme_after": None}

        current_i = start_verb_i
        while current_i in imme_after_sequence:
            next_i = imme_after_sequence[current_i][0]
            next_verb = imme_after_sequence[current_i][1]
            sequence_graph[next_i] = {"verb": next_verb, "imme_after": current_i}
            current_i = list(sequence_graph.keys())[-1]

    return sequence_graph


def delete_verb_in_sequence_graph(sequence_graph, tgt_verb_idx):
    """Delete a verb in the sequence graph.
    Update other verbs that have "imme_after" pointing to the deleted verb.

    Args:
        sequence_graph (dict): the sequence graph
        verb_idx (int): the index of the verb to be deleted
    
    Returns:
        dict: the updated sequence graph
    """
    if tgt_verb_idx not in sequence_graph:
        return sequence_graph
    
    # update the sequence graph before deleting the verb from the sequence graph
    before_verb = sequence_graph[tgt_verb_idx]["imme_after"]
    for v_idx in sequence_graph:
        if sequence_graph[v_idx]["imme_after"] == tgt_verb_idx:
            sequence_graph[v_idx]["imme_after"] = before_verb

    del sequence_graph[tgt_verb_idx]

    return sequence_graph


def get_subtree_of_verb(verb, ignored_verbs):
    """Get the subtree of a verb, including the verb itself,
    excluding subtrees of other main verbs in the same clause.

    Args:
        verb (spacy.tokens.token.Token): the verb
        ignored_verbs (OrderedSet(int)): the ignored support verbs (v-ing, v-ed) that don't describe the action

    Returns:
        list(spacy.tokens.token.Token): the subtree of the verb
    """    
    subtree = []

    if list(verb.children):
        left_childs = []
        for child in verb.lefts:
            next_verb_rel = ((verb.pos_ in ["AUX", "VERB"] and child.pos_ in ["VERB"]) or \
                (verb.pos_ not in ["AUX", "VERB"] and child.pos_ in ["AUX", "VERB"])) and \
                (child.i not in ignored_verbs) # no AUX here for is/are + v-ed cases
            
            if not next_verb_rel:
                child_subtree = get_subtree_of_verb(child, ignored_verbs)
                left_childs += child_subtree

        subtree = left_childs + subtree
        subtree.append(verb)

        for child in verb.rights:
            next_verb_rel = ((verb.pos_ in ["AUX", "VERB"] and child.pos_ in ["VERB", "AUX"]) or \
                (verb.pos_ not in ["AUX", "VERB"] and child.pos_ in ["AUX", "VERB"])) and \
                (child.i not in ignored_verbs)
            
            if not next_verb_rel:
                child_subtree = get_subtree_of_verb(child, ignored_verbs)
                subtree = subtree + child_subtree
    else:
        punct_cconj_verb = (verb.head.pos_ in ["VERB", "AUX"]) and \
            ((verb.pos_ in ["PUNCT"] and verb.text not in ["\"", "'"]) or \
             (verb.pos_ in ["CCONJ"] and verb.nbor().text == "then")) # include " or ' in the subtree of the verb
        if not punct_cconj_verb:
            subtree.append(verb)
    
    return subtree


def get_non_root_verb_range(non_root_verb, ignored_verbs):
    """Get the range of a non-root verb, exculding ranges from other verbs.

    Args:
        non_root_verb (spacy.tokens.token.Token): the non-root verb
        ignored_verbs (OrderedSet(int)): the ignored support verbs (v-ing, v-ed) that don't describe the action

    Returns:
        list(int, int), list(int): list of the start and end indices of the tokens in the document,
        and list of indices of the tokens in the subtree of the verb, including the verb.
    """
    tokens_range = []

    verb_subtree = get_subtree_of_verb(non_root_verb, ignored_verbs)
    
    if not verb_subtree:
        return []
    
    start_i = verb_subtree[0].i
    end_i = start_i
    counter = start_i
    for token in verb_subtree[1:]:
        if token.i == counter + 1: # imidiate after
            counter = token.i
            end_i = token.i
        else:
            counter = token.i
            tokens_range.append((start_i, end_i + 1))
            start_i = token.i
            end_i = start_i
    
    tokens_range.append((start_i, end_i + 1)) # add the last range

    token_idxs = [token.i for token in verb_subtree]

    return tokens_range, token_idxs


def extract_non_root_verb_noun_phrases(nlp_doc, non_root_verbs, sequence_graph, ignored_verbs):
    """Extract the non-root verb-noun phrases from a NL query

    Args:
        nlp_doc (spacy.tokens.doc.Doc): the NL query
        non_root_verbs (OrderedSet): the non-root verbs
        sequence_graph (dict): the sequence graph of actions of the NL query
        ignored_verbs (OrderedSet(int)): the supporting verbs (e.g. v-ing, v-ed)

    Returns:
        (list, list, dict): the non-root verb-noun phrases and list of their ranges in token idx,
        and updated sequence graph with verb-noun phrases of non-root verbs
        {
            verb_idx: {
                "verb": str,
                "imme_after": verb idx,
                "phrase": str
            }
        }
    """
    non_root_verb_noun_phrases = []
    non_root_verb_token_idxs = []
    considered_token_idxs = set()
    marked_no_phrase_verb_ranges = []
    marked_no_phrase_verb_idxs = []

    for non_root_verb in non_root_verbs:
        non_root_range, token_idxs = get_non_root_verb_range(non_root_verb, ignored_verbs)

        no_phrase = len(token_idxs) ==0  or len(token_idxs) <=3 and \
            ((len(token_idxs) == 2 and nlp_doc[token_idxs[0]].text == "then" and nlp_doc[token_idxs[1]].text == non_root_verb.text) or \
            (len(token_idxs) == 1 and nlp_doc[token_idxs[0]].text == non_root_verb.text) or \
            (len(token_idxs) == 3 and nlp_doc[token_idxs[0]].text == "then" and nlp_doc[token_idxs[1]].text == non_root_verb.text and nlp_doc[token_idxs[2]].text == "and"))
        
        if not no_phrase:
            if not set(token_idxs).issubset(considered_token_idxs):
                # aggregate the range and idxs of the previous no-phrase verbs, if any
                if marked_no_phrase_verb_ranges and marked_no_phrase_verb_idxs:
                    non_root_range = marked_no_phrase_verb_ranges + non_root_range
                    token_idxs = marked_no_phrase_verb_idxs + token_idxs

                    # reset the marked no-phrase verbs
                    marked_no_phrase_verb_ranges = []
                    marked_no_phrase_verb_idxs = []

                # if there are two non-root verbs that have the same phrase,
                # only consider the phrase once
                considered_token_idxs.update(token_idxs)

                non_root_verb_token_idxs.append(token_idxs)

                non_root_text = ""
                for token_range in non_root_range:
                    non_root_text += nlp_doc[token_range[0]:token_range[1]].text.strip() + " "

                non_root_text = non_root_text.strip()
                non_root_verb_noun_phrases.append(non_root_text)

                # update the sequence graph
                sequence_graph[non_root_verb.i]["phrase"] = non_root_text


            else:
                # ignore the verb that has the same phrase with the previous verb
                sequence_graph = delete_verb_in_sequence_graph(sequence_graph, non_root_verb.i)

        else:
            # keep track of the verbs that have no phrase
            # add these verbs to the directly next verb that has valid phrase
            marked_no_phrase_verb_ranges.extend(non_root_range)
            marked_no_phrase_verb_idxs.extend(token_idxs)

            # update the sequence graph
            sequence_graph = delete_verb_in_sequence_graph(sequence_graph, non_root_verb.i)

    return non_root_verb_noun_phrases, non_root_verb_token_idxs, sequence_graph


def extract_root_verb_noun_phrase(nlp_doc, non_root_verb_token_idxs):
    """Extract the root verb-noun phrase from a NL query

    Args:
        nlp_doc (spacy.tokens.doc.Doc): the NL query
        non_root_verb_token_idxs (list(list(int))): the indices of tokens in the subtree of non-root verbs

    Returns:
        str: the root verb-noun phrase
    """
    # the verb-noun phrase of the root verb will contain the remaining tokens
    # except the following direct children:
    #   - punctuation, e.g. comma
    #   - conjunction, e.g. "and"/"or"

    non_consider_tokens = []

    if non_root_verb_token_idxs: # there are non-root verbs
        # the indices of tokens in subtree of non-root verbs
        for token_idxs in non_root_verb_token_idxs:
            non_consider_tokens.extend(token_idxs)

        # punctuation and conjunction
        for i in range(len(nlp_doc)):
            if i not in non_consider_tokens:
                punct_cconj = (nlp_doc[i].pos_ in ["PUNCT"] and nlp_doc[i].text not in ["\"", "'"]) or \
                    (nlp_doc[i].pos_ in ["CCONJ"] and nlp_doc[i].nbor().text == "then")
                    
                if punct_cconj:
                    non_consider_tokens.append(i)

    non_consider_tokens = sorted(non_consider_tokens)

    # the remaining tokens are the root verb-noun phrase
    # we now can get all the text in the root range and seperate them by space
    # however, there will be some cases that the tokens might not be splitted by space
    # therefore, we will use the text idx to get the text
    non_consider_idx_ranges = []
    for i in non_consider_tokens:
        token = nlp_doc[i]
        non_consider_idx_ranges.append((token.idx, token.idx + len(token))) # end idx is exclusive

    root_verb_noun_phrase = ""
    start_idx = 0
    for idxs_range in non_consider_idx_ranges:
        root_verb_noun_phrase += nlp_doc.text[start_idx:idxs_range[0]]
        start_idx = idxs_range[1]
    root_verb_noun_phrase += nlp_doc.text[start_idx:] # the remaining text
    root_verb_noun_phrase = root_verb_noun_phrase.strip() # remove the leading and trailing spaces if any

    return root_verb_noun_phrase


def extract_verb_noun_phrases(nlp_doc, root_verb, non_root_verbs, sequence_graph, ignored_verbs):
    """Extract the verb-noun phrases from a NL query.
    A verb-noun phrases covers the verb and its related tokens (e.g. nouns, adjective, etc.)
    The verb-noun phrase of the root verb will not contain other phrases of non-root verbs

    Args:
        nlp_doc (spacy.tokens.doc.Doc): the NL query
        root_verb (spacy.tokens.token.Token): the root verb
        non_root_verbs (OrderedSet): the non-root verbs
        sequence_graph (dict): the sequence graph of actions of the NL query
        ignored_verbs (OrderedSet(int)): the list of supporting verbs (e.g. v-ing, v-ed)

    Returns:
        list(str), dict: verb-noun phrases,
        and updated sequence graph with verb-noun phrases
        {
            verb_idx: {
                "verb": str,
                "imme_after": verb idx,
                "phrase": str
            }
        }
    """
    verb_noun_phrases = []

    # get verb-noun phrases of non-root verbs first
    non_root_verb_noun_phrases, non_root_verb_token_idxs, sequence_graph = extract_non_root_verb_noun_phrases(nlp_doc, non_root_verbs, sequence_graph, ignored_verbs)
    verb_noun_phrases.extend(non_root_verb_noun_phrases)

    # if there is no root verb, it means no verb in the NL query
    # the phrase will be the NL query except the first token (i.e. "Function")
    root_verb_noun_phrase = extract_root_verb_noun_phrase(nlp_doc, non_root_verb_token_idxs)

    no_phrase_root_verb = root_verb is not None and root_verb_noun_phrase.strip() == root_verb.text

    if no_phrase_root_verb:
        # root verb doesn't have noun phrase
        # e.g. check if the function is valid
        first_non_root_v_n_phrase = verb_noun_phrases[0]
        first_non_root_v_n_phrase = "{} {}".format(root_verb.text, first_non_root_v_n_phrase)
        verb_noun_phrases[0] = first_non_root_v_n_phrase

        first_non_root_verb = non_root_verbs[0]
        sequence_graph[first_non_root_verb.i]["phrase"] = first_non_root_v_n_phrase

        # update the sequence graph
        sequence_graph = delete_verb_in_sequence_graph(sequence_graph, root_verb.i)
    else:
        verb_noun_phrases.insert(0, root_verb_noun_phrase) # insert the root verb-noun phrase to the first

        # update the sequence graph
        if root_verb is not None:
            sequence_graph[root_verb.i]["phrase"] = root_verb_noun_phrase

    return verb_noun_phrases, sequence_graph


def extract_root_and_verb_noun_phrases(nl_query, nlp):
    """Extract the root verb and verb-noun phrases from a NL query

    Args:
        nl_query (str): the NL query
        nlp (spacy.lang.en.English): the language model used

    Returns:
        (spacy.tokens.token.Token, list(str), dict): the root verb and verb-noun phrases,
        and the sequence graph of actions of the NL query
        {
            verb_idx: {
                "verb": str,
                "imme_after": verb idx,
                "phrase": str
            }
        }
    """
    nlp_doc = nlp(nl_query)
    root_verb = None
    verb_noun_phrases = []

    # don't consider verb between string quotes (i.e. only double quote)
    string_quotes_indices = get_string_quotes_indices(nlp_doc)

    verb_phrases = extract_verb_phrases(nlp_doc)
    root_verb, non_root_verbs, ignored_verbs = get_root_non_root_verbs(verb_phrases, string_quotes_indices)

    sequence_graph = build_sequence_graph(root_verb, non_root_verbs, ignored_verbs)

    verb_noun_phrases, sequence_graph = extract_verb_noun_phrases(nlp_doc, root_verb, non_root_verbs, sequence_graph, ignored_verbs)

    return root_verb, verb_noun_phrases, sequence_graph


def is_similar(first_phrase, second_phrase, nlp, threshold, lemma=True, stop_words=True):
    """Check if two phrases are similar

    Args:
        first_phrase (str): the first phrase
        second_phrase (str): the second phrase
        nlp (spacy.lang.en.English): the language model used
        threshold (float, optional): the threshold of similarity. Defaults to 0.7
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        bool: True if two phrases are similar, False otherwise
    """
    first_phrase_doc = nlp(first_phrase)
    second_phrase_doc = nlp(second_phrase)

    if stop_words:
        # remove stop words before comparing
        first_phrase_tokens = [token.text for token in first_phrase_doc if not token.is_stop]
        second_phrase_tokens = [token.text for token in second_phrase_doc if not token.is_stop]

        first_phrase_doc = nlp(" ".join(first_phrase_tokens))
        second_phrase_doc = nlp(" ".join(second_phrase_tokens))

    if lemma:
        # convert word to lemma before comparing
        first_lemma = []
        second_lemma = []

        for f_token in first_phrase_doc:
            first_lemma.append(f_token.lemma_)

        for s_token in second_phrase_doc:
            second_lemma.append(s_token.lemma_)

        similarity = nlp(" ".join(first_lemma)).similarity(nlp(" ".join(second_lemma)))
    else:
        similarity = first_phrase_doc.similarity(second_phrase_doc)

    return similarity >= threshold, similarity


def find_comm_uncomm_parts(first_verb_noun_phrases, second_verb_noun_phrases, nlp, threshold, lemma=True, stop_words=True):
    """Find the common and uncommon parts of two NL queries

    Args:
        first_verb_noun_phrases (list): the verb-noun phrases of the first query
        second_verb_noun_phrases (list): the verb-noun phrases of the second query
        nlp (spacy.lang.en.English): the language model used
        threshold (float): the threshold of similarity
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        dict: the common and uncommon parts of two NL queries
    """
    comparison = dict()
    common_parts = []
    uncommon_parts = dict()
    uncommon_parts["first_query"] = []
    uncommon_parts["second_query"] = []

    # if the root verb-noun phrases are not similar, then the two queries are not really similar
    if is_similar(first_verb_noun_phrases[0], second_verb_noun_phrases[0], nlp, threshold, lemma=lemma, stop_words=stop_words)[0]:
        # since roots are similar, add them to common parts then remove them from the lists
        common_parts.append((first_verb_noun_phrases[0], second_verb_noun_phrases[0]))
        first_verb_noun_phrases.pop(0)
        second_verb_noun_phrases.pop(0)

        # find the common parts
        first_comm_indices = []
        second_comm_indices = []
        for i in range(len(first_verb_noun_phrases)):
            for j in range(len(second_verb_noun_phrases)):
                first_verb_noun_phrase = first_verb_noun_phrases[i]
                second_verb_noun_phrase = second_verb_noun_phrases[j]
                if is_similar(first_verb_noun_phrase, second_verb_noun_phrase, nlp, threshold, lemma=lemma, stop_words=stop_words)[0]:
                    common_parts.append((first_verb_noun_phrase, second_verb_noun_phrase))
                    first_comm_indices.append(i)
                    second_comm_indices.append(j)

        # find the uncommon parts
        for i in range(len(first_verb_noun_phrases)):
            if i not in first_comm_indices:
                uncommon_parts["first_query"].append(first_verb_noun_phrases[i])

        for j in range(len(second_verb_noun_phrases)):
            if j not in second_comm_indices:
                uncommon_parts["second_query"].append(second_verb_noun_phrases[j])

        comparison["common"] = common_parts
        comparison["uncommon"] = uncommon_parts

    return comparison


def compare_queries(first_query, second_query, nlp, threshold=0.7, lemma=True, stop_words=True):
    """Compare two similar NL queries to find the common and uncommon parts

    Args:
        first_query (str): First query
        second_query (str): Second query
        nlp (spacy.lang.en.English): the language model used
        threshold (float, optional): the threshold of similarity. Defaults to 0.7
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        dict: common parts, uncommon parts in first query, uncommon parts in second query
        {
            "common": [],
            "uncommon": {
                "first_query": [],
                "second_query": []
            }
        }
    """
    comparision = dict()

    # find the root verb and extract verb-noun phrases
    first_query_root, first_query_verb_noun_phrases, _ = extract_root_and_verb_noun_phrases(first_query, nlp)
    second_query_root, second_query_verb_noun_phrases, _ = extract_root_and_verb_noun_phrases(second_query, nlp)

    # get common and uncommon parts
    comparision = find_comm_uncomm_parts(first_query_verb_noun_phrases, second_query_verb_noun_phrases, nlp, threshold, lemma=lemma, stop_words=stop_words)

    return comparision


def find_most_similar_part(nl_chunk, verb_noun_phrases_query, nlp, threshold=0.7, lemma=True, stop_words=True):
    """Find similar parts in a query for a chunk.

    Args:
        nl_chunk (str): the chunk
        verb_noun_phrases_query (list): the verb-noun phrases of the query
        nlp (spacy.lang.en.English): the language model used
        threshold (float, optional): the threshold of similarity. Defaults to 0.7
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        idx, str: the index and the verb-noun phrases in the query that are similar to the chunk.
        None if there is no similar part.
    """
    similar_parts = []

    for i, v_n_phrase in enumerate(verb_noun_phrases_query):
        similar, score =  is_similar(nl_chunk, v_n_phrase, nlp, threshold, lemma=lemma, stop_words=stop_words)
        if similar:
            similar_parts.append((score, (i, v_n_phrase)))

    if not similar_parts:
        return None
    
    similar_parts.sort(key=lambda x: x[0], reverse=True)

    most_similar_part = similar_parts[0][1]

    return most_similar_part


def compare_chunk_and_query(nl_chunk, nl_query, nlp, threshold=0.7, lemma=True, stop_words=True):
    """Compare similar chunk and query to find the verb-noun phrase in the query that is most similar to the chunk

    Args:
        nl_chunk (str): the chunk
        nl_query (str): the query
        nlp (spacy.lang.en.English): the language model used
        threshold (float, optional): the threshold of similarity. Defaults to 0.7
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        idx, str: index and the verb-noun phrases in the query that is most similar to the chunk.
        None if there is no similar part.
    """
    # find the root verb and extract verb-noun phrases from the nl query
    root_verb_query, verb_noun_phrases_query, sequence_graph = extract_root_and_verb_noun_phrases(nl_query, nlp)

    # get common and uncommon parts
    return find_most_similar_part(nl_chunk, verb_noun_phrases_query, nlp, threshold, lemma=lemma, stop_words=stop_words)