# build the proposed code as a function from sub snippets of each chunk in the NL query
# Note: the current implementation is preferred when user wants to translate the NL query to a function

from scripts.one_shot_correction.chunking import queries_comparison
from scripts.one_shot_correction.chunking import nl_code_mapping
from scripts.ast_parser import utils as ast_utils
from ordered_set import OrderedSet
from scripts.ast_parser import get_token_types
from collections import OrderedDict


def get_fisrt_direct_object(verb):
    """Get the first direct object of a verb.

    Args:
        verb (spacy.tokens.token.Token): verb token.

    Returns:
        direct_obj (spacy.tokens.token.Token): the first direct object.
    """
    direct_obj = None
    if verb.pos_ == "VERB":
        for child in verb.children:
            if child.dep_ == "dobj":
                direct_obj = child
                break
    elif verb.pos_ == "AUX":
        for child in verb.children:
            if child.dep_ == "attr":
                direct_obj = child
                break

    return direct_obj


def check_child_conditions(child):
    # somehow x is recognized as PUNCT!
    return (child.pos_ not in ["DET", "PUNCT"] or child.text.lower() == "x") and child.dep_ not in ["relcl", "prep"]


def get_subtree_with_child_conditions(node):
    """Get subtree of a node with conditions for children.

    Args:
        node (spacy.tokens.token.Token): node.

    Returns:
        subtree (list(spacy.tokens.token.Token)): list of the tokens in the subtree.
    """
    subtree_nodes = []


    if not node:
        return subtree_nodes
    
    if node.children:
        for child in node.lefts:
            if check_child_conditions(child):
                subtree_nodes.extend(get_subtree_with_child_conditions(child))
        subtree_nodes.append(node)
        for child in node.rights:
            if check_child_conditions(child):
                subtree_nodes.extend(get_subtree_with_child_conditions(child))
    else:
        if check_child_conditions(node):
            subtree_nodes.append(node)

    return subtree_nodes

def get_direct_obj_subtree(verb):
    """Get subtree of the direct object of a verb.

    Args:
        verb (spacy.tokens.token.Token): verb token.

    Returns:
        direct_objs (list(spacy.tokens.token.Token)): list of the tokens in direct object subtree.
    """
    direct_objs = []
    if verb is None or verb.pos_ not in ["VERB", "AUX"]:
        return None
    
    for child in verb.children:
        main_verb_dobj = child.dep_ == "dobj" and child.pos_ == "NOUN"

        # the aux verb conditions are only considered when the main verb doesn't have direct object
        aux_verb_nsubj_attr = verb.pos_ == "AUX" and child.pos_ == "NOUN" and child.dep_ in ["nsubj", "attr"]
        aux_verb_adj = verb.pos_ == "AUX" and child.pos_ == "ADJ"

        if main_verb_dobj or aux_verb_nsubj_attr or aux_verb_adj:
            direct_objs.extend(get_subtree_with_child_conditions(child))

    return direct_objs


def get_verbs(nl_query_doc):
    """Get main verbs in the NL query.
    Main verbs are considered as verbs without auxiliaries.

    Args:
        nl_query_doc (spacy.tokens.doc.Doc): NL query.

    Returns:
        verbs (OrderedSet(spacy.tokens.token.Token)): list of main verbs.
    """
    phrases = queries_comparison.extract_verb_phrases(nl_query_doc)
    string_quotes_indices = queries_comparison.get_string_quotes_indices(nl_query_doc)
    root_verb, non_root_verbs, ignored_verbs = queries_comparison.get_root_non_root_verbs(phrases, string_quotes_indices)

    if root_verb is None:
        return non_root_verbs
    else:
        verbs = OrderedSet([root_verb]) | non_root_verbs

    return verbs


def find_first_aux_of_verb(verb):
    """Find the first auxiliary of a verb.

    Args:
        verb (spacy.tokens.token.Token): verb token.

    Returns:
        aux (spacy.tokens.token.Token): the first auxiliary of the verb.
    """
    aux = None
    if verb.pos_ == "VERB":
        for child in verb.children:
            if child.pos_ in ["AUX"] and child.dep_ in ["ccomp", "advcl"]:
                aux = child
                break
    elif verb.pos_ == "AUX":
        aux = verb

    return aux


def extract_function_name(nl_query, nlp):
    """Extract function name from the NL query.
    Function name is the combination of all main verbs (not aux) in the NL query 
    together with direct object (including children of this object) of the first verb.

    Args:
        nl_query (str): NL query.
        nlp (spacy.lang.en.English): Spacy NLP model.

    Returns:
        function_name (str): function name.
    """
    if not nl_query:
        return None
    
    nl_query_doc = nlp(nl_query)
    verbs = get_verbs(nl_query_doc)
    name_tokens = []

    if len(verbs) == 0:
        # no verb in the query
        # return the first two nouns
        nouns = [noun for noun in queries_comparison.extract_nouns(nl_query_doc)]
        name_tokens.extend([t.text for t in nouns[:2]])
    else:
        # has_direct_obj = False
        for verb in verbs:
            # exlcude v-ing, v-ed, aux
            considered_verb = (verb.i == 0) or (verb.i != 0 and verb.nbor(-1).pos_ != "AUX" and verb.pos_ != "AUX")
            if considered_verb:
                name_tokens.append(verb.lemma_) # use lemma of the verb
                direct_objs = get_direct_obj_subtree(verb)
                if direct_objs:
                    # has_direct_obj = True # only get direct object of the first verb
                    name_tokens.extend([obj.text for obj in direct_objs])
                else:
                    # get object of the aux of this verb
                    aux = find_first_aux_of_verb(verb)
                    if aux is not None:
                        direct_objs = get_direct_obj_subtree(aux)
                        if direct_objs:
                            # has_direct_obj = True
                            name_tokens.extend([obj.text for obj in direct_objs])
    
    function_name = "_".join(name_tokens)
    return function_name


def get_no_start_return_stmts(last_verb, snippets_info, phrase_idx):
    """Get the sub snippets of a verb phrase that don't start with "return" if last_verb is False.
    If the verb phrase is not the last verb phrase in the NL query, it can't start with "return"
        + However, if all sub snippets of the verb phrase start with "return",
        Get the first sub snippet (most similar in the KNNs of the chunk),
        and convert it to "result_stmt_<verb_phrase_idx> = <part after return>"

    Args:
        last_verb (bool): whether the verb phrase is the last verb phrase in the NL query.
        snippets_info (list): list of sub snippets of the verb phrase.
            which is list(list(sub snippet, simi. score, sub snippet explaination), most simi. chunk, simi. query, simi. target code)
        phrase_idx (int): index of the verb phrase in the list of verb phrases in the NL query.

    Returns:
        list(str, float, str): list of (sub snippet, score, snippet explaination) of the verb phrase that don't start with "return" if last_verb is False.
    """
    no_start_return_stmts = []

    if last_verb:
        # get the first non-empty sub snippets
        for snippet_info in snippets_info:
            if snippet_info:
                info, most_simi_chunk, simi_query, simi_target_code = snippet_info
                if info:
                    for item in info:
                        no_start_return_stmts.append(item)
                    return no_start_return_stmts
    else: # not last verb
        marked_return = None
        for snippet_info in snippets_info:
            if snippet_info:
                info, most_simi_chunk, simi_query, simi_target_code = snippet_info
                if info:
                    first_statement = info[0][0]
                    if not first_statement.strip().startswith("return"):
                        for item in info:
                            no_start_return_stmts.append(item)
                        break
                    else:
                        if marked_return is None:
                            marked_return = info # first non-empty info starting with "return"
        if not no_start_return_stmts:
            # all sub snippets start with "return"
            # assuming that only one "return" stmt  in the list of sub snippets
            if marked_return:
                first_statement = marked_return[0][0]
                no_start_return_stmts.append(("result_stmt_{} = {}".format(phrase_idx, first_statement.strip()[6:].strip()),
                                              marked_return[0][1], marked_return[0][2]))
                
                # programmatically, there shouldn't be any statements after return
                # for item in marked_return[1:]:
                #     no_start_return_stmts.append(item)

    return no_start_return_stmts


def get_reduced_plural_stmts(verb, nlp, no_start_return_stmts,
                             reduce_threshold=0.85, lemma=True, check_root_verb=False, stop_words=True):
    """Get the reduced list of sub snippets of a verb phrase that have singular direct objects.
    If the direct object in the verb phrase is singular,
        + Check if there are snippets that are "very similar" to each other,
        + Reduce these snippets to only one of them
    If the direct object in the verb phrase is plural without specific number,
        + Keep the snippet as it is 
    If the direct object in the verb phrase is plural with specific number,
        + Keep the snippet as it is 

    Args:
        verb (spacy.tokens.token.Token): verb of the verb phrase.
        nlp (spacy.lang.en.English): Spacy NLP model.
        no_start_return_stmts (list(str, float, str)): list of sub snippets of the verb phrase that don't start with "return" if last_verb is False.
        reduce_threshold (float): threshold for snippets similarity. Defaults to 0.85.
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        (list): reduced list of sub snippets of the verb phrase that have plural direct objects.
    """
    if verb is None:
        return no_start_return_stmts
    
    direct_obj = get_fisrt_direct_object(verb)

    if direct_obj is None:
        return no_start_return_stmts

    singular_direct_obj = direct_obj.tag_ == "NN"
    plural_direct_obj = direct_obj.tag_ == "NNS"

    if plural_direct_obj:
        return no_start_return_stmts
    elif singular_direct_obj:
        reduced_plural_stmts = nl_code_mapping.reduce_similar_snippets(no_start_return_stmts, nlp, 
                                                                       reduce_threshold, lemma, check_root_verb, stop_words)
        return reduced_plural_stmts
    
    return no_start_return_stmts


def get_after_return(return_stmt):
    """Check if after a return statement is a variable and return this part.

    Args:
        return_stmt (str): statement to check.
    
    Returns:
        bool, str: whether after a return statement is a variable, and the after return part.
    """
    if not return_stmt:
        return None, None
    
    if not return_stmt.strip().startswith("return"):
        return False, return_stmt
    
    # get AST sub snippets
    tree = ast_utils.get_tree_sitter_tree(return_stmt.strip())
    root_ast = tree.root_node

    after_return_node = root_ast.named_children[0].named_children[0] # module -> return statement -> after return
    is_var = after_return_node.type in ["identifier", "expression_list"]

    after_return_text = return_stmt.strip()[after_return_node.start_byte:after_return_node.end_byte]

    return is_var, after_return_text


def get_refined_between_return_stmts(last_verb, reduced_plurals_stmts, phrase_idx):
    """Get the refined list of sub snippets of a verb phrase that have "return" statement in it.
    If the verb phrase is not the last verb phrase and has "return" statement in it,
        + If after "return" is a variable, delete return statement
        + If after "return" is not a variable, convert it to "result_<verb_phrase_idx> = <part after return>"
        + Delete all statements after "return" statement
    If the verb phrase is the last verb phrase and has "return" statement in it,
        + Delete all statements after "return" statement

    Args:
        last_verb (bool): whether the verb phrase is the last verb phrase in the NL query.
        reduced_plurals_stmts (list): reduced list of sub snippets of the verb phrase that have singular direct objects.
        phrase_idx (int): index of the verb phrase in the list of verb phrases in the NL query.

    Returns:
        (list): refined list of sub snippets of the verb phrase that have "return" statement in it.
    """
    no_between_return_stmts = []

    for statement_info in reduced_plurals_stmts:
        stmt, score, explanation = statement_info
        if not stmt.strip().startswith("return"):
            no_between_return_stmts.append(stmt)
        else:
            if last_verb:
                no_between_return_stmts.append(stmt)
                break
            else:
                is_var, after_return = get_after_return(stmt)
                if (is_var is not None) and (is_var is False):
                    no_between_return_stmts.append("result_stmt_{} = {}".format(phrase_idx, after_return))
                break # programmatically, statements after return are invalid
    
    return no_between_return_stmts


def separate_import_stmts(statements):
    """Extract import and import from statements.

    Args:
        statements (list(str)): a list of statements.

    Returns:
        list(str), list(str): A list of import, import from statements and the remaining statements.
    """
    import_stmts = []
    no_import_stmts = []
    for statement in statements:
        if statement.startswith("import") or statement.startswith("from"):
            import_stmts.append(statement)
        else:
            no_import_stmts.append(statement)
    
    return import_stmts, no_import_stmts


def get_snippets_by_rules(last_verb, verb, verb_phrase, sub_snippets, nl_query, nlp, knn_indices, distances, corrt_ds, phrase_idx,
                          reduce_threshold=0.85, lemma=True, check_root_verb=False, stop_words=True,
                          refine_no_start_return=True,
                        refine_reduce_plural=True,
                        refine_between_return=True):
    """Get most suitable sub snippets of a verb phrase by rules.

    Rules:
        1. See rules in :func:`get_no_start_return_stmts`

        2. See rules in :func:`get_reduced_plural_stmts`

        3. See rules in :func:`get_refined_between_return_stmts`

    Args:
        verb (spacy.tokens.token.Token): verb.
        last_verb (bool): whether the verb phrase is the last verb phrase in the NL query.
        verb_phrase (str): verb phrase.
        sub_snippets (dict(str: list)): sub snippets of each chunk in the NL query.
            Keys are chunks in NL query,
            values are list(list(sub snippet, simi. score, sub snippet explaination), most simi. chunk, simi. query, simi. target code)
        nl_query (str): NL query.
        nlp (spacy.lang.en.English): Spacy NLP model.
        knn_indices (list): list of indices of nearest neighbors in correction_ds.
        distances (list): list of distances of nearest neighbors in correction_ds.
        corrt_ds (CorrectionDS): correction datastore.
        phrase_idx (int): index of the verb phrase in the list of verb phrases in the NL query.
        reduce_threshold (float): threshold for snippets similarity. Defaults to 0.85.
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        list(str): list of import statements, list of sub snippets of the verb phrase.
    """
    snippets_info = sub_snippets[verb_phrase]

    if not snippets_info:
        print("No sub snippets either from CorrectionDS or generated for verb phrase: {}".format(verb_phrase))
        return []
    
    result_stmts = []

    if refine_no_start_return:
        result_stmts = get_no_start_return_stmts(last_verb, snippets_info, phrase_idx)
    else:
        # get the first non-empty sub snippets
        for snippet_info in snippets_info:
            if snippet_info:
                info, most_simi_chunk, simi_query, simi_target_code = snippet_info
                if info:
                    for item in info:
                        result_stmts.append(item)
                    break

    if refine_reduce_plural:
        result_stmts = get_reduced_plural_stmts(verb, nlp, result_stmts,
                                                     reduce_threshold, lemma, check_root_verb, stop_words)
    else:
        pass

    if refine_between_return:
        result_stmts = get_refined_between_return_stmts(last_verb, result_stmts, phrase_idx)
    else:
        # return only the sub-snippet
        result_stmts = [stmt for stmt, _, _ in result_stmts]

    return result_stmts


def get_vars_def_usg_strs(snippets, string_len_threshold=15):
    """Get variable definitions and usages from snippets.

    Args:
        snippets (list): list of sub snippets of a verb phrase.
        string_len_threshold (int, optional): threshold for string length to be replaced later. Defaults to 10.

    Returns:
        OrderedSet(str), OrderedSet(str), OrderedDict(str: tuple(int, int)), OrderedDict(str: tuple(int, int)):
        + Ordered set of variable definitions.
        + Ordered set of variable usages.
        + Ordered dictionary of variable positions in the snippet.
        + Ordered dictionary of string positions in the snippet.
        + Ordered set of import lib, import sub lib and alias
        + Ordered set of argument definitions.
    """
    vars_def = OrderedSet()
    vars_pos = OrderedDict()
    vars_usg = OrderedSet()
    strings_pos = OrderedDict()
    import_libs_sublibs_alias = OrderedSet()
    arg_defs = OrderedSet()

    if not snippets:
        return vars_def, vars_usg, vars_pos, strings_pos, import_libs_sublibs_alias, arg_defs
    
    tree = ast_utils.get_tree_sitter_tree("\n".join(snippets))
    root_ast = tree.root_node
    dfs_sequence_ast = ast_utils.get_dfs_sequence(root_ast, [])
    start_end_byte_info = {}
    for node in dfs_sequence_ast:
        start_end_byte_info[node.id] = (node.start_byte, node.end_byte)

    types_per_token = get_token_types.get_types_per_token(tree, categories=["SyntaxType"])
    
    for n_id, info in types_per_token.items():
        token = info["token"]
        token_types = info["ct3"] #[["type1", "type2", ...]] or None
        if token_types is not None:
            if "arg_def" in token_types[0]:
                arg_defs.add(token)
            elif ("imp_alias" in token_types[0]) or ("imp_lib" in token_types[0]) or ("imp_sublib" in token_types[0]):
                import_libs_sublibs_alias.add(token)
            elif "var_def" in token_types[0]: # only one token type per token
                vars_def.add(token)
                if token not in vars_pos:
                    vars_pos[token] = OrderedSet()
                vars_pos[token].add(start_end_byte_info[n_id])
            elif "var_usg" in token_types[0]:
                vars_usg.add(token)
                if token not in vars_pos:
                    vars_pos[token] = OrderedSet()
                vars_pos[token].add(start_end_byte_info[n_id])
            elif "const_str" in token_types[0]:
                if len(token) > string_len_threshold:
                    if token not in strings_pos:
                        strings_pos[token] = OrderedSet()
                    strings_pos[token].add((start_end_byte_info[n_id][0]+1, start_end_byte_info[n_id][1]-1)) # quotes are excluded

    return vars_def, vars_usg, vars_pos, strings_pos, import_libs_sublibs_alias, arg_defs


def replace_vars_def(statements, considered_vars_def, considered_undef_vars, vars_pos,
                     reduce_strings=True, default_string="<update string>", stmt_strings_pos=OrderedDict()):
    """Replace variable definitions in the current statement by the undefined variables of the next statement,
    starts from the corresponding position of the variable definition.

    Args:
        statements (list): list of statements.
        considered_vars_def (OrderedSet(str)): ordered set of considered variable definitions.
        considered_undef_vars (OrderedSet(str)): ordered set of considered undefined variables.
        vars_def_or_usg_pos (OrderedDict(str: OrderedSet(tuple(int, int)))): ordered dict of variable definitions or usages and their positions.
        reduce_strings (bool): whether to reduce strings.
        default_string (str): default string to replace the strings.
        stmt_strings_pos (OrderedDict(str: OrderedSet(tuple(int, int)))): ordered dict of strings and their positions in the current statement.

    Returns:
        str: the refined statements in one string.
    """
    if not statements:
        return ""
    
    stmt_text = "\n".join(statements)

    assert len(considered_vars_def) == len(considered_undef_vars)

    # extract all the positions that need to be replaced first, together with the replacing values,
    # then replace them in the order of the positions
    total_replace_poss = []
    for var_def, undef_var in zip(considered_vars_def, considered_undef_vars):
        replace_poss = vars_pos[var_def]
        for replace_pos in replace_poss:
            total_replace_poss.append((replace_pos[0], replace_pos[1], var_def, undef_var))

    if reduce_strings:
        for stmt_str, pos_info in stmt_strings_pos.items():
            for pos in pos_info:
                total_replace_poss.append((pos[0], pos[1], stmt_str, default_string))

    total_replace_poss.sort(key=lambda x: x[0])

    refined_stmt = ""
    start_idx = 0
    for replace_info in total_replace_poss:
        refined_stmt += stmt_text[start_idx:replace_info[0]] + replace_info[3]
        start_idx = replace_info[1]
    refined_stmt += stmt_text[start_idx:]

    return refined_stmt


def reduce_strings_in_stmts(statements, strings_pos, default_string):
    """Reduce strings in statements.

    Args:
        statements (list): list of statements.
        strings_pos (OrderedDict(str: OrderedSet(tuple(int, int)))): ordered dict of strings and their positions in the current statement.
        default_string (str): default string to replace the strings.

    Returns:
        str: the refined statements in one string.
    """
    if not statements:
        return ""

    stmt_text = "\n".join(statements)

    total_replace_poss = []
    for stmt_str, pos_info in strings_pos.items():
        for pos in pos_info:
            total_replace_poss.append((pos[0], pos[1], stmt_str, default_string))

    total_replace_poss.sort(key=lambda x: x[0])

    refined_stmt = ""
    start_idx = 0
    for replace_info in total_replace_poss:
        refined_stmt += stmt_text[start_idx:replace_info[0]] + replace_info[3]
        start_idx = replace_info[1]
    refined_stmt += stmt_text[start_idx:]

    return refined_stmt


def rename_identifiers(mapping, reduce_strings=True, default_string="<update string>", string_len_threshold=15):
    """Rename identifiers in a mapping of verb phrases and their sub snippets.
    Assumption:
        + Variables used in the current statement are defined in the statement right above it with THE SAME ORDER!

    Args:
        mapping (dict(str: list)): mapping of verb phrases and their sub snippets.
            Keys are verb phrases,
            values are list(str) of sub snippets of the verb phrase.
        reduce_strings (bool): whether to reduce strings or not. Default: True.
        default_string (str, optional): default string to replace the string in the sub snippet. Defaults to "<update string>".
        string_len_threshold (int, optional): threshold for string length to be replaced later. Defaults to 10.
    
    Returns:
        mapping (dict(str: str)): mapping of verb phrases and their sub snippets with new names of identifiers
        OrderedSet(str): list of undefined identifiers in the mapping.
    """
    undefined_vars = OrderedSet()
    refined_statements = []
    refined_mapping = {}

    if not mapping:
        return refined_mapping, undefined_vars

    verb_phrases = list(mapping.keys())

    last_statements = mapping[verb_phrases[-1]]
    last_stmt_vars_def, last_stmt_vars_usg, _, last_stmt_strings_pos, last_stmt_libs, last_stmt_arg_defs = get_vars_def_usg_strs(last_statements, string_len_threshold)
    undefined_vars = last_stmt_vars_usg - last_stmt_libs - last_stmt_arg_defs - last_stmt_vars_def # union OrderedSets

    if reduce_strings:
        updated_last_stmt = reduce_strings_in_stmts(last_statements, last_stmt_strings_pos, default_string)
        refined_statements.insert(0, updated_last_stmt)
    else:
        refined_statements.insert(0, "\n".join(last_statements))

    for i in range(len(verb_phrases) - 2, -1, -1):
        statements = mapping[verb_phrases[i]]

        considered_vars_def = OrderedSet()
        considered_undef_vars = OrderedSet()

        stmt_vars_def, stmt_vars_usg, stmt_vars_pos, stmt_strings_pos, stmt_libs, stmt_arg_defs = get_vars_def_usg_strs(statements, string_len_threshold)
        stmt_undefined_vars = stmt_vars_usg - stmt_libs - stmt_arg_defs - stmt_vars_def
        refined_stmt_var_def = stmt_vars_def - stmt_vars_usg

        refined_stmt = "\n".join(statements)
        if not refined_stmt_var_def:
            if not stmt_vars_usg:
                # no variable definition and usage
                refined_stmt = "\n".join(statements)
            else:
                # no variable definition but usage
                if len(undefined_vars) == len(stmt_vars_usg):
                    considered_vars_def = stmt_vars_usg
                    considered_undef_vars = undefined_vars

                elif len(undefined_vars) < len(stmt_vars_usg):
                    considered_vars_def = stmt_vars_usg[-len(undefined_vars):] 
                    considered_undef_vars = undefined_vars

                    undefined_vars = stmt_undefined_vars - considered_vars_def
                else:
                    considered_vars_def = stmt_vars_usg
                    considered_undef_vars = undefined_vars[:len(stmt_vars_usg)] 

                if considered_vars_def and considered_undef_vars:
                    # only replace when both list are not empty
                    refined_stmt = replace_vars_def(statements, considered_vars_def, considered_undef_vars, stmt_vars_pos, 
                                                reduce_strings, default_string, stmt_strings_pos)
        else:
            if len(undefined_vars) == len(refined_stmt_var_def):
                # replace all vars def by undefined vars of the next stmt
                considered_vars_def = refined_stmt_var_def
                considered_undef_vars = undefined_vars
                
                undefined_vars = stmt_undefined_vars
            elif len(undefined_vars) < len(refined_stmt_var_def):
                # replace only the last vars def by undefined vars of the next stmt
                considered_vars_def = refined_stmt_var_def[-len(undefined_vars):]
                considered_undef_vars = undefined_vars

                undefined_vars = stmt_undefined_vars
            else:
                # replace all vars def by last undefined vars of the next stmt
                considered_vars_def = refined_stmt_var_def
                considered_undef_vars = undefined_vars[:len(refined_stmt_var_def)] 

                undefined_vars = stmt_undefined_vars | undefined_vars[len(refined_stmt_var_def):]

            if considered_vars_def and considered_undef_vars:
                # only replace when both list are not empty
                refined_stmt = replace_vars_def(statements, considered_vars_def, considered_undef_vars, stmt_vars_pos,
                                            reduce_strings, default_string, stmt_strings_pos)

        refined_statements.insert(0, refined_stmt)

    for i, phrase in enumerate(verb_phrases):
        refined_mapping[phrase] = refined_statements[i]

    return refined_mapping, undefined_vars

def build_function_body(nl_query, sub_snippets, nlp, knn_indices, distances, corrt_ds, sequence_graph,
                        reduce_threshold=0.85, lemma=True, check_root_verb=False, stop_words=True, 
                        reduce_strings=True, default_string="<update string>", string_len_threshold=15,
                        refine_no_start_return=True,
                        refine_reduce_plural=True,
                        refine_between_return=True):
    """Build the body of the function from sub snippets of each chunk in the NL query.

    Args:
        nl_query (str): NL query.
        sub_snippets (dict(str: list)): sub snippets of each chunk in the NL query.
            Keys are chunks in NL query,
            values are list(list(sub snippet, simi. score, sub snippet explaination), most simi. chunk, simi. query, simi. target code)
        nlp (spacy.lang.en.English): Spacy NLP model.
        knn_indices (list): list of indices of nearest neighbors in correction_ds.
        distances (list): list of distances of nearest neighbors in correction_ds.
        corrt_ds (CorrectionDS): correction datastore.
        sequence_graph (dict): the sequence graph of actions of the NL query.
            {
                verb_idx: {
                    "verb": spacy token,
                    "imme_after": verb idx,
                    "phrase": str
                }
            }
        reduce_threshold (float): threshold for snippets similarity. Defaults to 0.85.
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.
        reduce_strings (bool, optional): whether to reduce strings or not. Defaults to True.
        default_string (str, optional): default string to replace the string in the sub snippet. Defaults to "<update string>".
        string_len_threshold (int, optional): threshold for string length to be replaced later. Defaults to 10.

    Returns:
        function_body (str): the body of the function.
        undefined_vars (list(str)): list of undefined variables in the function body.
        mapping (dict): a dict map chunk and its most suitable sub snippets.
    """
    mapping = {}
    num_verbs = len(list(sequence_graph.keys()))

    if not nl_query or not sub_snippets:
        return "", [], mapping

    if num_verbs == 0:
        # no verb in the query and so only one chunk
        one_verb_phrase = list(sub_snippets.keys())[0]
        verb_snippets = get_snippets_by_rules(True, None, one_verb_phrase, sub_snippets, nl_query, nlp, knn_indices, distances, corrt_ds, 
                                              phrase_idx=0, 
                                              reduce_threshold=reduce_threshold, lemma=lemma, 
                                              check_root_verb=check_root_verb, stop_words=stop_words,
                                              refine_no_start_return=refine_no_start_return,
                                            refine_reduce_plural=refine_reduce_plural,
                                            refine_between_return=refine_between_return)
        mapping[one_verb_phrase] = verb_snippets
    else:
        for i, verb_idx in enumerate(list(sequence_graph.keys())):
            verb_info = sequence_graph[verb_idx]
            verb_phrase = verb_info["phrase"]
            verb = verb_info["verb"]
            last_verb = i == num_verbs - 1
            verb_snippets = get_snippets_by_rules(last_verb, verb, verb_phrase, sub_snippets, nl_query, nlp, knn_indices, distances, corrt_ds, 
                                                  phrase_idx=i,
                                                  reduce_threshold=reduce_threshold, lemma=lemma, 
                                                  check_root_verb=check_root_verb, stop_words=stop_words,
                                                  refine_no_start_return=refine_no_start_return,
                                            refine_reduce_plural=refine_reduce_plural,
                                            refine_between_return=refine_between_return)
            mapping[verb_phrase] = verb_snippets

    refined_mapping, undefined_vars = rename_identifiers(mapping, reduce_strings, default_string, string_len_threshold)
    function_body = []
    for _, snippets in refined_mapping.items():
        function_body.append(snippets)

    return "\n".join(function_body), undefined_vars, refined_mapping


def build_params(nl_query, nlp, undefined_vars):
    """Return a list of parameters of the function.
    """
    if not undefined_vars:
        return "()"
    return "({})".format(", ".join(undefined_vars))


def add_closed_stmt(function_text, no_closure=True):
    """Add a return or print statement to the end of the function text 
    if the function doesn't have print or return in its body.
    
    Args:
        function_text (str): function text.
        no_closure (bool, optional): whether to add return or print statement. Defaults to True.

    Returns:
        function_text (str): function text with return statement.
    """
    if not function_text:
        return ""

    if not no_closure:
        return function_text

    var_defs, var_usgs, _, _, libs, _ = get_vars_def_usg_strs([function_text])
    if var_defs:
        return function_text + "\n    return {}".format(", ".join(var_defs))
    elif (var_usgs - libs): 
        return function_text + "\n    return {}".format(", ".join(var_usgs - libs))
    else:
        return function_text + "\n    print(\"Print the results here\")"


def build_function(function_name, params, function_body):
    """Build the function from function name, params and function body.
    """
    if not function_name or not function_body:
        return ""
    
    body_indent = []
    import_stmts = []
    no_closure = True
    for line in function_body.split("\n"):
        if line.strip().startswith("import") or line.strip().startswith("from"):
            import_stmts.append(line.strip())
        else:
            if len(line) == len(line.lstrip()): # line doesn't have indent
                body_indent.append("    " + line)
            else:
                body_indent.append(line) # line has indent
                
            if line.strip().startswith("print") or line.strip().startswith("return"):
                no_closure = False

    if import_stmts:
        function_text =  "{}\n\ndef {}{}:\n{}".format("\n".join(import_stmts), function_name, params, "\n".join(body_indent))
    else:
        function_text =  "def {}{}:\n{}".format(function_name, params, "\n".join(body_indent))

    function_text = add_closed_stmt(function_text, no_closure)

    return function_text


def build_code_from_sub_snippets(nl_query, sub_snippets, nlp, knn_indices, distances, corrt_ds, sequence_graph,
                                 reduce_threshold=0.85, lemma=True, check_root_verb=False, stop_words=True, 
                                 reduce_strings=True, default_string="<update string>", string_len_threshold=15,
                                 refine_no_start_return=True,
                                refine_reduce_plural=True,
                                refine_between_return=True):
    """Build code from sub snippets of each chunk in the NL query.

    Args:
        nl_query (str): NL query.
        sub_snippets (dict(str: list)): sub snippets of each chunk in the NL query.
            Keys are chunks in NL query, 
            values are list(list(sub snippet, simi. score, sub snippet explaination), most simi.chunk, simi. query, simi. target code)
        nlp (spacy.lang.en.English): Spacy NLP model.
        knn_indices (list): list of indices of nearest neighbors in correction_ds.
        distances (list): list of distances of nearest neighbors in correction_ds.
        corrt_ds (CorrectionDS): correction datastore.
        sequence_graph (dict): the sequence graph of actions of the NL query.
            {
                verb_idx: {
                    "verb": spacy token,
                    "imme_after": verb idx,
                    "phrase": str
                }
            }
        reduce_threshold (float): threshold for snippets similarity. Defaults to 0.85.
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.
        reduce_strings (bool, optional): whether to reduce strings. Defaults to True.
        default_string (str, optional): default string to replace the string in the sub snippet. Defaults to "<update string>".
        string_len_threshold (int, optional): threshold for string length to be replaced later. Defaults to 10.
    
    Returns:
        code (str): code built from sub snippets.
        mappping (dict): mapping between sub snippets used to build the code and chunks in NL query.
    """
    # if the nl_query starts with "Function", extract function name from all verbs,
    # and return the empty function

    # start with the first element in sequence_graph
    # detect if there is any amount number in the phrase,
    # if yes then multiply the sub snippets of the phrase
    # extract variable definition from sub snippets to use in later snippets
    
    function_name = extract_function_name(nl_query, nlp)

    function_body, undefined_vars, mapping = build_function_body(nl_query, sub_snippets, nlp, knn_indices, distances, corrt_ds, sequence_graph,
                                                                 reduce_threshold, lemma, check_root_verb, stop_words, 
                                                                 reduce_strings, default_string, string_len_threshold,
                                                                 refine_no_start_return=refine_no_start_return,
                                                                    refine_reduce_plural=refine_reduce_plural,
                                                                    refine_between_return=refine_between_return)

    params = build_params(nl_query, nlp, undefined_vars)

    code = build_function(function_name, params, function_body)

    return code, mapping