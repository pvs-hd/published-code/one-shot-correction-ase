# Given a NL query and its target code in AST, map the verb-noun phrases of the query to the AST nodes.

from scripts.ast_parser import utils as ast_utils
from scripts.one_shot_correction.chunking import queries_comparison
from ordered_set import OrderedSet


def get_ancestor_ids(node):
    """Get the ancestor ids of a node.
    """
    if not ast_utils.is_root(node):
        ancestor_ids = get_ancestor_ids(node.parent)
        ancestor_ids.add(node.parent.id)
    else:
        ancestor_ids = OrderedSet()

    return ancestor_ids


def extract_sub_snippets(dfs_sequence_ast):
    """Extract sub snippets from an AST.
    Since we assume the NL query is about a single function, 
    we extract sub snippets from the function body

    Args:
        dfs_sequence_ast (list(tree-sitter node)): list of nodes in DFS sequence

    Returns:
        list(tree-sitter node, bool): list of root nodes of sub snippets with their corresponding flag.
        The flag indicates whether the sub snippet is a function definition or import statement.
    """
    root_sub_snippets = []
    considered_roots = OrderedSet()
    for i, node in enumerate(dfs_sequence_ast):
        if node.type in ["import_statement", "import_from_statement"]:
            root_sub_snippets.append((node, True))
            considered_roots.append(node.id)
        elif node.parent and node.parent.parent and node.parent.type == "block" and node.parent.parent.type == "function_definition":
            root_sub_snippets.append((node, True))
            considered_roots.append(node.id)
        elif node.type.endswith("statement") and (not bool(get_ancestor_ids(node) & considered_roots)): # in case the snippet doesn't have a function definition
            root_sub_snippets.append((node, False))
            considered_roots.append(node.id)
    
    return root_sub_snippets


def parse_sub_snippet(root_snippet, in_func_import, has_string=True, has_comment=True):
    """Parse a sub snippet from the AST.

    Args:
        root_snippet (tree-sitter node): root node of the sub snippet
        in_func_import (bool): whether the sub snippet is a function definition or import statement
        has_string (bool, optional): whether to include string in the explaination. Defaults to True.
        has_comment (bool, optional): whether to include comment in the explaination. Defaults to True.

    Returns:
        str: text of the sub snippet
        list(tree-sitter token): list of tokens in the sub snippet
    """
    sub_snippet_text = ast_utils.get_text(root_snippet)

    if not in_func_import:
        # add indent for each line
        sub_snippet_text = "\n".join(["    " + line for line in sub_snippet_text.split("\n")])

    sub_snippet_explaination = ast_utils.explain_simply(root_snippet, [], has_string=has_string, has_comment=has_comment)

    return sub_snippet_text, sub_snippet_explaination


def is_similar(verb_noun_phrase, sub_snippet_explaination, nlp, threshold=0.5, lemma=True, check_root_verb=True, stop_words=True):
    """Check if two list of tokens have overlap.

    Args:
        verb_noun_phrase (str): the verb-noun phrase in string format
        sub_snippet_explanation (str): the sub snippet explaination in string format
        nlp (spacy.lang.en.English): Spacy model
        threshold (float, optional): threshold for similarity. Defaults to 0.5.
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        bool, similarity: True if two string are similar, False otherwise and the similarity score
    """
    if check_root_verb:
        root_verb_nl, _, _ = queries_comparison.extract_root_and_verb_noun_phrases(verb_noun_phrase, nlp)
        root_verb_snippet, _, _ = queries_comparison.extract_root_and_verb_noun_phrases(sub_snippet_explaination, nlp)

        if root_verb_snippet is None:
            root_verb_snippet = nlp(sub_snippet_explaination)[0]

        strict_unmatch = root_verb_nl is None or \
            root_verb_snippet is None and \
            ( # we don't compare stop words for root verbs
                not root_verb_nl.is_stop and \
                not root_verb_snippet.is_stop
            )

        if strict_unmatch:
            return False, -1
        
        if lemma:
            root_verb_nl_lemma = root_verb_nl.lemma_
            root_verb_snippet_lemma = root_verb_snippet.lemma_

            if nlp(root_verb_nl_lemma).similarity(nlp(root_verb_snippet_lemma)) < threshold:
                return False, -1
        else:
            if root_verb_nl.similarity(root_verb_snippet) < threshold:
                return False, -1

    
    return queries_comparison.is_similar(verb_noun_phrase, sub_snippet_explaination, nlp, threshold, lemma=lemma, stop_words=stop_words)


def map_verb_noun_phrases_to_sub_snippets(verb_noun_phrases, sub_snippets, nlp, threshold=0.5, 
                                          lemma=True, check_root_verb=True, has_string=True, has_comment=True, stop_words=True,
                                          filter_import=True):
    """Map verb-noun phrases to sub snippets.

    Args:
        verb_noun_phrases (list(str)): list of verb-noun phrases
        sub_snippets (list(tree-sitter node, in_func_import)): list of root nodes of sub snippets
            with their corresponding DFS index
        nlp (spacy.lang.en.English): Spacy model
        threshold (float, optional): threshold for similarity. Defaults to 0.5.
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        has_string (bool, optional): whether to include string in the explaination. Defaults to True.
        has_comment (bool, optional): whether to include comment in the explaination. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        dict(str: list(str, float, str)): dict of verb-noun phrases and their corresponding code snippets, similarity scores, and sub snippet explainations
    """
    verb_noun_phrases_to_code = dict()
    temp_results = dict()

    if len(verb_noun_phrases) <= 1:
        return verb_noun_phrases_to_code
    else:
        # if there are more than one verb-noun phrases,
        # if the number of verbs is the same as the number of sub snippets,
        # we can map each verb-noun phrase to a sub snippet
        if len(verb_noun_phrases) == len(sub_snippets):
            one_one_mapping = True
        elif filter_import and (len(verb_noun_phrases) == sum(1 for sub_snippet, flag in sub_snippets if "import" not in sub_snippet.type)):
            one_one_mapping = True
        else:
            one_one_mapping = False

        # consider each pair of verb-noun phrases and sub snippets
        # for the pairs of same sub snippet, one with the highest similarity score (>= threshold) is the mapping
        considered_verb_noun_phrases = set()
        unmapped_sub_snippets = OrderedSet()

        # filter import statements first
        if filter_import:
            filtered_sub_snippets = []
            for sub_snippet in sub_snippets:
                root_snippet, in_func_import = sub_snippet
                sub_snippet_text, sub_snippet_explaination = parse_sub_snippet(root_snippet, in_func_import, has_string=has_string, has_comment=has_comment)

                if sub_snippet_text.lstrip().startswith("import") or sub_snippet_text.lstrip().startswith("from"):
                    # we always map the import statements to the root verb-noun phrase
                    temp_results[sub_snippet_text] = (verb_noun_phrases[0], 0.0, " ".join(sub_snippet_explaination))
                    # the root verb-noun phrase still can be mapped to other sub snippets later
                else:
                    filtered_sub_snippets.append(sub_snippet)
        else:
            filtered_sub_snippets = sub_snippets
        
        for snippet_idx, sub_snippet in enumerate(filtered_sub_snippets):
            root_snippet, in_func_import = sub_snippet
            sub_snippet_text, sub_snippet_explaination = parse_sub_snippet(root_snippet, in_func_import, has_string=has_string, has_comment=has_comment)
            simi_phrases = []

            for phrase_idx, verb_noun_phrase in enumerate(verb_noun_phrases):
                similar, score = is_similar(verb_noun_phrase, " ".join(sub_snippet_explaination), nlp, threshold, 
                                            lemma=lemma, check_root_verb=check_root_verb, stop_words=stop_words)
                if similar:
                    simi_phrases.append((verb_noun_phrase, score, phrase_idx))

            if simi_phrases:
                # sort the similar verb-noun phrases by their similarity score
                simi_phrases = sorted(simi_phrases, key=lambda x: x[1], reverse=True)
                if one_one_mapping:
                    for simi_phrase, simi_score, simi_phrase_idx in simi_phrases:
                        if (simi_phrase not in considered_verb_noun_phrases) and (snippet_idx == simi_phrase_idx):
                            temp_results[sub_snippet_text] = (simi_phrase, simi_score, " ".join(sub_snippet_explaination))
                            considered_verb_noun_phrases.add(simi_phrase)
                            break

                    if sub_snippet_text not in temp_results:
                        unmapped_sub_snippets.add((sub_snippet_text, " ".join(sub_snippet_explaination)))
                else:
                    # one phrase can be mapped to multiple sub snippets
                    temp_results[sub_snippet_text] = (simi_phrases[0][0], simi_phrases[0][1], " ".join(sub_snippet_explaination))
                    considered_verb_noun_phrases.add(simi_phrases[0][0])
            else:
                # no mapping for this sub snippet
                # add this snippet to root verb-noun phrase
                if one_one_mapping:
                    if (verb_noun_phrase[0] not in considered_verb_noun_phrases) and (snippet_idx == 0):
                        temp_results[sub_snippet_text] = (verb_noun_phrase[0], 0.0, " ".join(sub_snippet_explaination))
                        considered_verb_noun_phrases.add(verb_noun_phrase[0])
                    else:
                        unmapped_sub_snippets.add((sub_snippet_text, " ".join(sub_snippet_explaination)))
                else:
                    temp_results[sub_snippet_text] = (verb_noun_phrases[0], 0.0, " ".join(sub_snippet_explaination)) # root verb
                    considered_verb_noun_phrases.add(verb_noun_phrases[0])
        
        # convert to another dict
        for snippet, info in temp_results.items():
            phrase, score, explaination = info
            if phrase not in verb_noun_phrases_to_code:
                verb_noun_phrases_to_code[phrase] = [(snippet, score, explaination)]
            else:
                verb_noun_phrases_to_code[phrase].append((snippet, score, explaination))

        # handle unmapped sub snippets if one-one mapping is True
        if one_one_mapping and unmapped_sub_snippets:
            # find unmapped verb-noun phrases
            unmapped_phrases = OrderedSet()
            for verb_noun_phrase in verb_noun_phrases:
                if verb_noun_phrase not in considered_verb_noun_phrases:
                    unmapped_phrases.add(verb_noun_phrase)
            
            # map phrases to sub snippets by their order
            assert len(unmapped_phrases) == len(unmapped_sub_snippets)
            for phrase, unmapped_sub_snippet in zip(unmapped_phrases, unmapped_sub_snippets):
                sub_snippet_text, sub_snippet_explaination = unmapped_sub_snippet
                verb_noun_phrases_to_code[phrase] = [(sub_snippet_text, 0.0, sub_snippet_explaination)]


    return verb_noun_phrases_to_code


def map_nl_ast(nl_query, target_code, nlp, threshold=0.5, lemma=True, check_root_verb=True, has_string=True, has_comment=True, stop_words=True):
    """Given a NL query and its target code in AST, map the verb-noun phrases of the query to sub snippets of the code.

    Args:
        nl_query (str): NL query
        target_code (str): target code of the query
        nlp (spacy.lang.en.English): Spacy model
        threshold (float, optional): threshold for similarity. Defaults to 0.5.
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        has_string (bool, optional): whether to include string in the explaination. Defaults to True.
        has_comment (bool, optional): whether to include comment in the explaination. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        dict{str: list(str, float, str)}: dict of verb-noun phrases and their corresponding code snippets, 
            similarity scores, and sub snippet explainations
    """
    # get AST sub snippets
    tree = ast_utils.get_tree_sitter_tree(target_code)
    root_ast = tree.root_node
    dfs_sequence_ast = ast_utils.get_dfs_sequence(root_ast, [])
    sub_snippets = extract_sub_snippets(dfs_sequence_ast)

    # list of sub snippets in target code
    function_body = []
    for node_info in sub_snippets:
        node, in_func_import = node_info
        node_text = ast_utils.get_text(node)

        if not in_func_import:
            # add indent for each line
            node_text = "\n".join(["    " + line for line in node_text.split("\n")])

        function_body.append(node_text)

    # get verb-noun phrases
    root_verb, verb_noun_phrases, _ = queries_comparison.extract_root_and_verb_noun_phrases(nl_query, nlp)

    # map verb-noun phrases to AST nodes
    verb_noun_phrases_to_code = dict()
    if len(verb_noun_phrases) <= 1:
        verb_noun_phrases_to_code[verb_noun_phrases[0]] = [(fun, 1.0, "") for fun in function_body]
    else:
        map_results = map_verb_noun_phrases_to_sub_snippets(verb_noun_phrases, sub_snippets, nlp, threshold, 
                                                            lemma=lemma, check_root_verb=check_root_verb, 
                                                            has_string=has_string, has_comment=has_comment,
                                                            stop_words=stop_words)
        if all(v_n_phrase not in map_results for v_n_phrase in verb_noun_phrases[1:]):
            # more than 1 verb-noun phrases, but no mapping for ALL non-root verb-noun phrases
            # map root verb-noun phrase to all sub snippets
            if verb_noun_phrases[0] in map_results:
                verb_noun_phrases_to_code[verb_noun_phrases[0]] = map_results[verb_noun_phrases[0]]
            else:
                verb_noun_phrases_to_code[verb_noun_phrases[0]] = [(fun, 0.0, "") for fun in function_body] # just in case

            for v_n_phrase in verb_noun_phrases[1:]:
                verb_noun_phrases_to_code[v_n_phrase] = []
        else:
            for v_n_phrase in verb_noun_phrases:
                if v_n_phrase in map_results:
                    verb_noun_phrases_to_code[v_n_phrase] = map_results[v_n_phrase]
                else:
                    verb_noun_phrases_to_code[v_n_phrase] = []
    
    return verb_noun_phrases_to_code


def reduce_similar_snippets(info, nlp, reduce_threshold, lemma=True, check_root_verb=True, stop_words=True):
    """Similar snippets are reduced to one snippet with the highest similarity score (between the nl and code)

    Args:
        info (list(str, float, str)): list of code snippets, similarity scores with the nl, and explainations
        nlp (spacy.lang.en.English): Spacy model
        reduce_threshold (float): threshold for snippets similarity
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        list(str, float, str): reduced list of code snippets, similarity scores with the nl, and explainations
    """
    if len(info) <= 1:
        return info
    
    explainations = []
    for i in range(len(info)):
        _, _, explaination = info[i]
        explainations.append(explaination)

    # compare the explainations between snippets
    # if their snippet similarity is higher than the reduce_threshold, then the snippets are similar
    # only the snippet with highest nl-code similarity is kept
    similar_query_idxs = {}
    for i in range(len(explainations)):
        for j in range(i+1, len(explainations)):
            if explainations[i] and explainations[j]:
                similar, reduce_score = is_similar(explainations[i], explainations[j], nlp, reduce_threshold, 
                                                lemma=lemma, check_root_verb=check_root_verb, stop_words=stop_words)
                if similar:
                    if i not in similar_query_idxs:
                        similar_query_idxs[i] = [j]
                    else:
                        similar_query_idxs[i].append(j)

    # keep the snippet with highest nl-code similarity
    reduced_idxs = set()
    for idx, idxs in similar_query_idxs.items():
        max_nl_code_score = info[idx][1]
        max_idx = idx
        for i in idxs:
            if info[i][1] > max_nl_code_score:
                max_nl_code_score = info[i][1]
                reduced_idxs.add(max_idx)
                max_idx = i
            else:
                reduced_idxs.add(i)
    
    reduced_info = []
    for idx in range(len(info)):
        if idx not in reduced_idxs:
            reduced_info.append(info[idx])

    return reduced_info


def map_nl_ast_reduce_similar_codes(nl_query, target_code, nlp, nl_code_threshold=0.5, reduce_threshold=0.99,
                                     lemma=True, check_root_verb=True, has_string=True, has_comment=True, stop_words=True):
    """Given a NL query and its target code in AST, map the verb-noun phrases of the query to sub snippets of the code.
    For each non-root verb-noun phrase, similar snippets are reduced to one snippet.

    Args:
        nl_query (str): NL query
        target_code (str): target code of the query
        nlp (spacy.lang.en.English): Spacy model
        nl_code_threshold (float, optional): threshold for similarity between nl and snippet. Defaults to 0.5.
        reduce_threshold (float, optional): threshold for similarity between snippets. Defaults to 0.9.
        lemma (bool, optional): whether to use lemma for comparison. Defaults to True.
        check_root_verb (bool, optional): whether to check the root verb first, before similarity. Defaults to True.
        has_string (bool, optional): whether to include string in the explaination. Defaults to True.
        has_comment (bool, optional): whether to include comment in the explaination. Defaults to True.
        stop_words (bool, optional): whether to remove stop words before comparison. Defaults to True.

    Returns:
        dict{str: list(str)}: list of verb-noun phrases and their corresponding code snippets
    """
    verb_noun_phrases_to_code = map_nl_ast(nl_query, target_code, nlp, nl_code_threshold, lemma, check_root_verb, has_string, has_comment, stop_words)

    if len(verb_noun_phrases_to_code.keys()) > 1: # more than 1 verb-noun phrases
        for v_n_phrase in list(verb_noun_phrases_to_code.keys())[1:]: # skip the root verb-noun phrase
            info = verb_noun_phrases_to_code[v_n_phrase]
            if len(info) > 1:
                # reduce similar snippets to one snippet, if any
                info = reduce_similar_snippets(info, nlp, reduce_threshold, lemma=lemma, check_root_verb=check_root_verb, stop_words=stop_words)
                verb_noun_phrases_to_code[v_n_phrase] = info

    return verb_noun_phrases_to_code