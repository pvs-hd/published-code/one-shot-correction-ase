# functions create nlp

import spacy
from spacy.tokenizer import Tokenizer

LANGUAGE_MODEL = "en_core_web_md"

def create_code_lp(nlp):
    """Create a language processing for code based on nlp of Spacy.
    This parser is just used to extract tokens from the code.

    Args:
        nlp (spacy.lang.en.English): Spacy model

    Returns:
        spacy.lang.en.English: Spacy model for code
    """
    language_model_name = nlp.path.name.split("-")[0]

    code_lp = spacy.load(LANGUAGE_MODEL)
    prefix_re = spacy.util.compile_prefix_regex(code_lp.Defaults.prefixes)
    suffix_re = spacy.util.compile_suffix_regex(code_lp.Defaults.suffixes)
    custom_infixes = [r"\=|\(|\)|\:|\'|\""]
    infix_re = spacy.util.compile_infix_regex(list(code_lp.Defaults.infixes) + custom_infixes)
    code_lp.tokenizer = Tokenizer(
        nlp.vocab,
        prefix_search=prefix_re.search,
        suffix_search=suffix_re.search,
        infix_finditer=infix_re.finditer,
        token_match=None
    )

    return code_lp


def create_nlp(language_model=LANGUAGE_MODEL):
    """Create a Spacy model for English language.

    Returns:
        spacy.lang.en.English: Spacy model for English language
    """
    nlp = spacy.load(language_model)
    if "which" in nlp.Defaults.stop_words:
        nlp.Defaults.stop_words.remove("which") # add which to increase similarity
    return nlp