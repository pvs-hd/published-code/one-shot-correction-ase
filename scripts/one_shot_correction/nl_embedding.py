# Refer to https://github.com/openai/openai-cookbook/blob/main/examples/Recommendation_using_embeddings.ipynb
# Embed NL queries

from pathlib import Path
import pickle
import openai
import os
import time
from openai.embeddings_utils import get_embedding
import json

config_file = "path to your/scripts/chunking_eval/config.json"


def get_openai_api_key():
    try:
        with open(config_file, "r") as cf:
            config = json.load(cf)
    except FileNotFoundError:
        print("File not found!")
        return None
    
    api_key = os.getenv("OPENAI_API_KEY")
    if os.name == "nt":
        api_key = config["openai_api_key"]
    return api_key


def get_embeddings_cache(cache_file):
    """Establish a cache of embeddings to avoid recomputing
    cache is a dict of tuples (text, engine) -> embedding, saved as a pickle file.

    Args:
        cache_file (str): path to cache file
    """
    # load the cache if it exists, and save a copy to disk
    try:
        with open(cache_file, "rb") as f:
            embedding_cache = pickle.load(f)

            # for checking the embedding of the test suite
            # if "test_suite" in cache_file:
            #     print("Len of embedding cache test suite: ", len(embedding_cache))
    except FileNotFoundError:
        print("Creating new embedding cache")
        embedding_cache = {}
        with open(cache_file, "wb") as f:
            pickle.dump(embedding_cache, f)

    return embedding_cache


def call_codex_api(delay_in_seconds: float = 1, **kwargs):
    """Delay a completion by a specified amount of time."""

    # Sleep for the delay
    time.sleep(delay_in_seconds)

    # Call the Completion API and return the result
    return get_embedding(**kwargs)


def get_nl_embedding(rate_limit_per_minute, nl_query, cache_file, engine="text-embedding-ada-002"):
    """Return embedding of a given NL query, using a cache to avoid recomputing.
    If the NL query is not in the cache, request via OpenAI API and save to cache.

    Args:
        rate_limit_per_minute (int): rate limit per minute, e.g. 20, from OpenAI's website
        nl_query (str): NL query
        cache_file (str): path to cache file
        engine (str): engine to use. Defaults to "text-embedding-ada-002".
    """
    # Calculate the delay based on your rate limit
    delay = 60.0 / rate_limit_per_minute

    embedding_cache = get_embeddings_cache(cache_file)

    api_key = get_openai_api_key()

    if (nl_query, engine) not in embedding_cache.keys():
        print("The query is not in cache, requesting from OpenAI API...")
        embedding_cache[(nl_query, engine)] = call_codex_api(
            api_key=api_key,
            delay_in_seconds=delay, text=nl_query, engine=engine
        )
        with open(cache_file, "wb") as embedding_cache_file:
            pickle.dump(embedding_cache, embedding_cache_file)

    return embedding_cache[(nl_query, engine)]


def get_nl_embedding_multi_caches(nl_query, cache_files, engine="text-embedding-ada-002", rate_limit_per_minute=20):
    """Returns embedding of a given NL query.
    If the query is not in any caches, request via OpenAI API and save to the last cache file.

    Args:
        nl_query (str): NL query
        cache_files (list): list of paths to cache files
        engine (str): engine to use. Defaults to "text-embedding-ada-002".
        rate_limit_per_minute (int): rate limit per minute, e.g. 20, from OpenAI's website
    """
     # Calculate the delay based on your rate limit
    delay = 60.0 / rate_limit_per_minute
    embedding_values = []

    for cache_file in cache_files:
        embedding_cache = get_embeddings_cache(cache_file)
        if (nl_query, engine) in embedding_cache.keys():
            embedding_values = embedding_cache[(nl_query, engine)]
            break

    api_key = get_openai_api_key()

    if not embedding_values:
        print("The query is not in cache, requesting from OpenAI API...")
        embedding_values = call_codex_api(
            api_key=api_key,
            delay_in_seconds=delay, text=nl_query, engine=engine
        )

        # store the new embedding into the last cache file
        embedding_cache[(nl_query, engine)] = embedding_values
        with open(cache_file, "wb") as embedding_cache_file:
            pickle.dump(embedding_cache, embedding_cache_file)
        print("Len of embedding cache test suite changed to: ", len(embedding_cache))

    return embedding_values