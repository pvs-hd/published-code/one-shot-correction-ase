# For saving and retrieving correction information
# Assuming that all embeddings in the datastore were generated by the same engine, i.e. text-embedding-ada-002 from OpenAI.
# The correction datastore is a dictionary with the following structure:
# {
#     (nl_query_embedding list->tuple): {
#         "nl": "the nl query",
#         "code": "the corrected code in string"
#         # additional field will be added later
#     }
# }


import pickle
from scripts.one_shot_correction import similar_queries


class CorrectionDS():
    def __init__(self, ds_path=None, ds=None):
        self.path = ds_path
        self.ds = ds
        self.correction_ds = {}

        self._load_correction_ds()

    def _load_correction_ds(self):
        if self.path is not None:
            try:
                with open(self.path, "rb") as f:
                    self.correction_ds = pickle.load(f)
            except FileNotFoundError:
                print("Creating an empty correction datastore at: ", self.path)
                self.correction_ds = {}
                with open(self.path, "wb") as f:
                    pickle.dump(self.correction_ds, f)
        elif self.ds is not None:
            # load datastore from a dict
            self.correction_ds = self.ds
        else:
            raise ValueError("No correction datastore is loaded!")

    def get_knn_indices(self, query_emb, knn=5, knn_threshold=0.25):
        """Get the KNNs of a given query embedding.

        Args:
            query_emb (list): embedding of the query.
            knn (int): number of nearest neighbors to return.
            knn_threshold (float): threshold of cosine distance to filter out the nearest neighbors. Default is 0.25.

        Returns:
            (list, list) A tuple of list of indices of keys in datastore, and list of distances.
        """
        # get all embeddings from the datastore
        queries_embs = [list(k) for k in self.correction_ds.keys()]
        # get indices of nearest neighbors
        indices, distances = similar_queries.get_indices_nearest_neighbors(query_emb, queries_embs, knn=knn, knn_threshold=knn_threshold)
        return indices, distances
    
    def update_correction_ds(self, query_emb, nl_query, code):
        """Add a new item or update an existing item in the correction datastore.

        Args:
            query_emb (list): embedding of the query.
            nl_query (str): the nl query.
            code (str): the corrected code in string.
        """
        self.correction_ds[tuple(query_emb)] = {
            "nl": nl_query,
            "code": code
        }
        with open(self.path, "wb") as f:
            pickle.dump(self.correction_ds, f)

    def get_correction(self, query_emb):
        """Get the correction information of a given query embedding.

        Args:
            query_emb (list): embedding of the query.

        Returns:
            dict: correction information.
        """
        return self.correction_ds[tuple(query_emb)]
    
    def delete_correction(self, query_emb):
        """Delete the correction information of a given query embedding.

        Args:
            query_emb (list): embedding of the query.
        """
        del self.correction_ds[tuple(query_emb)]
        with open(self.path, "wb") as f:
            pickle.dump(self.correction_ds, f)