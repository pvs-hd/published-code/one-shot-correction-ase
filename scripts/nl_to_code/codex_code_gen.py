# Reference: https://github.com/openai/openai-cookbook/blob/main/examples/How_to_handle_rate_limits.ipynb

import os
import openai
import sys
import time
import json

config_file = "path to your/scripts/chunking_eval/config.json"


def get_openai_api_key():
    try:
        with open(config_file, "r") as cf:
            config = json.load(cf)
    except FileNotFoundError:
        print("File not found!")
        return None
    
    api_key = os.getenv("OPENAI_API_KEY")
    if os.name == "nt":
        api_key = config["openai_api_key"]
    return api_key


def add_quotes(nl_queries):
    return ["\"\"\"\n{}\n\"\"\"\n".format(query) for query in nl_queries]

def call_codex_api(delay_in_seconds: float = 1, **kwargs):
    """Delay a completion by a specified amount of time."""

    # Sleep for the delay
    time.sleep(delay_in_seconds) # disable for running GUI

    # Call the Completion API and return the result
    return openai.ChatCompletion.create(**kwargs)

def get_codes_single_input(nl_query, rate_limit_per_minute,
              **kwargs):
    generated_codes = []

    api_key = get_openai_api_key()

    # Calculate the delay based on your rate limit
    delay = 60.0 / rate_limit_per_minute

    # Adjust the temperature and topp to get different results
    responses = call_codex_api(
        api_key=api_key,
        delay_in_seconds=delay,
        **kwargs
    )
    print("Got response for query: ", nl_query)
    print(responses)

    for choice in responses["choices"]:
        generated_codes.append(choice["message"]["content"].lstrip("\n"))

    return generated_codes


def remove_python_mark(code):
    """Remove the Python code mark, happened with gpt-3.5-turbo
    """
    refined_code = code.strip().strip("\n")
    if refined_code.startswith("```python") and refined_code.endswith("```"):
        return refined_code[10:-3]
    else:
        return code


def get_codes(nl_queries, rate_limit_per_minute, stop_sequences, max_tokens, add_quote=False,
              model="gpt-3.5-turbo",
              system_content=("You are a helpful assistant "
                            "that translates natural language into Python function, "
                            "return only Python code snippets, "
                            "no explanation. "
                            "names in the Python code must be separated by underscore. "
                            "Each answer is a different Python function. "
                            "The answers are sorted by how close the code snippets are to the query."),
                user_content=("Translate the below query to Python code with the following constraints: \n"
                            "1. return only Python code, no explanation. \n"
                            "2. names in the Python code must be separated by underscore \n")):
    generated_codes = {}
    if add_quote:
        nl_queries = add_quotes(nl_queries)
    query_indices = len(nl_queries)

    api_key = get_openai_api_key()

    # Initiate the results dictionary
    for i in range(query_indices):
        generated_codes[i] = dict()
        generated_codes[i]["logprobs"] = []
        generated_codes[i]["codes"] = []

    # Calculate the delay based on your rate limit
    delay = 60.0 / rate_limit_per_minute

    # Adjust the temperature and topp to get different results
    for idx, query in enumerate(nl_queries):
        responses = call_codex_api(
            api_key=api_key,
            delay_in_seconds=delay,
            model=model, # used to be code-davinci-002
            messages=[
                {"role": "system", "content": system_content},
                {"role": "user", "content": "{}{}".format(user_content, query)},
            ],
            temperature=0.9,
            max_tokens=max_tokens,
            top_p=0.9,
            n=1,
            frequency_penalty=0.5,
            presence_penalty=1.5,
            stop=stop_sequences,
        )
        print("Got response for query: ", query)
        print(responses)

        for choice in responses["choices"]:
            generated_codes[idx]["codes"].append(choice["message"]["content"].lstrip("\n"))

    return generated_codes


def discard_extra_codes(generated_code):
    """Discard extra code snippets, i.e. unnecessary functions in the generated code.
    Only use this function when the target code is a single function.

    Args:
        generated_code (str): generated code snippet.
    
    Returns:
        str: cleaned code snippet.
    """
    start_comment = "\n\"\"\""
    start_function = "def "
    enter_function = "\n" + start_function
    generated_function = generated_code
    starts_with_enter_function = None
    have_another_function = None

    if start_comment in generated_code:
        generated_function = generated_code.split(start_comment)[0]

    if generated_function.startswith(start_function):
        starts_with_enter_function = False
        have_another_function = generated_function.count(enter_function) >= 1
    else:
        starts_with_enter_function = True
        have_another_function = generated_function.count(enter_function) >= 2

    if (not starts_with_enter_function) and have_another_function:
        generated_function = generated_function.split(enter_function)[0]
    elif starts_with_enter_function and have_another_function:
        generated_function = enter_function.join(generated_function.split(enter_function)[0:2])

    return generated_function


def order_logprobs(generated_codes):
    """Order the generated codes by logprobs.

    Args:
        generated_codes (dict): generated codes.

    Returns:
        dict: ordered generated codes by logprobs.
    """
    ordered_generated_codes = {}
    for idx, logs_codes in generated_codes.items():
        logs = logs_codes["logprobs"]
        codes = logs_codes["codes"]
        ordered_generated_codes[idx] = dict()
        ordered_generated_codes[idx]["logprobs"] = []
        ordered_generated_codes[idx]["codes"] = []
        sorted_indices = sorted(range(len(logs)), key=lambda k: logs[k], reverse=True)
        for i in sorted_indices:
            ordered_generated_codes[idx]["logprobs"].append(logs[i])
            ordered_generated_codes[idx]["codes"].append(codes[i])
    return ordered_generated_codes


def functions_only(generated_codes):
    """Only keep codes as function definitions.

    Args:
        generated_codes (dict): generated codes.

    Returns:
        dict: only generated function definitions.
    """
    functions_only_codes = {}
    for idx, logs_codes in generated_codes.items():
        logs = logs_codes["logprobs"]
        codes = logs_codes["codes"]
        functions_only_codes[idx] = dict()
        functions_only_codes[idx]["logprobs"] = []
        functions_only_codes[idx]["codes"] = []
        for i, code in enumerate(codes):
            if code.lstrip("\n").lstrip().startswith("def "):
                functions_only_codes[idx]["logprobs"].append(logs[i])
                functions_only_codes[idx]["codes"].append(code)
            else:
                functions_only_codes[idx]["logprobs"].append(-sys.float_info.max)
                functions_only_codes[idx]["codes"].append("")
    return functions_only_codes


if __name__ == "__main__":
    nl_query = "add two number x and y"
    rate_limit_per_minute = 20

    # we expect the generated code to be a single function
    stop_sequences = ["\n\"\"\"", "\n#"]
    system_content="You are a helpful assistant"
    user_content=("Translate the below query to Python code with the following constraints:\n"
                "1. Return only Python code, no explanation, no python mark\n"
                "2. Names in the Python code must be separated by underscores\n"
                )
    messages = [
        {"role": "system", "content": system_content},
        {"role": "user", "content": "{}Query: \nFunction {}".format(user_content, nl_query)},
    ]

    generated_codes = get_codes_single_input(nl_query, rate_limit_per_minute,
                                            model="gpt-3.5-turbo",
                                            messages=messages,
                                            temperature=0.9,
                                            max_tokens=200,
                                            top_p=0.9,
                                            n=1,
                                            frequency_penalty=0.5,
                                            presence_penalty=1.5,
                                            stop=stop_sequences)
    
    print("*******Query {}*******".format(nl_query))
    
    for i, code in enumerate(generated_codes):
        print("===={} with logprobs {}====\n{}\n".format(i, "#no logprobs#", code))
        print("======={}-refined=======\n{}\n".format(i, discard_extra_codes(code)))